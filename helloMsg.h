#ifndef __HELLO_H_
#define __HELLO_H_

#include "mac.h"
#include <stdio.h>

struct Hello
{
	// HEADER
	char messageType;
	char VTIME;
	short messageSize;
	struct mac_addr originatorAddress; //long originatorAddress = 0x02 + 0x0X + 0x0X + 0x0X
	char timeToLive;
	char hopCount;
	short messageSequenceNum; //initial value 0
	
	// CONTENT
	short RESERVED;
	char HTIME;
	char willingness;
	char linkCode;
	char RESERVED2;
	short linkMsgSize;
	char * neighborList;
};

short initHellomsg(struct Hello *Hi, struct mac_addr originatorAddress,
				   struct mac_addr * neighborList, int len,
				   char type);
char * serializeHelloMessage(struct Hello * Hi);
struct Hello * deserializeHelloMessage(char* message, int length);

void recvAddressList(int timer, struct Hello *recHi, struct mac_addr * asymList,
					 struct mac_addr * symList, struct mac_addr * twoHopList,
					 struct mac_addr senderMAC, struct mac_addr ourMAC,
					 int *sizeAsym, int *sizeSym, int *sizeTwoHop, FILE * file);

void checkExpiry(struct mac_addr *asymList, struct mac_addr *symList, struct mac_addr *twoHopList, 
				 int *sizeAsym, int *sizeSym, int *sizeTwoHop, int timer, FILE * log);
#endif