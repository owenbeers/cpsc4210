
void recvAddressList(struct Hello recHi, struct mac_addr *asymList,
		struct mac_addr *symList, struct mac_addr *twoHopList, struct mac_addr senderMAC,
		struct mac_addr ourMAC, int *sizeAsym, int *sizeSym, int *sizeTwoHop)
{
	struct mac_addr emptyMAC;
	struct mac_addr currentMac;
	bool selfFound = false;
	int k = 0;
	if (recHi.linkCode==4) //ASYM LIST 
	{
		//bool selfFound;
		//selfFound = false;
		bool foundSenderInSym;
		foundSenderInSym = false;
		int i;
		for(i=0; i<((recHi.linkMsgSize - 8)/4); i++) 
		//Loop through all of the neighbours
		{
			int j;
			for(j=0; j<4; j++)
			//Each neighbour occupies 4 slots in the char array, one for each group of 2 hex characters
			{
				//We add 'k' so we are working with the 'next' neighbour
				currentMac.bytes[j] = recHi.neighborList[j+k];			
			}
			//compare this neighbour to our address
			if(currentMac.bytes[0] == ourMAC.bytes[0] && currentMac.bytes[1] == ourMAC.bytes[1]
				&& currentMac.bytes[2] == ourMAC.bytes[2] && currentMac.bytes[3] == ourMAC.bytes[3])
			{
				selfFound = true; //We set this to true so we don't add this neighbour to our Asym list
			}
			k+=4;
		}
		//Finished looking for ourself in the asym list.
		if (selfFound == true)
		{
			//We found ourself in this host's asym list. This means they received a hello from us, 
			//but didn't see themselves. We need to make sure they are in our sym list.
			int m;
			for(m=0; m<*sizeSym; m++)
			{
				if(senderMAC.bytes[0] == symList[0].bytes[0] && senderMAC.bytes[1] == symList[1].bytes[1]
					&& senderMAC.bytes[2] == symList[2].bytes[2] && senderMAC.bytes[3] == symList[3].bytes[3])
				{
					foundSenderInSym = true;
				}
			}
			//Do not combine these loops. We need to verify them separately.
			for(m=0; m<*sizeAsym; m++)
			{
				if(senderMAC.bytes[0] == asymList[0].bytes[0] && senderMAC.bytes[1] == asymList[1].bytes[1]
					&& senderMAC.bytes[2] == asymList[2].bytes[2] && senderMAC.bytes[3] == asymList[3].bytes[3])
				{
					if(selfFound == true)
					{
						asymList[m] = asymList[(*sizeAsym)-1];
						asymList[(*sizeAsym)-1] = emptyMAC;
						(*sizeAsym)--;
					}
				}
			}
			if(foundSenderInSym == false)
			{
				//Add this sender to our sym list
				symList[(*sizeSym)] = senderMAC;
				(*sizeSym)++;
			}
		}
		else 
		{
			int m;
			bool foundSenderInAsym = false;
			for(m=0; m<*sizeAsym; m++)
			{
				if(senderMAC.bytes[0] == asymList[0].bytes[0] && senderMAC.bytes[1] == asymList[1].bytes[1]
					&& senderMAC.bytes[2] == asymList[2].bytes[2] && senderMAC.bytes[3] == asymList[3].bytes[3])
				{
					foundSenderInAsym = true;
				}
			}
			if(foundSenderInAsym == false)
			{
				//Add this sender to our asym list
				asymList[(*sizeAsym)] = senderMAC;
				(*sizeAsym)++;
			}
		}
	}
	k=0;
	if (recHi.linkCode==3) //SYM LIST (All neighbours are either sender sym 1-hop or our 2-hops)
	{
		bool foundInAsym = false;
		bool foundInSym = false;
		bool foundSelf = false;
		bool foundSenderInSym = false;
		//Not sure if we divide by 8 or by 32, not sure if we -8 or -12 (you guys wrote conflicting code)
		int i;
		for(i=0; i<((recHi.linkMsgSize - 8)/4); i++) 
		//Loop through all of the neighbours
		{
			int j;
			for(j=0; j<4; j++)
			//Each neighbour occupies 4 slots in the char array, one for each group of 2 hex characters
			{
				//We add 'k' so we are working with the 'next' neighbour
				currentMac.bytes[j] = recHi.neighborList[j+k];	
			}

			//compare this neighbour to our address
			if(currentMac.bytes[0] == ourMAC.bytes[0] && currentMac.bytes[1] == ourMAC.bytes[1]
				&& currentMac.bytes[2] == ourMAC.bytes[2] && currentMac.bytes[3] == ourMAC.bytes[3])
			{
				//This neighbour in the list is us. We will discard this packet
				//after updating this neighbours status.
				int l;
				for(l=0; l<*sizeSym; l++)
				{
					if(symList[l].bytes[0] == senderMAC.bytes[0] && symList[l].bytes[1] == senderMAC.bytes[1]
						&& symList[l].bytes[2] == senderMAC.bytes[2] && symList[l].bytes[3] == senderMAC.bytes[3])
					{
						foundSenderInSym = true;
					}
				}
				for(l=0; l<*sizeAsym; l++)
				{
					//If the sender is in our Asym list, remove them
					if(asymList[l].bytes[0] == senderMAC.bytes[0] && asymList[l].bytes[1] == senderMAC.bytes[1]
						&& asymList[l].bytes[2] == senderMAC.bytes[2] && asymList[l].bytes[3] == senderMAC.bytes[3])
					{
						asymList[l] = asymList[(*sizeAsym)-1];
						asymList[(*sizeAsym)-1] = emptyMAC;
						(*sizeAsym)--;
					}
				}
				//If the sender is not in our sym list, add them.
				if(foundSenderInSym == false)
				{
					symList[(*sizeSym)] = senderMAC;
					(*sizeSym)++;
					foundSenderInSym = true;
				}
			}

			//Check if we have heard from this neighbour before
			int n;
			for(n=0; n<*sizeSym; n++) //If these len() checks dont work, we can use (*sizeAsym) - 1
			{
				if(currentMac.bytes[0] == symList[0].bytes[0] && currentMac.bytes[1] == symList[1].bytes[1]
					&& currentMac.bytes[2] == symList[2].bytes[2] && currentMac.bytes[3] == symList[3].bytes[3])
				{
					foundInSym = true;
				}
			}
			int o;
			for(o=0; o<*sizeAsym; o++)
			{
				if(currentMac.bytes[0] == asymList[0].bytes[0] && currentMac.bytes[1] == asymList[1].bytes[1]
					&& currentMac.bytes[2] == asymList[2].bytes[2] && currentMac.bytes[3] == asymList[3].bytes[3])
				{
					foundInAsym = true;
				}
			}
			if(foundInSym == false && foundInAsym == false)
			{
				//Add neighbour to twohop
				twoHopList[(*sizeTwoHop)] = currentMac;
				(*sizeTwoHop)++;
			}
			foundInAsym = false;
			foundInSym = false;
			foundSelf = false;
			//else do nothing because the neighbour is within our range (we heard a hello from them before)
		}
	}
}
