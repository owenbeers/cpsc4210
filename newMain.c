#include <sys/socket.h>
#include <sys/types.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include "sock.h"
#include "packet.h"
#include "helloMsg.h"
#include "mac.h"


int main()
{
	struct Ifconfig ifc_send, ifc_receive;
	init(&ifc_send);
	init(&ifc_receive);
	FILE * file;
	file = fopen("log.txt", "w+");
	fclose(file);
	if(file==NULL)
		printf("error opening file\n");


	printf("This Pi is ");
	/* print the MAC addr */
	int i;
	for (i=0; i<MACADDRLEN-1; i++)
		printf("%02X:", ifc_send.mac[i]);
	printf("%02X\n", ifc_send.mac[MACADDRLEN-1]);

	struct mac_addr ourMAC;
	ourMAC.bytes[0] = '\x02';
	ourMAC.bytes[1] = ifc_send.mac[MACADDRLEN-3];
	ourMAC.bytes[2] = ifc_send.mac[MACADDRLEN-2];
	ourMAC.bytes[3] = ifc_send.mac[MACADDRLEN-1];

	// RECEIVE SOCKET - SET UP SOURCE SOCKET
	struct sockaddr_ll from;
	socklen_t fromlen = sizeof(from);
	/* setting up the buffer or receiving */
	char * buf_receive = malloc(ifc_receive.mtu);
	if (buf_receive == NULL)
		die("Cannot allocate receiving buffer\n");

	// SEND SOCKET
	/* set-up destination structure in a sockaddr_ll struct */
	struct sockaddr_ll to;
	memset(&to, 0, sizeof(to)); /* clearing the structure, just in case */
	to.sll_family = AF_PACKET; /* always AF_PACKET */
	to.sll_ifindex = ifc_send.ifindex;
	to.sll_halen = MACADDRLEN;

	/* setup broadcast address of FF:FF:FF:FF:FF:FF */
	for (i=0; i<MACADDRLEN; i++)
		to.sll_addr[i] = 0xff;

	/*Neighbour Lists*/
	struct mac_addr asymList[10];
	struct mac_addr symList[10];
	struct mac_addr twoHopList[10];

	struct mac_addr * asymPtr = malloc(sizeof(struct mac_addr));
	asymPtr = &asymList[0];

	struct mac_addr * symPtr = malloc(sizeof(struct mac_addr));
	symPtr = &symList[0];

	struct mac_addr * twoHopPtr = malloc(sizeof(struct mac_addr));
	twoHopPtr = &twoHopList[0];

	int sizeAsym = 0;
	int sizeSym = 0;
	int sizeTwoHop = 0;
	int *sizeAsymPtr = &sizeAsym;
	int *sizeSymPtr = &sizeSym;
	int *sizeTwoHopPtr = &sizeTwoHop;

	int timer=0;

	while(1)
	{
		//Init hellomsg
		struct Packet * pack = malloc(sizeof(struct Packet));//, *packPtr;
		initPacket(pack, ourMAC, symList, sizeSym, asymList, sizeAsym);
		int packetLength = pack->packetLength;

		char *sendBuffer = serializePacket(pack);

		// Send the hello message after it has been initialized
		int sendStruct = sendto(ifc_send.sockd, sendBuffer, packetLength, 0, (struct sockaddr*) &to, sizeof(to));
		printf("%d bytes sent\n", sendStruct);

  		sleep(1);
  		timer+=1;

		int recvlen = recvfrom(ifc_receive.sockd, buf_receive, ifc_receive.mtu, 0, (struct sockaddr*) &from, &fromlen);
		if (recvlen < 0)
		{
			printf("Cannot receive data: %s\n", strerror(errno));
		} 
		else if (recvlen > 20 && recvlen < 200)
		{
			printf("Received %d bytes from ", recvlen);

			for (i=0; i<from.sll_halen-1; i++)
	     		printf("%02X:", from.sll_addr[i]);
      		printf("%02X >\n", from.sll_addr[from.sll_halen-1]);

      		if(buf_receive[4] != 0x01)
      			continue;
      		 // message type == 0x01
				
			struct Packet *recHi;
			recHi = deserializePacket(buf_receive);
			struct mac_addr senderMAC;
			senderMAC = recHi->helloSYM->originatorAddress;

			if(ourMAC.bytes[0] == senderMAC.bytes[0])
			{
				printf("Received from ourselves\n");
				continue;
			}


			recvAddressList(timer, recHi->helloSYM, asymPtr, symPtr, twoHopPtr, senderMAC, ourMAC, sizeAsymPtr, sizeSymPtr, sizeTwoHopPtr, file);
			recvAddressList(timer, recHi->helloASYM, asymPtr, symPtr, twoHopPtr, senderMAC, ourMAC, sizeAsymPtr, sizeSymPtr, sizeTwoHopPtr, file);
			/* print buffer content */
			printf("%s\n", buf_receive);
			destroyPacket(recHi);
  		}

  		checkExpiry(asymPtr, symPtr, twoHopPtr, sizeAsymPtr, sizeSymPtr, sizeTwoHopPtr, timer, file);

  		free(buf_receive);
  		buf_receive = malloc(ifc_receive.mtu);
  		destroyPacket(pack);
		
	}

	fclose(file);
	free(asymPtr);
	free(symPtr);
	free(twoHopPtr);
	free(sizeSymPtr);
	free(sizeAsymPtr);
	free(sizeTwoHopPtr);
	destroy(&ifc_receive);
	destroy(&ifc_send);
	return 0;
}
