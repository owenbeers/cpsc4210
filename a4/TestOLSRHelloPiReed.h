// *** CPSC 4210/5210 Assignment 3 code
// ***** CPPUnit fixture class sample for creating headers for IP/UDP packets ***

#ifndef __TESTOLSRHelloPacketPiReed_H
#define __TESTOLSRHelloPacketPiReed_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <iostream>

#include "OLSRHelloFactory.h"
#include "OLSRHello.h"
#include "IPAddr.h"

#include <netinet/in.h>


/// Test fixture for ProgressiveRateStrategy
class TestOLSRHelloPiReed : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(TestOLSRHelloPiReed);
	CPPUNIT_TEST(testOLSRHelloreserved);
	CPPUNIT_TEST(testOLSRHelloHtime);
	CPPUNIT_TEST(testOLSRHelloWillingness);
	CPPUNIT_TEST(testOLSRHelloLinkCode);
	CPPUNIT_TEST(testOLSRHelloreserved2);
	CPPUNIT_TEST(testOLSRHellolms);
	CPPUNIT_TEST(testOLSRHelloNIA);
	CPPUNIT_TEST(testOLSRHelloisValid);
	CPPUNIT_TEST(testOLSRlink);
	CPPUNIT_TEST_SUITE_END();
private:
	// we make the factory static so that we can initialize it in the
	// .cc file to the concrete factory that returns the concrete object
	// with our implementation of setHeader()
	static std::shared_ptr<OLSRHelloFactory> factory;

private:
	// the packet object being tested
	std::shared_ptr<OLSRHello> hello;
	// the buffer used for testing
	unsigned char * buffer;
	// the buffer length
	const int buflen = 1000;

	std::vector<IPAddr> mpr;
	std::vector<IPAddr> nbours;

	//setHeader(unsigned char * buffer, int buflen, std::vector<IPAddr> mpr, std::vector<IPAddr> nbours)

public:
	void setUp()  {
		if (factory)
			hello = factory->createOLSRHello();
		else {
	    std::cerr << "TestHelloSample test fixture not properly initialized!\n";
			exit(EXIT_FAILURE);
		}
		buffer = new unsigned char [buflen];
	};
	
	void tearDown()  {
		delete[] buffer;
		// we don't need to delete ipudp since it is a smart pointer
	};

	// *************** test cases ************
	void testOLSRHelloreserved()
	{
		hello->setHeader(buffer,buflen, mpr, nbours);
		
		uint16_t got;
	
		memcpy(&got, buffer+0, sizeof(uint16_t));

		CPPUNIT_ASSERT(got == 0);
	}

	void testOLSRHelloHtime()
	{
		hello->setHeader(buffer,buflen, mpr, nbours);
		
		uint8_t got;
	
		memcpy(&got, buffer+2, sizeof(uint8_t));

		CPPUNIT_ASSERT(got == 0);
	}

	void testOLSRHelloWillingness()
	{
		hello->setHeader(buffer,buflen, mpr, nbours);
		
		uint8_t got;
	
		memcpy(&got, buffer+3, sizeof(uint8_t));

		CPPUNIT_ASSERT(got == 0);
	}

	void testOLSRHelloLinkCode()
	{
		hello->setHeader(buffer,buflen, mpr, nbours);
		
		uint8_t got;
	
		memcpy(&got, buffer+4, sizeof(uint8_t));

		//Checks the Neighbor type, default 0
		//0000 1100 = 0C
		CPPUNIT_ASSERT((got & 0x0C) == 0);

		//Checks the Link type, default 0
		//0000 0011 = 03
		CPPUNIT_ASSERT((got & 0x03) == 0);
	}

	void testOLSRHelloreserved2()
	{
		hello->setHeader(buffer,buflen, mpr, nbours);
		
		uint8_t got;
	
		memcpy(&got, buffer+5, sizeof(uint8_t));

		CPPUNIT_ASSERT(got == 0);
	}

	void testOLSRHellolms()
	{
		hello->setHeader(buffer,buflen, mpr, nbours);
		
		uint16_t got;
	
		memcpy(&got, buffer+6, sizeof(uint16_t));
		int neighborSize = (nbours.size()*4);

		//4 bytes for every neighbour, plus 4 bytes for  the header
		CPPUNIT_ASSERT(got == neighborSize+4);
	}

	//Neighbor interface address
	void testOLSRHelloNIA()
	{

		IPAddr temp(10,1);
		IPAddr temp2(10,2);

		nbours.push_back(temp);
		nbours.push_back(temp2);

		hello->setHeader(buffer,buflen, mpr, nbours);
		
		uint32_t got;
		uint32_t got2;
	
		//First neighbor address at byte 8
		memcpy(&got, buffer+8, sizeof(uint32_t));


		CPPUNIT_ASSERT(got == temp.to32bWord());

		//Second neighbor address at byte 12
		memcpy(&got2, buffer+12, sizeof(uint32_t));


		CPPUNIT_ASSERT(got2 == temp2.to32bWord());
	}

	void testOLSRHelloisValid()
	{
		hello->setHeader(buffer,buflen, mpr, nbours);
		OLSRHello::HeaderInfo hi;

		CPPUNIT_ASSERT(hello->isValid(buffer,buflen,4,hi));
	}


	void testOLSRlink()
	{
		hello->setHeader(buffer,buflen, mpr, nbours);
		OLSRHello::HeaderInfo hi;

		uint8_t link;
		std::memcpy(&link, buffer+4, sizeof(uint8_t));

		CPPUNIT_ASSERT(link == 0);
	}
	
};

#endif
