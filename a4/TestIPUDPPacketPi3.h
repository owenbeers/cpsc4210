
#ifndef __TESTIPUDPPACKETPI3_H
#define __TESTIPUDPPACKETPI3_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <bitset>
#include "IPUDPPacketFactory.h"
#include "IPAddr.h"
// #include "IPUDPPacketSample.h"
#include "IPUDPPacket.h"

using namespace std;


/// Test fixture for ProgressiveRateStrategy
class TestIPUDPPacketPi3 : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(TestIPUDPPacketPi3);
	CPPUNIT_TEST(sampleTestIPUDPPacketTooLarge);


    CPPUNIT_TEST(testIPUDPPktVersion);
	CPPUNIT_TEST(testIPUDPPktIhl);
	CPPUNIT_TEST(testIPUDPPktTypeOfService);
	CPPUNIT_TEST(testIPUDPPktTotalLength);
    CPPUNIT_TEST(testIPUDPPktIdentification);

	CPPUNIT_TEST(testIPUDPPktFlagsAndFragmentOffset);
	//CPPUNIT_TEST(testIPUDPPktFragmentOffset);
	CPPUNIT_TEST(testIPUDPPktTimeToLive);
    CPPUNIT_TEST(testIPUDPPktProtocol);

	CPPUNIT_TEST(testIPUDPPktHeaderChecksum);
	CPPUNIT_TEST(testIPUDPPktSourceAddr);
	CPPUNIT_TEST(testIPUDPPktDestinationAddr);
    CPPUNIT_TEST(testIPUDPPktSourcePort);

	CPPUNIT_TEST(testIPUDPPktDestinationPort);
	CPPUNIT_TEST(testIPUDPPktDataLength);
    CPPUNIT_TEST(testIPUDPPktUDPChecksum);
    CPPUNIT_TEST(testIPUDPHeaderIpcksum);

    // Assignement 4 testing function starts from here
    CPPUNIT_TEST(testIPUDPHeaderDecodingLengthShorter);
    CPPUNIT_TEST(testIPUDPHeaderDecodingLengthLarger);
    CPPUNIT_TEST(testIPUDPHeaderDecodingHeaderLength);
    CPPUNIT_TEST(testIPUDPHeaderDecodingDataLength);
    CPPUNIT_TEST(testIPUDPHeaderDecodingTTL);
    CPPUNIT_TEST(testIPUDPHeaderDecodingOrigin);
    CPPUNIT_TEST(testIPUDPHeaderDecodingDestination);
    CPPUNIT_TEST(testIPUDPHeaderDecodingCheckSumIpUDP);
	//----------CHECK SUM


	CPPUNIT_TEST_SUITE_END();

private:
	// we make the factory static so that we can initialize it in the
	// .cc file to the concrete factory that returns the concrete object
	// with our implementation of setHeader()
	static std::shared_ptr<IPUDPPacketFactory> factory;

private:
	// the packet object being tested
	std::shared_ptr<IPUDPPacket> ipudp;
	// the buffer used for testing
	unsigned char * buffer;
	// the buffer length
	const int buflen = 1000;
    uint16_t ipUdpPacketLength = 45;
    uint8_t timeToLive = 1;
    uint16_t dataLen = 20;
    int sndHeaderLenth = 28;
    int sndDataLength = sndHeaderLenth + dataLen;

public:
uint8_t ver_PACKET_ihl = 0x45;

 uint16_t Ipcksum = 0;
	void setUp()  {
		if (factory){
			ipudp = factory->createIPUDPPacket();

		}
		else {
	    std::cerr << "TestIPUDPSample test fixture not properly initialized!\n";
			exit(EXIT_FAILURE);
		}
		buffer = new unsigned char [buflen];
	};

	void tearDown()  {
		delete[] buffer;
		// we don't need to delete ipudp since it is a smart pointer
	};

	// *************** test cases ************
	void sampleTestIPUDPPacketTooLarge() {
        IPAddr origin, destination;
        int ipUdpPacketLength = 100;
		int header_len = ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
		CPPUNIT_ASSERT(header_len == -1);
	}

   void testIPUDPPktVersion() {

        IPAddr origin, destination;

        //uint8_t sndVersion = 4;
		ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        //uint8_t rcvdVersion = 0;
        memcpy(&ver_PACKET_ihl, buffer, sizeof(uint8_t));
        ver_PACKET_ihl = (ver_PACKET_ihl & 0xF0);
		CPPUNIT_ASSERT(ver_PACKET_ihl == 0x40);
     }

    void testIPUDPPktIhl(){

        IPAddr origin, destination;
       // uint8_t sndIhl = 5;
		ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        //uint8_t rcvdIhl = 0;
        memcpy(&ver_PACKET_ihl, buffer, sizeof(uint8_t));
        ver_PACKET_ihl =( ver_PACKET_ihl & 0x0F);
		CPPUNIT_ASSERT(ver_PACKET_ihl == 0x05);
     }


    void testIPUDPPktTypeOfService() {

        IPAddr origin, destination;
        uint8_t sndTypeOfService = 0;
		ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint8_t rcvdTypeOfService = 0;
        memcpy(&rcvdTypeOfService, buffer+1, sizeof(uint8_t));
		CPPUNIT_ASSERT(rcvdTypeOfService == sndTypeOfService);
	}

	  void testIPUDPPktTotalLength() {

        IPAddr origin, destination;
        uint16_t sndTotalLength = 50;
		ipudp->setHeader(buffer, sndTotalLength, origin, destination, timeToLive, dataLen);
        uint16_t rcvdTotalLength = 0;
        memcpy(&rcvdTotalLength, buffer+2, sizeof(uint16_t));
		CPPUNIT_ASSERT(rcvdTotalLength == sndTotalLength);
	}

    void testIPUDPPktIdentification() {

        IPAddr origin, destination;
        uint16_t sndIdentification = 0;
	    ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint16_t rcvdIdentification = 0;
        memcpy(&rcvdIdentification, buffer+4, sizeof(uint16_t));
		CPPUNIT_ASSERT(rcvdIdentification == sndIdentification);
     }

    void testIPUDPPktFlagsAndFragmentOffset(){
        IPAddr origin, destination;
        uint16_t sndFlags = 0;
	    ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint16_t rcvdFlags = 0;
        memcpy(&rcvdFlags, buffer+6, sizeof(uint16_t));
		CPPUNIT_ASSERT(rcvdFlags == sndFlags);
     }


    void testIPUDPPktTimeToLive() {

        IPAddr origin, destination;
        uint8_t sndTimeToLive = 1;
		ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint8_t rcvdTimeToLive = 0;
        memcpy(&rcvdTimeToLive, buffer+8, sizeof(uint8_t));
		CPPUNIT_ASSERT(rcvdTimeToLive == sndTimeToLive);
     }

    void testIPUDPPktProtocol(){

        IPAddr origin, destination;
        uint8_t sndProtocol = 138;
		ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint8_t rcvdProtocol = 0;
        memcpy(&rcvdProtocol, buffer+9, sizeof(uint8_t));
		CPPUNIT_ASSERT(rcvdProtocol == sndProtocol);
     }
       void testIPUDPPktHeaderChecksum() {

        IPAddr origin, destination;
        uint16_t sndHeaderChecksum = 0;
		ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint16_t rcvdHeaderChecksum = 0;
        memcpy(&rcvdHeaderChecksum, buffer+10, sizeof(uint16_t));
		//CPPUNIT_ASSERT(rcvdHeaderChecksum == sndHeaderChecksum);
     }

     //****Checksum
uint16_t testconvertion(int tstchk)
 {
    uint16_t ckvalue = 0;
    if (tstchk <= 1)
    {
        return !tstchk;
    }
    ckvalue = tstchk % 2;
    testconvertion(tstchk / 2);
    return ((ckvalue*10) + !ckvalue);
  }


     void testIPUDPHeaderIpcksum(){

		 IPAddr origin, destination;
		ipudp->setHeader(buffer, 28+10, origin, destination,timeToLive, dataLen);
		memcpy(&Ipcksum, buffer+10,sizeof(Ipcksum));

		uint16_t bufarr[10];
        memset(bufarr, 0, sizeof(bufarr));
        memcpy(bufarr, buffer, 20);
        uint16_t sum = 0;
        for (int i = 0; i < 10; i++) {
            sum += bufarr[i];
        }

        sum += Ipcksum;
        sum  = testconvertion(sum);
        Ipcksum = sum;

		CPPUNIT_ASSERT(Ipcksum == 1);
	}
	//************************************

    void testIPUDPPktSourceAddr(){

      IPAddr origin(3, 1), destination;
      ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
      uint32_t rcvdSrcAddr = 0;
      memcpy(&rcvdSrcAddr, buffer+12, sizeof(uint32_t));
      CPPUNIT_ASSERT(rcvdSrcAddr == origin.to32bWord());
     }

    void testIPUDPPktDestinationAddr() {

      IPAddr origin, destination(3,1);
      ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
      uint32_t rcvdDstAddr = 0;
      memcpy(&rcvdDstAddr, buffer+16, sizeof(uint32_t));
      CPPUNIT_ASSERT(rcvdDstAddr == destination.to32bWord());
	}

	  void testIPUDPPktSourcePort() {

        IPAddr origin, destination;
        uint16_t sndSourcePort = 698;
        ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint16_t rcvdSourcePort = 0;
        memcpy(&rcvdSourcePort, buffer+20, sizeof(uint16_t));
		CPPUNIT_ASSERT(rcvdSourcePort == sndSourcePort);
	}

    void testIPUDPPktDestinationPort() {

        IPAddr origin, destination;
        uint16_t sndDstPort = 698;
		ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint16_t rcvdDstPort = 0;
        memcpy(&rcvdDstPort, buffer+22, sizeof(uint16_t));
		CPPUNIT_ASSERT(rcvdDstPort == sndDstPort);
     }

    void testIPUDPPktDataLength(){
        IPAddr origin, destination;
        uint16_t sndDataLength = 15;
	    ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint16_t rcvdDataLength = 0;
        memcpy(&rcvdDataLength, buffer+24, sizeof(uint16_t));
		CPPUNIT_ASSERT(rcvdDataLength == sndDataLength);
     }


    void testIPUDPPktUDPChecksum() {

        IPAddr origin, destination;
        uint16_t sndUdpCheckSum = 0;
        ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
		uint16_t rcvdUdpCheckSum = 0;
        memcpy(&rcvdUdpCheckSum, buffer+26, sizeof(uint16_t));
        CPPUNIT_ASSERT(rcvdUdpCheckSum == sndUdpCheckSum);

     }


 //*************************Start : Assignment 4 decoding part************************************//

     void testIPUDPHeaderDecodingLengthShorter() {
     IPAddr origin(169, 254, 94, 105), destination(169,254,255,255);
     int testShorterLenth = 10;
     //ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
     IPUDPPacket::HeaderInfo hi;
     CPPUNIT_ASSERT(ipudp->isValid(buffer,testShorterLenth,hi)==false);

  }

     void testIPUDPHeaderDecodingLengthLarger() {

     IPAddr origin(169, 254, 94, 105), destination(169,254,255,255);
     int testLargerLenth = 150;
     ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
     IPUDPPacket::HeaderInfo hi;
     CPPUNIT_ASSERT(ipudp->isValid(buffer,testLargerLenth,hi)==false);


  }


void testIPUDPHeaderDecodingHeaderLength(){

    IPAddr origin(169, 254, 94, 105), destination(169,254,255,255);
    int expectedRcvdHeaderLenth = 28;
   // ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
    IPUDPPacket::HeaderInfo hi;
    CPPUNIT_ASSERT(ipudp->isValid(buffer, sndHeaderLenth, hi));
    CPPUNIT_ASSERT(hi.headerLen == expectedRcvdHeaderLenth);

}


 void testIPUDPHeaderDecodingDataLength(){

     IPAddr origin(169, 254, 94, 105), destination(169,254,255,255);
    int expectedRcvdDataLenth = 48;
    //ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
    IPUDPPacket::HeaderInfo hi;
    CPPUNIT_ASSERT(ipudp->isValid(buffer, sndDataLength, hi));
    CPPUNIT_ASSERT(hi.dataLen == expectedRcvdDataLenth);

 }

 void  testIPUDPHeaderDecodingTTL(){

    IPAddr origin(169, 254, 94, 105), destination(169,254,255,255);
    int expectedTTL = 1;
    ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
    IPUDPPacket::HeaderInfo hi;
    CPPUNIT_ASSERT(ipudp->isValid(buffer, ipUdpPacketLength, hi));
    CPPUNIT_ASSERT(hi.TTL == expectedTTL);



   }

   void testIPUDPHeaderDecodingOrigin(){

    IPAddr origin(169,254,3,1), destination(169,254,255,255);
    // int expectedSourceAddr = IPAddr origin(169, 254, 94, 105);
   // ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
    uint16_t rcvSrcPrt = 70;
    //rcvSrcPrt = htons(rcvSrcPrt);
    IPUDPPacket::HeaderInfo hi;
    IPAddr rcvdSrc;
    rcvdSrc = origin;
    CPPUNIT_ASSERT(ipudp->isValid(buffer,rcvSrcPrt,hi) == true);

   }
  void  testIPUDPHeaderDecodingDestination(){

    IPAddr origin(169,254,3,1), destination(169,254,255,255);
    // int expectedDestinationAddr = 169,254,255,255;
    //ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
    uint16_t rcvDstPrt = 80;
    IPUDPPacket::HeaderInfo hi;
    IPAddr rcvdDest;
    rcvdDest = destination;
    CPPUNIT_ASSERT(ipudp->isValid(buffer,rcvDstPrt,hi) == true);
    //CPPUNIT_ASSERT(hi.dest ==  IPAddr origin(169, 254, 94, 105));
    }

    void testIPUDPHeaderDecodingCheckSumIpUDP()
    {
    IPAddr origin(169, 254, 94, 105), destination(169,254,255,255);
    // We calculated Check Sum by hand and fouhnd 24816
    // But here we just send first two bits to the decoder part and that is 24
    int sndCheckSum = 24;
    // ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
    IPUDPPacket::HeaderInfo hi;
    CPPUNIT_ASSERT(ipudp->isValid(buffer, sndCheckSum, hi) == true);
    }

     //*************************End : Assignment 4 decoding part************************************//
};

#endif

