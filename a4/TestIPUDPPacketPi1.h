#ifndef __TESTIPUDPPACKETPI1_H
#define __TESTIPUDPPACKETPI1_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <iostream>



#include "IPUDPPacketFactory.h"
#include "IPAddr.h"


/// Test fixture for ProgressiveRateStrategy
class TestIPUDPPacketPi1 : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(TestIPUDPPacketPi1);
	CPPUNIT_TEST(sampleTestIPUDPDataTooLarge);

	CPPUNIT_TEST(checkIPUdpVersion);
	CPPUNIT_TEST(checkIPUdpIhl);
	CPPUNIT_TEST(checkIPUdpTypeOfService);
	CPPUNIT_TEST(checkIPUdpTotalLength);
	CPPUNIT_TEST(checkIPUdpIdentification);
	CPPUNIT_TEST(checkIPUdpFlagsAndFragmentOffset);
	CPPUNIT_TEST(checkIPUdpTTL);
	CPPUNIT_TEST(checkIPUdpProtocol);
	CPPUNIT_TEST(checkIPUdpHeaderChecksumIP);
	CPPUNIT_TEST(checkIPUdpSourceAddress);
	CPPUNIT_TEST(checkIPUdpDestAddress);
	CPPUNIT_TEST(checkIPUdpSourcePort);
	CPPUNIT_TEST(checkIPUdpDestPort);
	CPPUNIT_TEST(checkIPUdpLengthUDP);
	CPPUNIT_TEST(checkIPUdpChecksumUDP);

	//Assignment-4
	CPPUNIT_TEST(testIPUDVersion);
	CPPUNIT_TEST(testIPUDihl);
	CPPUNIT_TEST(testTotalLength);
	CPPUNIT_TEST(testTimetoLive);
	CPPUNIT_TEST(testHeaderChecksumIP);
	CPPUNIT_TEST(testSourceAddr);
	CPPUNIT_TEST(testDestAddr);
	CPPUNIT_TEST(testUDPLength);

	CPPUNIT_TEST_SUITE_END();
private:
	// we make the factory static so that we can initialize it in the
	// .cc file to the concrete factory that returns the concrete object
	// with our implementation of setHeader()
	static std::shared_ptr<IPUDPPacketFactory> factory;

private:
	// the packet object being tested
	std::shared_ptr<IPUDPPacket> ipudp;
	// the buffer used for testing
	unsigned char * buffer;
	// the buffer length
	const int buflen = 1000;


public:

    uint8_t ver_ihl = 0x45;
    uint8_t typeOfSer = 0;
    uint16_t totalLen = 38;
    uint16_t identification = 0;
    uint16_t flag_offset = 0;
    uint8_t ttl = 1;
    uint8_t protocol = 138;
    uint16_t checksumIp = 0;
    uint32_t srcAddr;
    uint32_t dstAddr;
    uint16_t sourcePort = 698;
    uint16_t destPort = 698;
    uint16_t length = 38;
    uint16_t checksumUdp = 0;


	void setUp()  {
		if (factory)
			ipudp = factory->createIPUDPPacket();
		else {
	    std::cerr << "TestIPUDPSample test fixture not properly initialized!\n";
			exit(EXIT_FAILURE);
		}
		buffer = new unsigned char [buflen];
	};

	void tearDown()  {
		delete[] buffer;
		// we don't need to delete ipudp since it is a smart pointer
	};

	// *************** test cases ************
	void sampleTestIPUDPDataTooLarge() {
		IPAddr src(1,1), dst(4,2);
		int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 11);
		CPPUNIT_ASSERT(header_len == -1);
	}

	void checkIPUdpVersion() {
	    IPAddr src(1,1), dst(4,2);
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&ver_ihl,buffer,sizeof(ver_ihl));
		ver_ihl = (ver_ihl & 0xF0);
		CPPUNIT_ASSERT((ver_ihl == 0x40));
	}

	void checkIPUdpIhl() {
	    IPAddr src(1,1), dst(4,2);
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
        memcpy(&ver_ihl,buffer,sizeof(ver_ihl));
		ver_ihl = (ver_ihl & 0x0F);
		CPPUNIT_ASSERT((ver_ihl == 0x05));
	}

	void checkIPUdpTypeOfService() {

		IPAddr src(1,1), dst(4,2);
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&typeOfSer, buffer+1,sizeof(typeOfSer));
		CPPUNIT_ASSERT(typeOfSer == 0);
	}

	void checkIPUdpTotalLength() {

		IPAddr src(1,1), dst(4,2);
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&totalLen, buffer+2,sizeof(totalLen));
		CPPUNIT_ASSERT(totalLen == 38);
	}

	void checkIPUdpIdentification() {

		IPAddr src(1,1), dst(4,2);
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&identification, buffer+4,sizeof(identification));
		CPPUNIT_ASSERT(identification == 0);
	}

	void checkIPUdpFlagsAndFragmentOffset() {

		IPAddr src(1,1), dst(4,2);
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&flag_offset, buffer+6,sizeof(flag_offset));
		CPPUNIT_ASSERT(flag_offset == 0);
	}

	void checkIPUdpTTL() {

		IPAddr src(1,1), dst(4,2);
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&ttl, buffer+8,sizeof(ttl));
		CPPUNIT_ASSERT(ttl == 1);
	}

	void checkIPUdpProtocol() {

		IPAddr src(1,1), dst(4,2);
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&protocol, buffer+9,sizeof(protocol));
		CPPUNIT_ASSERT(protocol == 138);
	}

	void checkIPUdpHeaderChecksumIP() {

		IPAddr src(1,1), dst(4,2);
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&checksumIp, buffer+10,sizeof(checksumIp));
		CPPUNIT_ASSERT(checksumIp == 15222);
	}

	void checkIPUdpSourceAddress() {

		IPAddr src(1,1), dst(4,2);
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&srcAddr, buffer+12,sizeof(srcAddr));
		CPPUNIT_ASSERT(srcAddr == src.to32bWord());
	}

	void checkIPUdpDestAddress() {

		IPAddr src(1,1), dst(4,2);
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&dstAddr, buffer+16,sizeof(dstAddr));
		CPPUNIT_ASSERT(dstAddr == dst.to32bWord());
	}

	void checkIPUdpSourcePort() {

		IPAddr src(1,1), dst(4,2);
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&sourcePort, buffer+20,sizeof(sourcePort));
		CPPUNIT_ASSERT(sourcePort == 698);
	}

	void checkIPUdpDestPort() {

		IPAddr src(1,1), dst(4,2);
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&destPort, buffer+22,sizeof(destPort));
		CPPUNIT_ASSERT(destPort == 698);
	}

	void checkIPUdpLengthUDP() {

		IPAddr src(1,1), dst(4,2);
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&length, buffer+24,sizeof(length));
		CPPUNIT_ASSERT(length == 38);
	}

	void checkIPUdpChecksumUDP() {

		IPAddr src(1,1), dst(4,2);
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&checksumUdp, buffer+26,sizeof(checksumUdp));
		CPPUNIT_ASSERT(checksumUdp == 0);
	}



  // Assignment-4

  void testIPUDVersion(){
        IPAddr src(1,1), dst(4,2);
        ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
        IPUDPPacket::HeaderInfo hi;
		CPPUNIT_ASSERT(ipudp->isValid(buffer, 28+10, hi));
		hi.ver_ihl = (hi.ver_ihl & 0xF0);
		CPPUNIT_ASSERT((hi.ver_ihl == 0x40));
  }

  void testIPUDihl(){
        IPAddr src(1,1), dst(4,2);
        ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
        IPUDPPacket::HeaderInfo hi;
		CPPUNIT_ASSERT(ipudp->isValid(buffer, 28+10, hi));
		hi.ver_ihl = (hi.ver_ihl & 0x0F);
		CPPUNIT_ASSERT((hi.ver_ihl == 0x05));
  }

  void testTotalLength(){
        IPAddr src(1,1), dst(4,2);
        ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
        IPUDPPacket::HeaderInfo hi;
		CPPUNIT_ASSERT(ipudp->isValid(buffer, 28+10, hi));
		CPPUNIT_ASSERT(hi.totalLen == 38);

  }

  void testTimetoLive(){

        IPAddr src(1,1), dst(4,2);
        ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
        IPUDPPacket::HeaderInfo hi;
		CPPUNIT_ASSERT(ipudp->isValid(buffer, 28+10, hi));
        CPPUNIT_ASSERT(hi.timeToLive == 1);
  }

  void testHeaderChecksumIP(){

        IPAddr src(1,1), dst(4,2);
        ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
        IPUDPPacket::HeaderInfo hi;
		CPPUNIT_ASSERT(ipudp->isValid(buffer, 28+10, hi));
		CPPUNIT_ASSERT(hi.checksumIp == 15222);
  }

   void testSourceAddr(){

        IPAddr src(1,1), dst(4,2);
        ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
        IPUDPPacket::HeaderInfo hi;
		CPPUNIT_ASSERT(ipudp->isValid(buffer, 28+10, hi));
        CPPUNIT_ASSERT(hi.src == src.to32bWord());
  }

  void testDestAddr(){

        IPAddr src(1,1), dst(4,2);
        ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
        IPUDPPacket::HeaderInfo hi;
		CPPUNIT_ASSERT(ipudp->isValid(buffer, 28+10, hi));
        CPPUNIT_ASSERT(hi.dest == dst.to32bWord());
  }

  void testUDPLength(){

        IPAddr src(1,1), dst(4,2);
        ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
        IPUDPPacket::HeaderInfo hi;
		CPPUNIT_ASSERT(ipudp->isValid(buffer, 28+10, hi));
        CPPUNIT_ASSERT(hi.length == 38);
  }

};

#endif
