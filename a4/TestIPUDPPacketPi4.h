// Cody Barnson
// Group Pi4
// 4210 - Assignment 4

#ifndef __TESTIPUDPPACKETPI4_H
#define __TESTIPUDPPACKETPI4_H

#include <cppunit/TextTestRunner.h>
#include <cppunit/TextOutputter.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestListener.h>
#include <cppunit/extensions/TestSuiteBuilderContext.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/ui/text/TextTestRunner.h>

#include <cstdlib>
#include <iostream>
#include <memory>

#include <arpa/inet.h>

// CPSC4210 NOTE TO OTHER GROUPS:
// If having problems including these headers, change includes from
// #include <...> (which includes from directories know to the project path)
// to #include "..." (which includes relative to this file making the include)
#include "IPUDPPacket.h"
#include "IPUDPPacketFactory.h"
#include "IPAddr.h"


/// Test fixture for ProgressiveRateStrategy
class TestIPUDPPacketPi4 : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(TestIPUDPPacketPi4);
  CPPUNIT_TEST(testIPUDPPacketDataTooLarge);
  CPPUNIT_TEST(testIPUDPPacketIPFieldVersionIs4);
  CPPUNIT_TEST(testIPUDPPacketIPFieldIHLIs5);
  CPPUNIT_TEST(testIPUDPPacketIPFieldTypeOfServiceIsZero);
  CPPUNIT_TEST(testIPUDPPacketIPFieldTotalLengthNTOHS);
  CPPUNIT_TEST(testIPUDPPacketIPFieldIdentificationIsZero);
  CPPUNIT_TEST(testIPUDPPacketIPFieldFlagAndOffsetAreZero);
  CPPUNIT_TEST(testIPUDPPacketIPFieldTTLIsOne);
  CPPUNIT_TEST(testIPUDPPacketIPFieldProtocolIs138);
  CPPUNIT_TEST(testIPAddrDefaultConstructor);
  CPPUNIT_TEST(testIPAddrConstructorPiNodeParam);
  CPPUNIT_TEST(testIPUDPPacketIPFieldSourceAddress);
  CPPUNIT_TEST(testIPUDPPacketIPFieldDestinationAddress);
  CPPUNIT_TEST(testIPUDPPacketIPFieldVerifyChecksum);
  CPPUNIT_TEST(testIPUDPPacketUDPFieldSourcePort);
  CPPUNIT_TEST(testIPUDPPacketUDPFieldDestinationPort);
  CPPUNIT_TEST(testIPUDPPacketUDPFieldLength);
  CPPUNIT_TEST(testIPUDPPacketUDPFieldChecksumZero);

  // 4210 - Assignment 4 - test cases
  CPPUNIT_TEST(testIPUDPPacketIsValidBufferLengthTooSmall);
  CPPUNIT_TEST(testIPUDPPacketIsValidCorrectHeaderLen);
  CPPUNIT_TEST(testIPUDPPacketIsValidCorrectDataLength);
  CPPUNIT_TEST(testIPUDPPacketIsValidUDPHeaderSourcePortFailIfNot698);
  CPPUNIT_TEST(testIPUDPPacketIsValidUDPHeaderDestinationPortFailIfNot698);
  CPPUNIT_TEST(testIPUDPPacketIsValidFailIfIncorrectChecksum);
  CPPUNIT_TEST(testIPUDPPacketIsValidTTLFailIfNotOne);
  
  CPPUNIT_TEST_SUITE_END();
private:
  // we make the factory static so that we can initialize it in the
  // .cc file to the concrete factory that returns the concrete object
  // with our implementation of setHeader()
  static std::shared_ptr<IPUDPPacketFactory> factory;

private:
  // the packet object being tested
  std::shared_ptr<IPUDPPacket> ipudp;
  // the buffer used for testing
  unsigned char * buffer;
  // the buffer length
  const int buflen = 1000;
	

public:
  void setUp()  {
    if (factory)
      ipudp = factory->createIPUDPPacket();
    else {
      std::cerr << "TestIPUDPSample test fixture not properly initialized!\n";
      exit(EXIT_FAILURE);
    }
    buffer = new unsigned char [buflen];
  };
	
  void tearDown()  {
    delete[] buffer;
    // we don't need to delete ipudp since it is a smart pointer
  };

  // *************** test cases ************
  void testIPUDPPacketDataTooLarge() {
    IPAddr src,dst;
    int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 11);
    CPPUNIT_ASSERT(header_len == -1);
  }

  void testIPUDPPacketIPFieldVersionIs4() {
    IPAddr src, dst;
    int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
    uint8_t ip = (buffer[0] & 0xF0);
    CPPUNIT_ASSERT(ip == 0x40);
  }

  void testIPUDPPacketIPFieldIHLIs5() {
    IPAddr src, dst;
    int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
    uint8_t ihl = (buffer[0] & 0x0F);
    CPPUNIT_ASSERT(ihl == 0x05);
  }

  void testIPUDPPacketIPFieldTypeOfServiceIsZero() {
    IPAddr src, dst;
    int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
    uint8_t tos = buffer[1];
    CPPUNIT_ASSERT(tos == 0x00);
  }

  void testIPUDPPacketIPFieldTotalLengthNTOHS() {
    IPAddr src, dst;
    int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
    uint16_t len = 0;
    memcpy(&len, buffer+2, sizeof(uint16_t));
    len = ntohs(len);
    CPPUNIT_ASSERT(len == 38);
  }

  void testIPUDPPacketIPFieldIdentificationIsZero() {
    IPAddr src, dst;
    int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
    uint16_t id = 0;
    memcpy(&id, buffer+4, sizeof(uint16_t));
    id = ntohs(id);
    CPPUNIT_ASSERT(id == 0);
  }

  void testIPUDPPacketIPFieldFlagAndOffsetAreZero() {
    IPAddr src, dst;
    int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
    uint16_t f = 0;
    memcpy(&f, buffer+6, sizeof(uint16_t));
    f = ntohs(f);
    CPPUNIT_ASSERT(f == 0);
  }

  void testIPUDPPacketIPFieldTTLIsOne() {
    IPAddr src, dst;
    int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
    uint8_t ttl = buffer[8];
    CPPUNIT_ASSERT(ttl == 0x01);      
  }

  void testIPUDPPacketIPFieldProtocolIs138() {
    IPAddr src, dst;
    int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
    uint8_t protocol = buffer[9];
    CPPUNIT_ASSERT(protocol == 138);
  }

  void testIPAddrDefaultConstructor() {
    IPAddr src;
    unsigned char arr[4] = { 169, 254, 255, 255 };
    uint32_t arr_addr = *((uint32_t*)arr);
    CPPUNIT_ASSERT(src.to32bWord() == arr_addr);
  }

  void testIPAddrConstructorPiNodeParam() {
    IPAddr src(4, 2);
    unsigned char arr[4] = { 169, 254, 4, 2 };
    uint32_t arr_addr = *((uint32_t*)arr);
    CPPUNIT_ASSERT(src.to32bWord() == arr_addr);
  }

  void testIPUDPPacketIPFieldSourceAddress() { // bytes 12, 13, 14, 15
    IPAddr src(4, 2), dst;
    int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
    uint32_t ip_addr = 0;
    memcpy(&ip_addr, buffer+12, sizeof(uint32_t));
    ip_addr = ntohl(ip_addr);
    CPPUNIT_ASSERT(ip_addr == src.to32bWord());
  }
   
  void testIPUDPPacketIPFieldDestinationAddress() { 
    IPAddr src, dst(4, 2);
    int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
    uint32_t ip_addr = 0;
    memcpy(&ip_addr, buffer+16, sizeof(uint32_t));
    ip_addr = ntohl(ip_addr);
    CPPUNIT_ASSERT(ip_addr == dst.to32bWord());
  }

  void testIPUDPPacketIPFieldVerifyChecksum() { // bytes 10 & 11
    // version 4, IHL 5, total length 38
    // 01000101 00000000 00000000 00100110
    // 00000000 00000000 00000000 00000000
    // 00000001 10001010 00000000 00000000 , header checksum for calc is 0
    // 11111111 11111111 11111110 10101001
    // 11111111 11111111 11111110 10101001
      
    // defaults 10101001.11111110  11111111.11111111, note reversal for byte order
     IPAddr src(169, 254, 255, 255), dst(169, 254, 255, 255);
    int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
    uint16_t cmp_check = 0;
    memcpy(&cmp_check, buffer+10, sizeof(uint16_t));
    uint16_t arr[10];
    memset(arr, 0, sizeof(arr));
      
    memcpy(arr, buffer, 20);
    arr[5] = 0; // checksum for compution is 0
    uint32_t sum = 0;
    for (int i = 0; i < 10; i++) {
      sum += (uint32_t)arr[i];
      if ((sum >> 16) & 1) {
        sum &= ~(1 << 16);
        sum += 1;
      }
    }
    // 'sum' is our one's complement sum.
    uint16_t result = ~(sum); // the "ones complement"
      
    CPPUNIT_ASSERT(cmp_check == result);
  }

  void testIPUDPPacketUDPFieldSourcePort() {
    IPAddr src, dst;
    int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
    uint16_t port = 0;
    memcpy(&port, buffer+20, sizeof(uint16_t));
    port = ntohs(port);
    CPPUNIT_ASSERT(port == 698);
  }

  void testIPUDPPacketUDPFieldDestinationPort() {
    IPAddr src, dst;
    int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
    uint16_t port = 0;
    memcpy(&port, buffer+22, sizeof(uint16_t));
    port = ntohs(port);
    CPPUNIT_ASSERT(port == 698);
  }

  void testIPUDPPacketUDPFieldLength() {
    IPAddr src, dst;
    int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
    uint16_t len = 0;
    memcpy(&len, buffer+24, sizeof(uint16_t));
    len = ntohs(len);
    CPPUNIT_ASSERT(len == (8 + 10));
  }

  void testIPUDPPacketUDPFieldChecksumZero() {
    IPAddr src, dst;
    int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
    uint16_t checksum = 0;
    memcpy(&checksum, buffer+26, sizeof(uint16_t));
    checksum = ntohs(checksum);
    CPPUNIT_ASSERT(checksum == 0);
  }


 

  void testIPUDPPacketIsValidBufferLengthTooSmall() {
     IPAddr src(169, 254, 1, 1), dst(169, 254, 255, 255);
     // ttl is 1, datalen is 10
     int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
     IPUDPPacket::HeaderInfo hi;
     CPPUNIT_ASSERT(!ipudp->isValid(buffer, 10, hi));
  }

  void testIPUDPPacketIsValidCorrectHeaderLen() {
     // unsigned char b[120] = { 0x45,               // version and header length
     //                          0, 0, 120,          // type of service, total length
     //                          0, 0, 0, 0,         // identification
     //                          1, 138,             // TTL, protocol
     //                          0, 0,               // checksum
     //                          169, 254, 1, 1,     // src address
     //                          169, 254, 255, 255, // dst address
     //                          0x2, 0xba,          // src port
     //                          0x2, 0xba,          // dst port
     //                          0, 100,             // length of udp
     //                          0, 0,               // checksum
     // };

     IPAddr src(169, 254, 1, 1), dst(169, 254, 255, 255);
     // ttl is 1, datalen is 10
     int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
     IPUDPPacket::HeaderInfo hi;
     CPPUNIT_ASSERT(ipudp->isValid(buffer, 28+10, hi));
     CPPUNIT_ASSERT(hi.headerLen == 28);
    
    
     // IPUDPPacket::HeaderInfo hi;
     // CPPUNIT_ASSERT(ipudp->isValid(b, 120, hi));
     // CPPUNIT_ASSERT(hi.src == IPAddr(169, 254, 1, 1));
     // ... test other fields of header info structure
  }
  
  void testIPUDPPacketIsValidCorrectDataLength() {
     IPAddr src(169, 254, 1, 1), dst(169, 254, 255, 255);
     // ttl is 1, datalen is 10
     int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
     IPUDPPacket::HeaderInfo hi;
     CPPUNIT_ASSERT(ipudp->isValid(buffer, 28+10, hi));
     CPPUNIT_ASSERT(hi.dataLen == 10);
  }

  // use setHeader, then change UDP field for source port, ensure IsValid
  // with fail
  void testIPUDPPacketIsValidUDPHeaderSourcePortFailIfNot698() {
     IPAddr src(169, 254, 1, 1), dst(169, 254, 255, 255);
     // ttl is 1, datalen is 10
     int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
     // modify bytes 20 to 21
     uint16_t port = 123;
     port = htons(port);
     memcpy(buffer+20, &port, sizeof(uint16_t));     
     IPUDPPacket::HeaderInfo hi;
     CPPUNIT_ASSERT(ipudp->isValid(buffer, 28+10, hi) == false);     
  }

  // use setHeader, then change UDP field for destination port, ensure IsValid
  // with fail
  void testIPUDPPacketIsValidUDPHeaderDestinationPortFailIfNot698() {       
     IPAddr src(169, 254, 1, 1), dst(169, 254, 255, 255);
     // ttl is 1, datalen is 10
     int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
     // modify bytes 22 to 23
     uint16_t port = 123;
     port = htons(port);
     memcpy(buffer+22, &port, sizeof(uint16_t));     
     IPUDPPacket::HeaderInfo hi;
     CPPUNIT_ASSERT(ipudp->isValid(buffer, 28+10, hi) == false);  
  }

  void testIPUDPPacketIsValidFailIfIncorrectChecksum() {
     // version 4, IHL 5, total length 38
     // 01000101 00000000 00000000 00100110
     // 00000000 00000000 00000000 00000000
     // 00000001 10001010 00000000 00000000 , header checksum for calc is 0
     // 11111111 11111111 11111110 10101001
     // 11111111 11111111 11111110 10101001
      
     // defaults 10101001.11111110  11111111.11111111, note reversal for byte order
     IPAddr src(169, 254, 255, 255), dst(169, 254, 255, 255);
     int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
     uint16_t cmp_check = 0;
     memcpy(&cmp_check, buffer+10, sizeof(uint16_t));     
     uint16_t result = ip_checksum(buffer, 20);
     CPPUNIT_ASSERT(cmp_check == result);

     // now modify the result, such that it is not equal to cmp_check,
     // clear IP header checksum field, write this "fake" value into the field
     // call IsValid, ensure is false
     result ^= result;
     CPPUNIT_ASSERT(cmp_check != result);

     // clear existing field
     memset(buffer+10, 0, sizeof(uint16_t));

     // write in new value
     memcpy(buffer+10, &result, sizeof(uint16_t));

     // check that it fails
     IPUDPPacket::HeaderInfo hi;
     CPPUNIT_ASSERT(ipudp->isValid(buffer, 28+10, hi) == false);
  }
  
  void testIPUDPPacketIsValidTTLFailIfNotOne() {
     IPAddr src(169, 254, 1, 1), dst(169, 254, 255, 255);
     // ttl is 1, datalen is 10
     int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
     // modify byte 8
     uint8_t fake_ttl = 0;     
     memcpy(buffer+8, &fake_ttl, sizeof(uint8_t));     
     IPUDPPacket::HeaderInfo hi;
     CPPUNIT_ASSERT(ipudp->isValid(buffer, 28+10, hi) == false); 
  }


  // helper function
  uint16_t ip_checksum(unsigned char *buf, size_t hdr_len) {
     unsigned long sum = 0;
     /* uint16_t *ip1; */

     uint16_t arr[10];
     memset(arr, 0, sizeof(arr));
     memcpy(arr, buf, 20);
     arr[5] = 0; // checksum for compution is 0
    
     /* ip1 = buf; */
     int i = 0;
     while (hdr_len > 1)
     {
	/* sum += *ip1++; */
	sum += arr[i++];
	if (sum & 0x80000000)
	   sum = (sum & 0xFFFF) + (sum >> 16);
	hdr_len -= 2;
     }
      
     while (sum >> 16)
	sum = (sum & 0xFFFF) + (sum >> 16);
      
     return(~sum);
  }

};

#endif
