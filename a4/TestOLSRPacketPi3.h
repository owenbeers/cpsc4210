
#ifndef __TESTOLSRPACKETPI3_H
#define __TESTOLSRPACKETPI3_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <bitset>
#include "OLSRPacketFactory.h"
#include "IPAddr.h"

using namespace std;


/// Test fixture for ProgressiveRateStrategy
class TestOLSRPacketPi3 : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(TestOLSRPacketPi3);
	CPPUNIT_TEST(sampleTestOLSRPacketTooLarge);


    CPPUNIT_TEST(testOLSRPktLength);
	CPPUNIT_TEST(testOLSRPktSeqNumber);
	CPPUNIT_TEST(testOLSRPktMsgType);
	CPPUNIT_TEST(testOLSRPktValidityTime);
    CPPUNIT_TEST(testOLSRPktMsgSize);
	CPPUNIT_TEST(testOLSRPktOriginatorAddr);
    CPPUNIT_TEST(testOLSRPktTimeToLive);
    CPPUNIT_TEST(testOLSRPktHopCount);
    CPPUNIT_TEST(testOLSRPktMsgSeqNumber);
   // Assignement 4 testing function starts from here

    CPPUNIT_TEST(testOLSRpktDecodingHeaderLength);
    CPPUNIT_TEST(testOLSRpktDecodingDataLength);
    CPPUNIT_TEST(testOLSRPktDecodingOriginatorAddr);
    CPPUNIT_TEST(testOLSRPktDecodingOMsgSize);

	CPPUNIT_TEST_SUITE_END();
private:
	// we make the factory static so that we can initialize it in the
	// .cc file to the concrete factory that returns the concrete object
	// with our implementation of setHeader()
	static std::shared_ptr<OLSRPacketFactory> factory;

private:
	// the packet object being tested
	std::shared_ptr<OLSRPacket> olsrp;
	// the buffer used for testing
	unsigned char * buffer;
	// the buffer length
	const int buflen = 1000;

    uint16_t packHeaderLength = 16;
	uint16_t packetSeqNum = 11;
	uint8_t  messageType = 1;
	uint8_t  vTime = 6;
	uint16_t messageSize = 25;
	uint8_t timeToLive = 1;
	uint8_t hopCount = 0;
	uint16_t messageSeqNum = 22;
    uint16_t sndActualPacketLength = packHeaderLength + messageSize ; // According to RFC 3626 document: assuming message Size will be 25

public:
	int psn, msn = 0;

	void setUp()  {
		if (factory){
			olsrp = factory->createOLSRPacket();

		}
		else {
	    std::cerr << "TestIPUDPSample test fixture not properly initialized!\n";
			exit(EXIT_FAILURE);
		}
		buffer = new unsigned char [buflen];
	};

	void tearDown()  {
		delete[] buffer;
		// we don't need to delete ipudp since it is a smart pointer
	};

	// *************** test cases ************
	void sampleTestOLSRPacketTooLarge() {

		IPAddr origin;
		int header_len = olsrp->setHeader(buffer, buflen, 1, origin, 1, 0, 33, 66, 11);
		CPPUNIT_ASSERT(header_len == -1);
	}

    void testOLSRPktLength() {

        IPAddr origin;


		olsrp->setHeader(buffer, packHeaderLength, messageType, origin, timeToLive, hopCount, packetSeqNum, messageSeqNum, messageSize);
        uint16_t rcvdActualPacketLength = 0;
        memcpy(&rcvdActualPacketLength, buffer, sizeof(uint16_t));
        CPPUNIT_ASSERT(rcvdActualPacketLength == sndActualPacketLength);
     }

    void testOLSRPktSeqNumber(){

       IPAddr origin;
       uint16_t sndPacketSeqNum = 11;
       olsrp->setHeader(buffer, packHeaderLength, messageType, origin, timeToLive, hopCount, packetSeqNum, messageSeqNum, messageSize);
       uint16_t rcvdPacketSeqNum = 0;
       memcpy(&rcvdPacketSeqNum, buffer+2, sizeof(uint16_t));
       CPPUNIT_ASSERT(rcvdPacketSeqNum == sndPacketSeqNum);
     }

    void testOLSRPktMsgType() {

		IPAddr origin;
		uint8_t sndMessageType = 1;
		olsrp->setHeader(buffer, packHeaderLength, messageType, origin, timeToLive, hopCount, packetSeqNum, messageSeqNum, messageSize);
		uint8_t rcvdMessageType = 0;
        memcpy(&rcvdMessageType, buffer+4, sizeof(uint8_t));
        CPPUNIT_ASSERT(rcvdMessageType == sndMessageType);
	}

	  void testOLSRPktValidityTime() {

		IPAddr origin;
		uint8_t sndValidityTime = 6;
		olsrp->setHeader(buffer, packHeaderLength, messageType, origin, timeToLive, hopCount, packetSeqNum, messageSeqNum, messageSize);
		uint8_t rcvdValidityTime= 0;
        memcpy(&rcvdValidityTime, buffer+5, sizeof(uint8_t));
        CPPUNIT_ASSERT(rcvdValidityTime == sndValidityTime);
	}

    void testOLSRPktMsgSize() {

        IPAddr origin;
        uint16_t sndMessageSize = 25;
		olsrp->setHeader(buffer, packHeaderLength, messageType, origin, timeToLive, hopCount, packetSeqNum, messageSeqNum, messageSize);
		uint16_t rcvdMessageSize= 0;
        memcpy(&rcvdMessageSize, buffer+6, sizeof(uint16_t));
        CPPUNIT_ASSERT(rcvdMessageSize == sndMessageSize);
     }

    void testOLSRPktOriginatorAddr(){
        unsigned char piNum = '3', nodeNum = '1';
       IPAddr origin(piNum, nodeNum);
       olsrp->setHeader(buffer, packHeaderLength, messageType, origin, timeToLive, hopCount, packetSeqNum, messageSeqNum, messageSize);
       uint32_t rcvdSrcAddr = 0;
       memcpy(&rcvdSrcAddr, buffer+8, sizeof(uint32_t));
       CPPUNIT_ASSERT(rcvdSrcAddr == origin.to32bWord());
     }

    void testOLSRPktTimeToLive() {

		IPAddr origin;
		uint8_t sndTimeToLive = 1;
        olsrp->setHeader(buffer, packHeaderLength, messageType, origin, timeToLive, hopCount, packetSeqNum, messageSeqNum, messageSize);
		uint8_t rcvdTimeToLive = 0;
        memcpy(&rcvdTimeToLive, buffer+12, sizeof(uint8_t));
        CPPUNIT_ASSERT(rcvdTimeToLive == sndTimeToLive);
	}

    void testOLSRPktHopCount() {

        IPAddr origin;
        uint8_t sndHopCount = 0;
		olsrp->setHeader(buffer, packHeaderLength, messageType, origin, timeToLive, hopCount, packetSeqNum, messageSeqNum, messageSize);
		uint8_t rcvdHopCount = 0;
        memcpy(&rcvdHopCount, buffer+13, sizeof(uint8_t));
        CPPUNIT_ASSERT(rcvdHopCount == sndHopCount);
     }

    void testOLSRPktMsgSeqNumber(){

       IPAddr origin;
       uint16_t sndMsgSeqNum = 22;
       olsrp->setHeader(buffer, packHeaderLength, messageType, origin, timeToLive, hopCount, packetSeqNum, messageSeqNum, messageSize);
       uint16_t rcvdMsgSeqNum = 22;
       memcpy(&rcvdMsgSeqNum, buffer+14, sizeof(uint16_t));
       CPPUNIT_ASSERT(rcvdMsgSeqNum == sndMsgSeqNum);
     }


//*************************Start : Assignment 4 decoding part************************************//

void testOLSRpktDecodingHeaderLength(){

    IPAddr origin(169, 254, 94, 105), destination(169,254,255,255);
    int expectedHeaderLenth = 16;
    //olsrp->setHeader(buffer, packHeaderLength, messageType, origin, timeToLive, hopCount, packetSeqNum, messageSeqNum, messageSize);
    OLSRPacket::HeaderInfo hi;
    CPPUNIT_ASSERT(olsrp->isValid(buffer, expectedHeaderLenth, hi));
    CPPUNIT_ASSERT(hi.headerLen == expectedHeaderLenth);

}


 void testOLSRpktDecodingDataLength(){

     IPAddr origin(169, 254, 94, 105), destination(169,254,255,255);
    int expectedDataLenth = 41;
    //olsrp->setHeader(buffer, packHeaderLength, messageType, origin, timeToLive, hopCount, packetSeqNum, messageSeqNum, messageSize);
    OLSRPacket::HeaderInfo hi;
    CPPUNIT_ASSERT(olsrp->isValid(buffer, sndActualPacketLength, hi));
    CPPUNIT_ASSERT(hi.dataLen == expectedDataLenth);

 }

   void testOLSRPktDecodingOMsgSize() {
      IPAddr origin(169, 254, 94, 105);
      //olsrp->setHeader(buffer, packHeaderLength, messageType, origin, timeToLive, hopCount, packetSeqNum, messageSeqNum, messageSize);
      uint16_t Ckpacketlen = 100;
      //Ckpacketlen = htons(Ckpacketlen);
      memcpy(buffer, &Ckpacketlen, sizeof(uint16_t));
      OLSRPacket::HeaderInfo hi;
      CPPUNIT_ASSERT(olsrp->isValid(buffer,Ckpacketlen, hi) == false);

   }

      void testOLSRPktDecodingOriginatorAddr() {

        IPAddr origin(169, 254, 3, 1);
        //olsrp->setHeader(buffer, packHeaderLength, messageType, origin, timeToLive, hopCount, packetSeqNum, messageSeqNum, messageSize);
        OLSRPacket::HeaderInfo hi;
        uint32_t OrgAddr = 45;
        //memcpy(buffer+8, &OrgAddr, sizeof(uint32_t));
        //OrgAddr = htons(OrgAddr);
         IPAddr rcvdSrc;
         rcvdSrc = origin;
        CPPUNIT_ASSERT(olsrp->isValid(buffer,OrgAddr,hi) == true);


   }

};

#endif
