// *** CPSC 4210/5210 Assignment 3 code
// ***** CPPUnit fixture class sample for creating headers for IP/UDP packets ***

#ifndef __TESTOLSRPACKETPIREED_H
#define __TESTOLSRPACKETPIREED_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <iostream>

#include "OLSRPacketFactory.h"
#include "OLSRPacket.h"
#include "IPAddr.h"

#include <netinet/in.h>


/// Test fixture for ProgressiveRateStrategy
class TestOLSRPacketPiReed : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(TestOLSRPacketPiReed);
	CPPUNIT_TEST(TestOLSRDataTooLarge);
	CPPUNIT_TEST(TestOLSRPacketLength);
	CPPUNIT_TEST(TestOLSRpsn);
	CPPUNIT_TEST(TestOLSRmsgType);
	CPPUNIT_TEST(TestOLSRvtime);
	CPPUNIT_TEST(TestOLSRmsgSize);
	CPPUNIT_TEST(TestOLSRorigin);
	CPPUNIT_TEST(TestOLSRttl);
	CPPUNIT_TEST(TestOLSRhop);
	CPPUNIT_TEST(TestOLSRmsn);



CPPUNIT_TEST_SUITE_END();
private:
	// we make the factory static so that we can initialize it in the
	// .cc file to the concrete factory that returns the concrete object
	// with our implementation of setHeader()
	static std::shared_ptr<OLSRPacketFactory> factory;

private:
	// the packet object being tested
	std::shared_ptr<OLSRPacket> olsr;
	// the buffer used for testing
	unsigned char * buffer;
	// the buffer length
	const int buflen = 1000;
	
	int msg_type = 1;
	int TTL = 1;
	int hop_count= 0;
	int psn = 10;
	uint16_t msn = 1;
	int msglen = 10;
	IPAddr origin;

public:
	void setUp()  {
		if (factory)
			olsr = factory->createOLSRPacket();
		else {
	    std::cerr << "TestOLSRPacket test fixture not properly initialized!\n";
			exit(EXIT_FAILURE);
		}
		buffer = new unsigned char [buflen];
	};
	
	void tearDown()  {
		delete[] buffer;
		// we don't need to delete olsr since it is a smart pointer
	};

	// *************** test cases ************
	void TestOLSRDataTooLarge() {
		int header_len = olsr->setHeader(buffer, 16, msg_type, origin, TTL, hop_count, psn, msn, msglen);
		CPPUNIT_ASSERT(header_len == -1);
	}
   
   void TestOLSRPacketLength() {
		
		msglen = 10;
		olsr->setHeader(buffer, buflen, msg_type, origin, TTL, hop_count, psn, msn, msglen);

		uint16_t got;
		std::memcpy(&got, buffer+0, sizeof(uint16_t));

		//The packet length should be 16 (header) and 10 (data)
		CPPUNIT_ASSERT(got == 16+10);
		
	}
   
   	void TestOLSRpsn() {
		olsr->setHeader(buffer, buflen, msg_type, origin, TTL, hop_count, psn, msn, msglen);

		uint16_t got;
		std::memcpy(&got, buffer+2, sizeof(uint16_t));

		CPPUNIT_ASSERT(got == 10);
	}


   	void TestOLSRmsgType() {
		olsr->setHeader(buffer, buflen, msg_type, origin, TTL, hop_count, psn, msn, msglen);
      
      		uint8_t got;
      		std::memcpy(&got, buffer+4, sizeof(uint8_t));

		CPPUNIT_ASSERT(got == 1);
	}

   	void TestOLSRvtime() {
		olsr->setHeader(buffer, buflen, msg_type, origin, TTL, hop_count, psn, msn, msglen);
      
      		uint8_t got;
      		std::memcpy(&got, buffer+5, sizeof(uint8_t));

		CPPUNIT_ASSERT(got == 6);
   	}

	void TestOLSRmsgSize() {
		msglen = 20;
		olsr->setHeader(buffer, buflen, msg_type, origin, TTL, hop_count, psn, msn, msglen);
      
      		uint16_t got;
      		std::memcpy(&got, buffer+6, sizeof(uint16_t));

		
		//Message size should be equal to message header portion (12) and msglen (20)
		CPPUNIT_ASSERT(got == 12+20);
   	}
	

   	void TestOLSRorigin() {
		IPAddr temp(9, 1);
		origin = temp;
		olsr->setHeader(buffer, buflen, msg_type, origin, TTL, hop_count, psn, msn, msglen);
      
      		uint32_t got;
      		std::memcpy(&got, buffer+8, sizeof(uint32_t));

		CPPUNIT_ASSERT(got == temp.to32bWord());
   	}

   	void TestOLSRttl() {
		olsr->setHeader(buffer, buflen, msg_type, origin, TTL, hop_count, psn, msn, msglen);
      
      		uint8_t got;
      		std::memcpy(&got, buffer+12, sizeof(uint8_t));

		CPPUNIT_ASSERT(got == 1);
   	}

   	void TestOLSRhop() {
		olsr->setHeader(buffer, buflen, msg_type, origin, TTL, hop_count, psn, msn, msglen);
      
      		uint8_t got;
      		std::memcpy(&got, buffer+13, sizeof(uint8_t));

		CPPUNIT_ASSERT(got == 0);
   	}

   	void TestOLSRmsn() {
		olsr->setHeader(buffer, buflen, msg_type, origin, TTL, hop_count, psn, msn, msglen);
      
      		uint16_t got;
      		std::memcpy(&got, buffer+14, sizeof(uint16_t));

		CPPUNIT_ASSERT(got == 1);
   	}

	void TestOLSRHeaderValid() {
		olsr->setHeader(buffer, buflen, msg_type, origin, TTL, hop_count, psn, msn, msglen);
		OLSRPacket::HeaderInfo hi;
		CPPUNIT_ASSERT(olsr->isValid(buffer, buflen, hi));
		CPPUNIT_ASSERT(hi.headerLen == 16);
		CPPUNIT_ASSERT(hi.dataLen == buflen-hi.headerLen);
      		CPPUNIT_ASSERT(hi.org == origin);
	}

   	void TestInvalidMessageType() {
 		olsr->setHeader(buffer, buflen, msg_type, origin, TTL, hop_count, psn, msn, msglen);
     		uint8_t temp8;
      		memcpy(&temp8, buffer+4, sizeof(uint8_t));
      		CPPUNIT_ASSERT(temp8 == 1);
   	}

   	void TestInvalidNumMessages() {
 		olsr->setHeader(buffer, buflen, msg_type, origin, TTL, hop_count, psn, msn, msglen);
      		uint16_t temp16;
      		memcpy(&temp16, buffer+6, sizeof(uint16_t));
      		CPPUNIT_ASSERT(temp16+4==buflen);
   	}

};

#endif
