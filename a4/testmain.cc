// *** CPSC 4210/5210 Assignment 3 code
// *** main file for the packet construction test cases

#include <cppunit/ui/text/TestRunner.h>
#include "ConcreteIPUDPPacketFactory.h"
#include "ConcreteOLSRHelloFactory.h"
#include "ConcreteOLSRPacketFactory.h"

// Include the test fixture header files here
// #include "TestIPUDPPacketPi1.h"
// #include "TestOLSRHelloPi1.h"
// #include "TestOLSRPacketPi1.h"

#include "TestIPUDPPacketPi2.h"
#include "TestOLSRHelloPi2.h"
#include "TestOLSRPacketPi2.h"

#include "TestIPUDPPacketPi3.h"
#include "TestOLSRHelloPi3.h"
#include "TestOLSRPacketPi3.h"

#include "TestIPUDPPacketPi4.h"
#include "TestOLSRHelloPi4.h"
#include "TestOLSRPacketPi4.h"

#include "TestIPUDPPacketPiReed.h"
#include "TestOLSRHelloPiReed.h"
#include "TestOLSRPacketPiReed.h"

#include "TestIPUDPPacketPiReed.h"
#include "TestOLSRHelloPiReed.h"
#include "TestOLSRPacketPiReed.h"


// //Pi 1
// std::shared_ptr<IPUDPPacketFactory> TestIPUDPPacketPi1::factory =
// 	std::make_shared<ConcreteIPUDPPacketFactory>();

// std::shared_ptr<OLSRHelloFactory> TestOLSRHelloPi1::factory =
// 	std::make_shared<ConcreteOLSRHelloFactory>();

// std::shared_ptr<OLSRPacketFactory> TestOLSRPacketPi1::factory =
// 	std::make_shared<ConcreteOLSRPacketFactory>();

//Pi 2
// Initialize the static factory member of the test fixtures here
std::shared_ptr<IPUDPPacketFactory> TestIPUDPPacketPi2::factory =
	std::make_shared<ConcreteIPUDPPacketFactory>();

std::shared_ptr<OLSRHelloFactory> TestOLSRHelloPi2::factory =
	std::make_shared<ConcreteOLSRHelloFactory>();

std::shared_ptr<OLSRPacketFactory> TestOLSRPacketPi2::factory =
	std::make_shared<ConcreteOLSRPacketFactory>();

// Pi 3
// Initialize the static factory member of the test fixtures here
std::shared_ptr<IPUDPPacketFactory> TestIPUDPPacketPi3::factory =
	std::make_shared<ConcreteIPUDPPacketFactory>();

std::shared_ptr<OLSRHelloFactory> TestOLSRHelloPi3::factory =
	std::make_shared<ConcreteOLSRHelloFactory>();

std::shared_ptr<OLSRPacketFactory> TestOLSRPacketPi3::factory =
	std::make_shared<ConcreteOLSRPacketFactory>();

//Pi 4
std::shared_ptr<IPUDPPacketFactory> TestIPUDPPacketPi4::factory =
	std::make_shared<ConcreteIPUDPPacketFactory>();

std::shared_ptr<OLSRHelloFactory> TestOLSRHelloPi4::factory =
	std::make_shared<ConcreteOLSRHelloFactory>();

std::shared_ptr<OLSRPacketFactory> TestOLSRPacketPi4::factory =
	std::make_shared<ConcreteOLSRPacketFactory>();

<<<<<<< HEAD
//Pi 5
=======
//Pi Reed
>>>>>>> 065541cff7ddf6ff7c2c0f1e5674777452216ccd
std::shared_ptr<IPUDPPacketFactory> TestIPUDPPacketPiReed::factory =
	std::make_shared<ConcreteIPUDPPacketFactory>();

std::shared_ptr<OLSRHelloFactory> TestOLSRHelloPiReed::factory =
	std::make_shared<ConcreteOLSRHelloFactory>();
<<<<<<< HEAD
=======

std::shared_ptr<OLSRPacketFactory> TestOLSRPacketPiReed::factory =
	std::make_shared<ConcreteOLSRPacketFactory>();

>>>>>>> 065541cff7ddf6ff7c2c0f1e5674777452216ccd

std::shared_ptr<OLSRPacketFactory> TestOLSRPacketPiReed::factory =
	std::make_shared<ConcreteOLSRPacketFactory>();


int main() {
	CppUnit::TextUi::TestRunner runner;
	
	//Pi 1
	// runner.addTest(TestIPUDPPacketPi1::suite());
	// runner.addTest(TestOLSRHelloPi1::suite());
	// runner.addTest(TestOLSRPacketPi1::suite());

	//Pi 2
<<<<<<< HEAD
	runner.addTest(TestIPUDPPacketPi2::suite());
	runner.addTest(TestOLSRHelloPi2::suite());
	runner.addTest(TestOLSRPacketPi2::suite());

	//Pi 3
	runner.addTest(TestIPUDPPacketPi3::suite());
	runner.addTest(TestOLSRHelloPi3::suite());
=======
	// runner.addTest(TestIPUDPPacketPi2::suite());
	// runner.addTest(TestOLSRHelloPi2::suite());
	runner.addTest(TestOLSRPacketPi2::suite());

	//Pi 3
	// runner.addTest(TestIPUDPPacketPi3::suite());
	// runner.addTest(TestOLSRHelloPi3::suite());
>>>>>>> 065541cff7ddf6ff7c2c0f1e5674777452216ccd
	runner.addTest(TestOLSRPacketPi3::suite());

	// //Pi 4
	// runner.addTest(TestIPUDPPacketPi4::suite());
	// runner.addTest(TestOLSRHelloPi4::suite());
	runner.addTest(TestOLSRPacketPi4::suite());

	// //Pi 5
	runner.addTest(TestIPUDPPacketPiReed::suite());
	runner.addTest(TestOLSRHelloPiReed::suite());
	runner.addTest(TestOLSRPacketPiReed::suite());

	// //Pi Reed
	// runner.addTest(TestIPUDPPacketPiReed::suite());
	// runner.addTest(TestOLSRHelloPiReed::suite());
	runner.addTest(TestOLSRPacketPiReed::suite());


	runner.run();
	return 0;
}
