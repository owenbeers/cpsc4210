// *** CPSC 4210/5210 Assignment 3 code
//Jason, Bronson, Reed

#ifndef __TESTIPUDPPACKETPIREED_H
#define __TESTIPUDPPACKETPIREED_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <iostream>

#include "IPUDPPacketFactory.h"
#include "IPUDPPacket.h"
#include "IPAddr.h"

#include <netinet/in.h>


/// Test fixture for ProgressiveRateStrategy
class TestIPUDPPacketPiReed : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(TestIPUDPPacketPiReed);
	CPPUNIT_TEST(TestIPUDPDataTooLarge);
	CPPUNIT_TEST(TestIPUDPSource);
	CPPUNIT_TEST(TestIPUDPDestination);
	CPPUNIT_TEST(TestIPUDPTTL);
	CPPUNIT_TEST(TestIPUDPdatalen);
	CPPUNIT_TEST(TestIPUDPTOS);
	CPPUNIT_TEST(TestIPUDPProtocal);
	CPPUNIT_TEST(TestIPUDPSrcPort);
	CPPUNIT_TEST(TestIPUDPDstPort);
	CPPUNIT_TEST(TestIPUDPudplen);
	CPPUNIT_TEST(TestIPUDPVersion);
	CPPUNIT_TEST(TestIPUDPIHL);
	CPPUNIT_TEST(TestIPUDPHeaderValid);
	CPPUNIT_TEST(TestInvalidPorts);
	CPPUNIT_TEST(TestInvalidHeaderProtocal);
	CPPUNIT_TEST(TestInvalidIPVersion);

CPPUNIT_TEST_SUITE_END();
private:
	// we make the factory static so that we can initialize it in the
	// .cc file to the concrete factory that returns the concrete object
	// with our implementation of setHeader()
	static std::shared_ptr<IPUDPPacketFactory> factory;

private:
	// the packet object being tested
	std::shared_ptr<IPUDPPacket> ipudp;
	// the buffer used for testing
	unsigned char * buffer;
	// the buffer length
	const int buflen = 1000;
   IPAddr src;
   IPAddr dst;
   const uint8_t TTL=1;
   const uint16_t datalen=11;

public:
	void setUp()  {
		if (factory)
			ipudp = factory->createIPUDPPacket();
		else {
	    std::cerr << "TestIPUDPSample test fixture not properly initialized!\n";
			exit(EXIT_FAILURE);
		}
		buffer = new unsigned char [buflen];
	};
	
	void tearDown()  {
		delete[] buffer;
		// we don't need to delete ipudp since it is a smart pointer
	};


   // *************** test cases ************
   void TestIPUDPDataTooLarge() {
		int header_len = ipudp->setHeader(buffer, 28+10, src, dst, TTL, datalen);
		CPPUNIT_ASSERT(header_len == -1);
   }

   void TestIPUDPDataLen() {
		int datalen = 15;

		int header_len = ipudp->setHeader(buffer, 28+10, src, dst, TTL, datalen);

		uint16_t got;
      		std::memcpy(&got, buffer+2, sizeof(uint16_t));

		CPPUNIT_ASSERT(got == datalen+28);
   }
   
   void TestIPUDPSource() {
		ipudp->setHeader(buffer, buflen, src, dst, TTL, datalen);
      
      uint32_t got;
      std::memcpy(&got, buffer+12, sizeof(uint32_t));
      
		CPPUNIT_ASSERT(got == src.to32bWord());
   }
   
   void TestIPUDPDestination() {
		ipudp->setHeader(buffer, buflen, src, dst, TTL, datalen);
      
      		uint32_t got;
      		std::memcpy(&got, buffer+16, sizeof(uint32_t));

		CPPUNIT_ASSERT(got == dst.to32bWord());
   }

   void TestIPUDPTTL() {
		ipudp->setHeader(buffer, buflen, src, dst, TTL, datalen);
      
      uint8_t got;
      std::memcpy(&got, buffer+8, sizeof(uint8_t));

		CPPUNIT_ASSERT(got == TTL);
   }

   void TestIPUDPdatalen() {
		ipudp->setHeader(buffer, buflen, src, dst, TTL, datalen);
      
      uint8_t got;
      std::memcpy(&got, buffer+2, sizeof(uint16_t));

		CPPUNIT_ASSERT(got == (uint16_t)(28+datalen));
   }

   void TestIPUDPTOS() {
		ipudp->setHeader(buffer, buflen, src, dst, TTL, datalen);
      
      uint8_t got;
      std::memcpy(&got, buffer+1, sizeof(uint8_t));

		CPPUNIT_ASSERT(got == (uint8_t)(0));
   }

   void TestIPUDPProtocal() {
		ipudp->setHeader(buffer, buflen, src, dst, TTL, datalen);
      
      uint8_t got;
      std::memcpy(&got, buffer+9, sizeof(uint8_t));

		CPPUNIT_ASSERT(got == (uint8_t)(138));
   }

   void TestIPUDPSrcPort() {
		ipudp->setHeader(buffer, buflen, src, dst, TTL, datalen);
      
      uint16_t got;
      std::memcpy(&got, buffer+20, sizeof(uint16_t));


		CPPUNIT_ASSERT(got == (uint16_t)(698));
   }

   void TestIPUDPDstPort() {
	IPAddr dest(10.1);
		ipudp->setHeader(buffer, buflen, src, dest, TTL, datalen);
      
      uint16_t got;
      std::memcpy(&got, buffer+22, sizeof(uint16_t));

		CPPUNIT_ASSERT(got == (uint16_t)(698));
   }

   void TestIPUDPudplen() {
		ipudp->setHeader(buffer, buflen, src, dst, TTL, datalen);
      
      uint8_t got;
      std::memcpy(&got, buffer+24, sizeof(uint16_t));

		CPPUNIT_ASSERT(got == (uint16_t)(8+datalen));
   }

   void TestIPUDPVersion() {
		ipudp->setHeader(buffer, buflen, src, dst, TTL, datalen);
      
      uint8_t got;
      std::memcpy(&got, buffer, sizeof(uint8_t));
      uint8_t version = (got & 0xF0);

		CPPUNIT_ASSERT(version == 0x40);
   }

   void TestIPUDPIHL() {
		ipudp->setHeader(buffer, buflen, src, dst, TTL, datalen);
      
      uint8_t got;
      std::memcpy(&got, buffer, sizeof(uint8_t));
      uint8_t ihl = (got & 0x0F);

		CPPUNIT_ASSERT(ihl == 0x05);
   }

   void TestIPUDPHeaderValid() {
	ipudp->setHeader(buffer, buflen, src, dst, TTL, datalen);
	IPUDPPacket::HeaderInfo hi;
	CPPUNIT_ASSERT(ipudp->isValid(buffer, 120, hi));
	CPPUNIT_ASSERT(hi.src == src);
	CPPUNIT_ASSERT(hi.headerLen==28);
   	CPPUNIT_ASSERT(hi.dataLen==92);
   	CPPUNIT_ASSERT(hi.TTL==1);
   	CPPUNIT_ASSERT(hi.dest == dst);
   }

   void TestInvalidPorts() {
		ipudp->setHeader(buffer, buflen, src, dst, TTL, datalen);

      uint16_t temp16;      
      
      //Test: Source port == 698
      std::memcpy(&temp16, buffer+20, sizeof(uint16_t));
      CPPUNIT_ASSERT(temp16 == 698);

      //Test: Destination port == 698
      std::memcpy(&temp16, buffer+22, sizeof(uint16_t));
      CPPUNIT_ASSERT(temp16 == 698);
   }
   
   void TestInvalidHeaderProtocal() {
		ipudp->setHeader(buffer, buflen, src, dst, TTL, datalen);

      uint8_t temp8;      

      //Test: IP Protocal == 138
      std::memcpy(&temp8, buffer+9, sizeof(uint8_t));
      CPPUNIT_ASSERT(temp8 == 138);
   }

   void TestInvalidIPVersion() {
		ipudp->setHeader(buffer, buflen, src, dst, TTL, datalen);

      uint8_t temp8;      

      //Test: IP Version == 4
      std::memcpy(&temp8, buffer, sizeof(uint8_t));
      CPPUNIT_ASSERT( (temp8 & 0xF0) == 0x40);
   }

   void TestInvalidCheckSum() {
/*
      unsigned long sum = 0;
      const unsigned char *ip1;
      
      int hdr_len = ipudp->setHeader(buffer, buflen, src, dst, TTL, datalen);

      ip1 = buffer;
      while (hdr_len > 1)
      {
         sum += *ip1++;
         if (sum & 0x80000000)
            sum = (sum & 0xFFFF) + (sum >> 16);
         hdr_len -= 2;
      }
     
      while (sum >> 16)
         sum = (sum & 0xFFFF) + (sum >> 16);



      uint16_t temp16;
      std::memcpy(&temp16, buffer+10, sizeof(uint16_t));

      std::cout << "|" << ntohs(temp16) << "|" << temp16 << "|" << sum << "|" << ntohs(sum) << "|";
      CPPUNIT_ASSERT(ntohs(temp16)==sum);      

      bool done=false;
      CPPUNIT_ASSERT(done==true);
*/
   }
};

#endif
