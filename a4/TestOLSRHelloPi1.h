#ifndef __TESTOLSRHELLOPI1_H
#define __TESTOLSRHELLOPI1_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <iostream>

#include "OLSRPacketFactory.h"
#include "OLSRHelloFactory.h"
#include "IPAddr.h"

#include <vector>

/// Test fixture for ProgressiveRateStrategy
class TestOLSRHelloPi1 : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(TestOLSRHelloPi1);
	CPPUNIT_TEST(helloBufferTooSmall);
	CPPUNIT_TEST(testMPR);
	CPPUNIT_TEST(testNeighbour);
	CPPUNIT_TEST(testForCompletePacket);
	CPPUNIT_TEST(checkFrontBufferAllocation);
	CPPUNIT_TEST(checkMidBufferAllocation);
	//CPPUNIT_TEST(checkForLinkCodeNeighbourType);
	//CPPUNIT_TEST(checkForLinkCodeLinkType);
	CPPUNIT_TEST(checkLMS);
	
	//:: Start of Assn 4 tests
	CPPUNIT_TEST(isValid_BufferProperSize);
	CPPUNIT_TEST(isValid_HeaderValid);
	CPPUNIT_TEST(isValid_HeaderInvalid);

	CPPUNIT_TEST_SUITE_END();
private:
	// we make the factory static so that we can initialize it in the
	// .cc file to the concrete factory that returns the concrete object
	// with our implementation of setHeader()
	static std::shared_ptr<OLSRHelloFactory> factory;

private:
	// the packet object being tested
	std::shared_ptr<OLSRHello> helloPacket;

	// the buffer used for testing
	unsigned char * buffer;
	// the buffer length
	const int buflen = 1000;

	int offset = 0;
	std::vector<IPAddr> testMpr, testNbours;

	

	
	
	
public:
	

	void setUp()  {
		if (factory)
			helloPacket = factory->createOLSRHello();
		else {
	    std::cerr << "OLSR Hello packet tests not properly initialized!\n (TestOLSRHelloPi1.h - Line 39)\n";
			exit(EXIT_FAILURE);
		}
		buffer = new unsigned char [buflen];
	};

	void tearDown()  {
		delete[] buffer;
		// we don't need to delete ipudp since it is a smart pointer
	};

	// *************** test cases ************
	void helloBufferTooSmall() {
		int header_len = helloPacket->setHeader(buffer, 7, testMpr, testNbours);
		CPPUNIT_ASSERT(header_len == -1);
	}

	void testMPR(){
		IPAddr testIP;
		testIP.to32bWord();

		testMpr.push_back(testIP);
		int header_len = helloPacket->setHeader(buffer, 8, testMpr, testNbours);

		CPPUNIT_ASSERT(header_len == -1);

	}

	void testNeighbour(){
		IPAddr testIP;
		testIP.to32bWord();

		testNbours.push_back(testIP);
		int header_len = helloPacket->setHeader(buffer, 8, testMpr, testNbours);

		CPPUNIT_ASSERT(header_len == -1);

	}

	void testForCompletePacket(){
		int header_len = helloPacket->setHeader(buffer, 12, testMpr, testNbours);
		CPPUNIT_ASSERT(header_len == 96);
	}

	void checkFrontBufferAllocation(){
		helloPacket->setHeader(buffer, buflen, testMpr, testNbours);

		uint16_t testVal = 0;
		uint16_t frontReserve;


		std::memcpy(&frontReserve, buffer+offset, sizeof(uint16_t));
		CPPUNIT_ASSERT(testVal == frontReserve);
	}

	void checkMidBufferAllocation(){
		helloPacket->setHeader(buffer, buflen, testMpr, testNbours);

		uint16_t testVal = 0;
		uint16_t midReserve;


		std::memcpy(&midReserve, buffer+offset+5, sizeof(uint16_t));
		CPPUNIT_ASSERT(testVal == midReserve);
	}

	void checkForLinkCodeNeighbourType(){

        uint8_t linkCode = 0x6;//for neighbour type = "01" and Link type = "10"
        uint8_t testNeighbourType;

		std::memcpy(&linkCode, buffer+offset+4, sizeof(uint8_t));
		testNeighbourType = (linkCode & 0xc);
		CPPUNIT_ASSERT(testNeighbourType == 0x4);
	}

	void checkForLinkCodeLinkType(){

		uint8_t linkCode = 0x6;//for neighbour type = "01" and Link type = "10"
        uint8_t testLinkType;

		memcpy(&linkCode, buffer+offset+4, sizeof(uint8_t));
		testLinkType = (linkCode & 0x3);
		CPPUNIT_ASSERT(testLinkType == 0x2);
	}

	void checkLMS(){
		helloPacket->setHeader(buffer, buflen, testMpr, testNbours);

		int nboursSize = (4*(int)testNbours.size());
		int mprSize = (4*(int)testMpr.size());


		int headerlen = 8;
		int headerSize = headerlen + nboursSize + mprSize + offset;

		uint8_t testVal = headerSize;
		uint8_t lMS;


		std::memcpy(&lMS, buffer+offset+7, sizeof(uint8_t));
		CPPUNIT_ASSERT(testVal == lMS);
	}

	void isValid_BufferProperSize(){
		
		//:: Populate the Vector with 4 Addresses
		IPAddr one(169,254,255,255);
		IPAddr two(168,254,255,254);
		IPAddr three(168,254,255,253);
		IPAddr four(168,254,255,252);

		testNbours.push_back(one);
		testNbours.push_back(two);
		testNbours.push_back(three);
		testNbours.push_back(four);

		OLSRHello::HeaderInfo testHelloHeaderInfo;
		//:: Assign Data to the struct
		testHelloHeaderInfo.neighbors = testNbours;
		testHelloHeaderInfo.type = OLSRHello::NeighborType::SYM;
		testHelloHeaderInfo.LMOffsetEnd = 0;

		//:: Intentionally set the buffer to be of proper size.
		//:: For this case we are passing 4 IP Address at 4 Bytes each, so 20 Bytes of space is needed. Any less and the test will fail.
		//:::: NOTE: Setting Offset to 4 to account for frontReserve, hTime, and willingness. (4 Bytes)
		bool testVal = helloPacket->isValid(buffer, 20, 4, testHelloHeaderInfo);

		CPPUNIT_ASSERT(testVal == true);
		CPPUNIT_ASSERT(!testHelloHeaderInfo.neighbors.empty());
	}

	void isValid_HeaderValid(){
		IPAddr one(169,254,255,255);
		IPAddr two(169,254,255,254);
		
		std::vector<IPAddr> cache;		
		cache.push_back(one);
		cache.push_back(two);
		
		OLSRHello::HeaderInfo testHelloHeaderInfo;
		testHelloHeaderInfo.neighbors = cache;
		testHelloHeaderInfo.type = OLSRHello::NeighborType::SYM;
		testHelloHeaderInfo.LMOffsetEnd = ( 4*(testHelloHeaderInfo.neighbors.size()) );

		CPPUNIT_ASSERT( helloPacket->isValid(buffer, 10, 0, testHelloHeaderInfo  ) );
		CPPUNIT_ASSERT(testHelloHeaderInfo.neighbors.front() == IPAddr(169,254,255,255) );
		CPPUNIT_ASSERT(testHelloHeaderInfo.type == OLSRHello::NeighborType::SYM);
		CPPUNIT_ASSERT( (4*(int)testHelloHeaderInfo.neighbors.size() ) <= testHelloHeaderInfo.LMOffsetEnd);		
	}

	void isValid_HeaderInvalid(){
		IPAddr one(169,254,255,255);
				
		std::vector<IPAddr> cache;		
		cache.push_back(one);
				
		OLSRHello::HeaderInfo testHelloHeaderInfo;
		testHelloHeaderInfo.neighbors = cache;
		
		//:: Intentionally set NeighborType to be incorrect
		testHelloHeaderInfo.type = OLSRHello::NeighborType::MPR;
		
		//:: Intentionally set an incorrect LMOffsetEnd
		testHelloHeaderInfo.LMOffsetEnd = ( 8*(testHelloHeaderInfo.neighbors.size()) );

		//:: Intentionally set msglen to be less than size required
		CPPUNIT_ASSERT( !helloPacket->isValid(buffer, 3, 0, testHelloHeaderInfo  ) );
		CPPUNIT_ASSERT(testHelloHeaderInfo.type != OLSRHello::NeighborType::SYM);
		CPPUNIT_ASSERT( (4*(int)testHelloHeaderInfo.neighbors.size() ) <= testHelloHeaderInfo.LMOffsetEnd);	
	}


};

#endif
