// *** CPSC 4210/5210 Assignment 3 code
// ***** CPPUnit fixture class sample for creating headers for IP/UDP packets ***

#ifndef __TESTIPUDPSAMPLE_H
#define __TESTIPUDPSAMPLE_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <iostream>

#include "IPUDPPacketFactory.h"
#include "IPAddr.h"


/// Test fixture for ProgressiveRateStrategy
class TestIPUDPPacketPi2 : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(TestIPUDPPacketPi2);
	CPPUNIT_TEST(TestIPUDPDataTooLarge);
	CPPUNIT_TEST(TestIPUDPVersion);
	CPPUNIT_TEST(TestIPUDPIHL);
	CPPUNIT_TEST(TestIPUDPTotalLength);
	CPPUNIT_TEST(TestIPUDPIdentification);
	CPPUNIT_TEST(TestIPUDPFlags);
	CPPUNIT_TEST(TestIPUDPFragmentOffset);
	CPPUNIT_TEST(TestIPUDPTTL);
	CPPUNIT_TEST(TestIPUDPProtocol);
	CPPUNIT_TEST(TestIPUDPChecksum);
	CPPUNIT_TEST(TestIPUDPTypeofService);
	CPPUNIT_TEST(TestIPUDPSrcto32bWord);
	CPPUNIT_TEST(TestIPUDPSrctoString);
	CPPUNIT_TEST(TestIPUDPDstto32bWord);
	CPPUNIT_TEST(TestIPUDPDsttoString);
	CPPUNIT_TEST(TestIPUDPdatalen);
	CPPUNIT_TEST(TestUDPSrcPort);
	CPPUNIT_TEST(TestUDPDstPort);
	CPPUNIT_TEST(TestUDPLength);
	CPPUNIT_TEST(TestUDPChecksum);

	//Assignment 4 Test Cases
	CPPUNIT_TEST(TestIPUDPHeaderValid);
	CPPUNIT_TEST(TestIPUDPInvalidbuflen);
	CPPUNIT_TEST(TestIPUDPInvaliedChecksum);



	CPPUNIT_TEST_SUITE_END();
private:
	// we make the factory static so that we can initialize it in the
	// .cc file to the concrete factory that returns the concrete object
	// with our implementation of setHeader()
	static std::shared_ptr<IPUDPPacketFactory> factory;

private:
	// the packet object being tested
	std::shared_ptr<IPUDPPacket> ipudp;
	// the buffer used for testing
	unsigned char * buffer;
	// the buffer length
	const int buflen = 1000;
	

public:
	void setUp()  {
		if (factory)
			ipudp = factory->createIPUDPPacket();
		else {
	    std::cerr << "TestIPUDP test fixture not properly initialized!\n";
			exit(EXIT_FAILURE);
		}
		buffer = new unsigned char [buflen];
	};
	
	void tearDown()  {
		delete[] buffer;
		// we don't need to delete ipudp since it is a smart pointer
	};

	// *************** test cases **************

	void TestIPUDPDataTooLarge() {
		IPAddr src,dst;
		int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 11);
		CPPUNIT_ASSERT(header_len == -1);
	}

	void TestIPUDPVersion() {
		IPAddr src, dst;
		unsigned char actualVersion;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, 1, 11);
		std::memcpy(&actualVersion, buffer, sizeof(unsigned char));

		actual << (uint16_t)(actualVersion >> 4);
		expected << 4;

		CPPUNIT_ASSERT(actual.str() == expected.str());	
	}

	void TestIPUDPIHL() {
		IPAddr src, dst;
		unsigned char actualIHL;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, 1, 11);
		std::memcpy(&actualIHL, buffer, sizeof(unsigned char));

		actual << (uint16_t)(actualIHL & 0x0F);
		expected << 5;

		CPPUNIT_ASSERT(actual.str() == expected.str());	
	}

	void TestIPUDPTypeofService() {
		IPAddr src, dst;
		unsigned char actualType;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, 1, 11);
		std::memcpy(&actualType, buffer+1, sizeof(unsigned char));

		actual << (uint16_t)(actualType);
		expected << 1;

		CPPUNIT_ASSERT(actual.str() == expected.str());	
	}

	void TestIPUDPTotalLength(){
		IPAddr src, dst;
		int datalen = 11, offset = 28;
		uint16_t srcDatalen;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, 1, datalen);
		std::memcpy(&srcDatalen, buffer+2, sizeof(uint16_t));

		actual << offset+datalen;
		expected << srcDatalen;

		CPPUNIT_ASSERT(actual.str() == expected.str());
	}

	void TestIPUDPIdentification(){
		IPAddr src, dst;
		int identification = 1; // Identification
		uint16_t srcOut;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, 1, 11);
		std::memcpy(&srcOut, buffer+4, sizeof(uint16_t));

		actual << srcOut;
		expected << identification;

		CPPUNIT_ASSERT(actual.str() == expected.str());
	}

	void TestIPUDPFlags(){
		IPAddr src, dst;
		uint16_t flags = 0;
		unsigned char actualFlags;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, 1, 11);
		std::memcpy(&actualFlags, buffer+6, sizeof(uint16_t));

		actual << (actualFlags >> 15);
		expected << flags;

		CPPUNIT_ASSERT(actual.str() == expected.str());	
	}

	void TestIPUDPFragmentOffset(){
		IPAddr src, dst;
		uint16_t offset = 0;
		unsigned char actualOffset;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, 1, 11);
		std::memcpy(&actualOffset, buffer+6, sizeof(uint16_t));

		actualOffset = (actualOffset << 3);
		actual << (actualOffset >> 3);
		expected << offset;

		CPPUNIT_ASSERT(actual.str() == expected.str());

	}

	void TestIPUDPTTL() {
		IPAddr src, dst;
		int TTL = 2;
		uint8_t srcTTL;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, TTL, 11);
		std::memcpy(&srcTTL, buffer+8, sizeof(uint8_t));

		actual << TTL;
		expected << (uint16_t)srcTTL;

		CPPUNIT_ASSERT(actual.str() == expected.str());
	}

	void TestIPUDPProtocol(){
		IPAddr src, dst;
		uint8_t protocol = 138;
		uint8_t srcProtocol;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, 1, 11);
		std::memcpy(&srcProtocol, buffer+9, sizeof(uint8_t));

		expected << (uint16_t)protocol;
		actual << (uint16_t)srcProtocol;

		CPPUNIT_ASSERT(actual.str() == expected.str());
	}

	void TestIPUDPChecksum() {
		IPAddr src, dst;
		//0100		|0101	|0000 0001	|0000 0000 0010 0111	|0000 0001		|000	|0 0000 0000 0000
		//version	| IHL 	|ToS 		|Total Length 			|Identification |Flags 	|Fragment Offset
		//This case, checksum is 50.
		int TTL = 1, datalen = 11, expectedChecksum = 50;
		uint8_t checksumResult;
		unsigned char parseCS[8];
		unsigned long actualChecksum = 0;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, TTL, datalen);
		
		std::memcpy(&actualChecksum, buffer, sizeof(uint64_t));
		parseCS[7] = (unsigned char)(actualChecksum >> 56) & 0xFF;
		parseCS[6] = (unsigned char)(actualChecksum >> 48) & 0xFF;
		parseCS[5] = (unsigned char)(actualChecksum >> 40) & 0xFF;
		parseCS[4] = (unsigned char)(actualChecksum >> 32) & 0xFF;
		parseCS[3] = (unsigned char)(actualChecksum >> 24) & 0xFF;
		parseCS[2] = (unsigned char)(actualChecksum >> 16) & 0xFF;
		parseCS[1] = (unsigned char)(actualChecksum >> 8) & 0xFF;
		//Seperate and add IHL and VERSION
		parseCS[0] = (unsigned char)actualChecksum & 0x0F;
		parseCS[0]+= (unsigned char)(actualChecksum >> 4) & 0x0F;

		checksumResult = parseCS[0] + parseCS[1] + parseCS[2] + parseCS[3] 
					   + parseCS[4] + parseCS[5] + parseCS[6] + parseCS[7];
		checksumResult = checksumResult ^ 0xFF;
		checksumResult += 1;

		checksumResult += expectedChecksum;
		checksumResult = checksumResult ^ 0xFF;
		checksumResult += 1;

		//Compare and Check: Checksum
		expected << 0;
		actual <<  (int)checksumResult;

		CPPUNIT_ASSERT(expected.str() == actual.str());
	}

	void TestIPUDPSrcto32bWord() {
		//Set up
		IPAddr src, dst;
		uint32_t srcAddr;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, 1, 11);
		std::memcpy(&srcAddr, buffer+12, sizeof(uint32_t));

		//Compare and Check: to32bWord()
		expected << src.to32bWord();
		actual << srcAddr;
		CPPUNIT_ASSERT(expected.str() == actual.str());

	}

	void TestIPUDPSrctoString() {
		//Set up
		unsigned char parseIP[4];
		IPAddr src, dst;
		uint32_t srcAddr;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, 1, 11);
		std::memcpy(&srcAddr, buffer+12, sizeof(uint32_t));

		parseIP[3] = (srcAddr >> 24) & 0xFF;
		parseIP[2] = (srcAddr >> 16) & 0xFF;
		parseIP[1] = (srcAddr >> 8) & 0xFF;
		parseIP[0] = srcAddr & 0xFF;
		
		//Compare and Check: Convert to ###.###.###.###
		expected << src.toString();
		actual << (int)parseIP[0] << "."
				<< (int)parseIP[1] << "."
				<< (int)parseIP[2] << "."
				<< (int)parseIP[3];
		CPPUNIT_ASSERT(expected.str() == actual.str());

	}

	void TestIPUDPDstto32bWord() {
		//Set up
		IPAddr src, dst;
		uint32_t dstAddr;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, 1, 11);
		std::memcpy(&dstAddr, buffer+16, sizeof(uint32_t));

		//Compare and Check: to32bWord()
		expected << src.to32bWord();
		actual << dstAddr;
		CPPUNIT_ASSERT(expected.str() == actual.str());
	}

	void TestIPUDPDsttoString() {
		//Set up
		unsigned char parseIP[4];
		IPAddr src, dst;
		uint32_t dstAddr;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, 1, 11);
		std::memcpy(&dstAddr, buffer+16, sizeof(uint32_t));

		parseIP[3] = (dstAddr >> 24) & 0xFF;
		parseIP[2] = (dstAddr >> 16) & 0xFF;
		parseIP[1] = (dstAddr >> 8) & 0xFF;
		parseIP[0] = dstAddr & 0xFF;
		
		//Compare and Check: Convert to ###.###.###.###
		expected << src.toString();
		actual << (int)parseIP[0] << "."
				<< (int)parseIP[1] << "."
				<< (int)parseIP[2] << "."
				<< (int)parseIP[3];
		CPPUNIT_ASSERT(expected.str() == actual.str());
	}

	void TestIPUDPdatalen() {
		IPAddr src, dst;
		int datalen = 11;
		uint16_t srcDatalen;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, 1, datalen);
		std::memcpy(&srcDatalen, buffer+2, sizeof(uint16_t));

		actual << 28+datalen;
		expected << srcDatalen;

		CPPUNIT_ASSERT(actual.str() == expected.str());		
	}

	void TestUDPSrcPort() {
		//Set up
		IPAddr src, dst;
		uint16_t port = 698;
		uint16_t srcPort;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, 1, 11);
		std::memcpy(&srcPort, buffer+20, sizeof(uint16_t));

		//Compare and Check
		expected << 698;
		actual << srcPort;
		CPPUNIT_ASSERT(expected.str() == actual.str());
	}

	void TestUDPDstPort() {
		//Set up
		IPAddr src, dst;
		uint16_t port = 698;
		uint16_t dstPort;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, 1, 11);
		std::memcpy(&dstPort, buffer+22, sizeof(uint16_t));

		//Compare and Check
		expected << 698;
		actual << dstPort;
		CPPUNIT_ASSERT(expected.str() == actual.str());
	}

	void TestUDPLength() {
		//Set up
		IPAddr src, dst;
		uint16_t length = 1000;
		uint16_t srcLength;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, 1, 11);
		std::memcpy(&srcLength, buffer+24, sizeof(uint16_t));

		//Compare and Check
		expected << length;
		actual << srcLength;
		CPPUNIT_ASSERT(expected.str() == actual.str());		
	}

	void TestUDPChecksum() {
		//Set up
		IPAddr src, dst;
		uint16_t checksum = 0;
		uint16_t srcChecksum;
		std::ostringstream expected, actual;

		int test = ipudp->setHeader(buffer, buflen, src, dst, 1, 11);
		std::memcpy(&srcChecksum, buffer+26, sizeof(uint16_t));

		//Compare and Check
		expected << checksum;
		actual << srcChecksum;
		CPPUNIT_ASSERT(expected.str() == actual.str());
	}
  ////////////////////////////////////////
 /////// Assignment 4 Test Cases ////////
////////////////////////////////////////
public:
	/* Run below code with invalid values for tests.
	unsigned char b[120] = {0x45,0,0,120,0,0,0,0,1,138,0,0,169,254,1,1,169,254,255,255,0x2,0xba,0x2,0xba,0,100,0,0};
	IPUDPPacket::HeaderInfo hi;
	CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(ipudp->isValid(b,120,hi)));
	*/
	void TestIPUDPHeaderValid() {
		unsigned char b[120] = {0x45 /*version and header length*/,
									0 /*type of service*/, 0, 120 /*total length, 2 bytes*/,
									0,0,0,0 /* identification =, 4 bytes*/,
									1 /*TTL*/, 138 /*protocol*/, 0, 0 /*checksum, 2 bytes*/,
									169, 254, 1, 1 /*source addr, 4 bytes*/,
									169,254,255,255 /*dest addr, 4 bytes*/,
									0x2, 0xba /*src port, 2 bytes*/, 0x2, 0xba /*dest port, 2 bytres*/,
									0,100 /*length of UDP, 2bytes*/, 0, 0 /*checksum, 2 bytes*/};
		
		IPUDPPacket::HeaderInfo hi;
		hi.headerLen = 28;
		hi.dataLen = 92;
		hi.TTL = 1;
		hi.src = IPAddr(169, 254, 1,1);
		hi.dest = IPAddr(169,254,255,255);
		CPPUNIT_ASSERT(ipudp->isValid(b, 120, hi));
		CPPUNIT_ASSERT(hi.headerLen == 28);
		CPPUNIT_ASSERT(hi.dataLen == 92);
		CPPUNIT_ASSERT(hi.TTL == 1);
		CPPUNIT_ASSERT(hi.src == IPAddr(169,254,1,1));
		CPPUNIT_ASSERT(hi.dest == IPAddr(169,254,255,255));
	}

	void TestIPUDPInvalidbuflen() {
		unsigned char b[120] = {0x45 /*version and header length*/,
									0 /*type of service*/, 0, 120 /*total length, 2 bytes*/,
									0,0,0,0 /* identification =, 4 bytes*/,
									1 /*TTL*/, 138 /*protocol*/, 0, 0 /*checksum, 2 bytes*/,
									169, 254, 1, 1 /*source addr, 4 bytes*/,
									169,254,255,255 /*dest addr, 4 bytes*/,
									0x2, 0xba /*src port, 2 bytes*/, 0x2, 0xba /*dest port, 2 bytres*/,
									0,100 /*length of UDP, 2bytes*/, 0, 0 /*checksum, 2 bytes*/};
		
		IPUDPPacket::HeaderInfo hi;
		hi.headerLen = 28;
		hi.dataLen = 92;
		hi.TTL = 1;
		hi.src = IPAddr(169, 254, 1,1);
		hi.dest = IPAddr(169,254,255,255);
		CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(ipudp->isValid(b, 12000, hi)));
	}

	void TestIPUDPInvaliedChecksum() {
		unsigned char b[120] = {0x45 /*version and header length*/,
									0 /*type of service*/, 0, 120 /*total length, 2 bytes*/,
									0,0,0,0 /* identification =, 4 bytes*/,
									1 /*TTL*/, 138 /*protocol*/, 0, 0 /*checksum, 2 bytes*/,
									169, 254, 1, 1 /*source addr, 4 bytes*/,
									169,254,255,255 /*dest addr, 4 bytes*/,
									0x2, 0xba /*src port, 2 bytes*/, 0x2, 0xba /*dest port, 2 bytres*/,
									0,100 /*length of UDP, 2bytes*/, 0, 0 /*checksum, 2 bytes*/};
		
		IPUDPPacket::HeaderInfo hi;
		hi.headerLen = 29;
		hi.dataLen = 92;
		hi.TTL = 1;
		hi.src = IPAddr(169, 254, 1,1);
		hi.dest = IPAddr(169,254,255,255);
		CPPUNIT_ASSERT_ASSERTION_FAIL(CPPUNIT_ASSERT(ipudp->isValid(b, 120, hi)));
	}

};

#endif
