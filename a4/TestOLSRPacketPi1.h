#ifndef __TESTOLSRPACKETPI1_H
#define __TESTOLSRPACKETPI1_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <bitset>

#include "OLSRPacketFactory.h"
#include "IPAddr.h"


using namespace std;


/// Test fixture for ProgressiveRateStrategy
class TestOLSRPacketPi1 : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(TestOLSRPacketPi1);
	CPPUNIT_TEST(sampleTestOLSRPacketDataTooLarge);

	CPPUNIT_TEST(checkPacketLength);
	CPPUNIT_TEST(checkPacketSequenceNumber);
	CPPUNIT_TEST(checkMessageType);
	CPPUNIT_TEST(checkVTime);
	CPPUNIT_TEST(checkMessageSize);
	CPPUNIT_TEST(checkOriginatorAddress);
	CPPUNIT_TEST(checkTimeToLive);
	CPPUNIT_TEST(checkHopCount);
	CPPUNIT_TEST(checkMessageSequence);

	//Assignment-4

	CPPUNIT_TEST(testOLSRPacketLength);
	CPPUNIT_TEST(testOLSRPacketSequenceNumber);
	CPPUNIT_TEST(testMessageSize);
	CPPUNIT_TEST(testOriginatorAddress);
	CPPUNIT_TEST(testMessageSequenceNumber);
	CPPUNIT_TEST(testTimeToLive);
	//CPPUNIT_TEST(checkMessageType);
	//CPPUNIT_TEST(checkTTL);

	CPPUNIT_TEST_SUITE_END();
private:
	// we make the factory static so that we can initialize it in the
	// .cc file to the concrete factory that returns the concrete object
	// with our implementation of setHeader()
	static std::shared_ptr<OLSRPacketFactory> factory;

private:
	// the packet object being tested
	std::shared_ptr<OLSRPacket> olsrP;
	// the buffer used for testing
	unsigned char * buffer;
	// the buffer length
	const int buflen = 1000;


public:

    uint16_t packLen = 24;
    uint16_t packSequenceNum = 1;
    uint8_t  messageType = 1;
    uint8_t  vTime = 6;
    uint16_t msgSize = 20;
    uint32_t srcAddr;
    uint8_t  ttl = 1;
    uint8_t  hopCount = 0;
    uint16_t msgSequenceNum = 1;





	void setUp()  {
		if (factory){
			//psn++;
			//msn++;
			olsrP = factory->createOLSRPacket();

		}
		else {
	    std::cerr << "TestIPUDPSample test fixture not properly initialized!\n";
			exit(EXIT_FAILURE);
		}
		buffer = new unsigned char [buflen];
	};

	void tearDown()  {
		delete[] buffer;
		// we don't need to delete ipudp since it is a smart pointer
	};

	// *************** test cases for Assignment-3************

	void sampleTestOLSRPacketDataTooLarge() {

		IPAddr origin;
		int header_length = olsrP->setHeader(buffer,packLen,messageType,origin,ttl,hopCount,packSequenceNum,msgSequenceNum,msgSize);
		CPPUNIT_ASSERT(header_length == -1);
	}

    //To Check Packet Length
    void checkPacketLength() {

		IPAddr origin;
		olsrP->setHeader(buffer,packLen,messageType,origin,ttl,hopCount,packSequenceNum,msgSequenceNum,msgSize);
		memcpy(&packLen,buffer,sizeof(packLen));
		CPPUNIT_ASSERT(packLen == 24);
	}


    // To check Packet Sequence Number
	void checkPacketSequenceNumber() {

		IPAddr origin;
		olsrP->setHeader(buffer,packLen,messageType,origin,ttl,hopCount,packSequenceNum,msgSequenceNum,msgSize);
		memcpy(&packSequenceNum,buffer+2,sizeof(packSequenceNum));
		CPPUNIT_ASSERT(packSequenceNum == 1);
	}

    //To Check Message Type
    void checkMessageType() {

		IPAddr origin;
		olsrP->setHeader(buffer,packLen,messageType,origin,ttl,hopCount,packSequenceNum,msgSequenceNum,msgSize);
		memcpy(&messageType,buffer+4,sizeof(messageType));
		CPPUNIT_ASSERT(messageType == 1);
	}

	//To Check VTime
	void checkVTime() {

		IPAddr origin;
		olsrP->setHeader(buffer,packLen,messageType,origin,ttl,hopCount,packSequenceNum,msgSequenceNum,msgSize);
		memcpy(&vTime,buffer+5,sizeof(vTime));
		CPPUNIT_ASSERT(vTime == 6);
	}

	//To Chcek Message Size
	void checkMessageSize() {

		IPAddr origin;
		olsrP->setHeader(buffer,packLen,messageType,origin,ttl,hopCount,packSequenceNum,msgSequenceNum,msgSize);
		memcpy(&msgSize,buffer+6,sizeof(msgSize));
		CPPUNIT_ASSERT(msgSize == 20);
	}


	//To Chcek Originator Address
	void checkOriginatorAddress() {

		IPAddr origin(1,1);
		olsrP->setHeader(buffer,packLen,messageType,origin,ttl,hopCount,packSequenceNum,msgSequenceNum,msgSize);
		memcpy(&srcAddr,buffer+8,sizeof(srcAddr));
		//std::cout<<srcAddr<<std::endl;
		//std::cout<<origin.to32bWord();
		CPPUNIT_ASSERT(srcAddr == origin.to32bWord());
	}

    //To Check Time to Live
     void checkTimeToLive() {

		IPAddr origin(1,1);
		olsrP->setHeader(buffer,packLen,messageType,origin,ttl,hopCount,packSequenceNum,msgSequenceNum,msgSize);
		memcpy(&ttl,buffer+12,sizeof(ttl));
		CPPUNIT_ASSERT(ttl == 1);
	}

	//To Check Hop Count
    void checkHopCount() {

		IPAddr origin(1,1);
		olsrP->setHeader(buffer,packLen,messageType,origin,ttl,hopCount,packSequenceNum,msgSequenceNum,msgSize);
		memcpy(&hopCount,buffer+13,sizeof(hopCount));
		CPPUNIT_ASSERT(hopCount == 0);
	}

	//To Check Message Sequence Number
	void checkMessageSequence() {

		IPAddr origin(1,1);
		olsrP->setHeader(buffer,packLen,messageType,origin,ttl,hopCount,packSequenceNum,msgSequenceNum,msgSize);
		memcpy(&msgSequenceNum,buffer+14,sizeof(msgSequenceNum));
		CPPUNIT_ASSERT(msgSequenceNum == 1);
	}



	// *************** test cases for Assignment-4************

     void testOLSRPacketLength(){
        OLSRPacket::HeaderInfo hi;
        CPPUNIT_ASSERT(olsrP->isValid(buffer,packLen,hi));
        CPPUNIT_ASSERT(hi.packetLength == 24);

     }

     void testOLSRPacketSequenceNumber(){
        OLSRPacket::HeaderInfo hi;
        CPPUNIT_ASSERT(olsrP->isValid(buffer,packLen,hi));
        CPPUNIT_ASSERT(hi.packetSequenceNumber == 1);

     }

     void testMessageSize(){

        OLSRPacket::HeaderInfo hi;
        CPPUNIT_ASSERT(olsrP->isValid(buffer,packLen,hi));
        CPPUNIT_ASSERT(hi.messageSize == 20);

     }

     void testOriginatorAddress(){
        IPAddr origin(1,1);
        OLSRPacket::HeaderInfo hi;
        CPPUNIT_ASSERT(olsrP->isValid(buffer,packLen,hi));
        CPPUNIT_ASSERT(hi.originAddress == origin.to32bWord());

     }

     void testTimeToLive(){
        OLSRPacket::HeaderInfo hi;
        CPPUNIT_ASSERT(olsrP->isValid(buffer,packLen,hi));
        CPPUNIT_ASSERT(hi.timeToLive == 1);

     }

     void testMessageSequenceNumber(){
        OLSRPacket::HeaderInfo hi;
        CPPUNIT_ASSERT(olsrP->isValid(buffer,packLen,hi));
        CPPUNIT_ASSERT(hi.messageSequenceNumber == 1);

     }





};

#endif
