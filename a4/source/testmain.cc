// *** CPSC 4210/5210 Assignment 3 code
// *** main file for the packet construction test cases

#include "IPAddr.h"
#include <cppunit/ui/text/TestRunner.h>
#include "ConcreteIPUDPPacketFactory.h"

// Include the test fixture header files here
#include "TestIPUDPSample.h"


// Initialize the static factory member of the test fixtures here
std::shared_ptr<IPUDPPacketFactory> TestIPUDPSample::factory =
	std::make_shared<ConcreteIPUDPPacketFactory>();

int main() {
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(TestIPUDPSample::suite());
	// add other test suites here like on the line above
	runner.run();
	return 0;
}
