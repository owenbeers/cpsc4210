//ConcreteOLSRPacket.h

// *** CPSC 4210/5210 Assignment 3 code
// ***** OLSRPacket interface: creates the combined header of IP/UDP packed

#ifndef __CONCRETEOLSRPACKET_H
#define __CONCRETEOLSRPACKET_H

#include <cstring>
#include "OLSRPacket.h"
#include <netinet/in.h>
#include <iostream>
#include <iomanip>

#include <arpa/inet.h>

using namespace std;
class ConcreteOLSRPacket : public OLSRPacket {
public:

	int setHeader(unsigned char * buf, int buflen, int msg_type, IPAddr origin,
				  int TTL, int hop_count, uint16_t psn, uint16_t msn, int msglen) {

		int offset = 0; // length of the IPUDP Header ** Correction, we dont need to worry about this
						// Just setting it to 0 for now
		int headerLen = 16; // supposed length of the OLSR packet header
		
		// first check if the buffer is big enough
		if (buflen < msglen+headerLen+offset)
			return -1;
		
		// For all of the integers that are being put into one byte fields
		// Assert that their value is not negative and less than 256

		// getting a valid msg_type
		if (msg_type > 255 || msg_type < 0)
			return -1;

		//getting a valid time to live
		if (TTL > 255 || TTL < 0)
			return -1;

		//getting a valid value for hop count
		if (hop_count > 255 || hop_count < 0)
			return -1;

		std::memset(buf+offset, 0, buflen-offset);  // buffer should be cleared already? not necessary?
		// set the packet length field. Should be equal to 16+msglen
		*(reinterpret_cast<uint16_t*>(buf + offset)) = ntohs(headerLen+msglen);
		// set the packet sequence number to the passed in parameter psn (2 bytes) 
		*(reinterpret_cast<uint16_t*>(buf+offset+2)) = ntohs(psn);
		// set packet msg_type to passed in parameter msg_type (integer is 4 bytes, just take last byte?)
		// std::printf("%d\n", msg_type);
		// int netOrdInt = msg_type; //htonl(msg_type);

		//Grab last byte of integer?? Asserted that anything above 255 is garbage
		*(buf+offset+4) = (msg_type & 0xFF); // = 0x01
		*(buf+offset+5) = 0x06; // VTIME
		*(reinterpret_cast<uint16_t*>(buf+offset+6)) = ntohs(msglen);
		*(reinterpret_cast<uint32_t*>(buf+offset+8)) = origin.to32bWord();
		*(buf+offset+12) = TTL & 0xFF; //grab last byte of integer
		*(buf+offset+13) = hop_count & 0xFF; //grab last byte of integer
		*(reinterpret_cast<uint16_t*>(buf+offset+14)) = ntohs(msn);
																										
		return 16;  // the size of the header in bytes
	}
	bool isValid(unsigned char * buf, int buflen, HeaderInfo & hi)
	{
	// decodes the OLSR packet header and returns relevant info in the
	// HeaderInfo structure
	// PARAMETERS
	// buf (in) is the buffer, pointing to the start of the OLSR packet
	// buflen (in) is the allocated length of the buffer starting from the offset pointed by buf
	// hi (out) is an output parameter containing OLSR packet relevant
	// fields extracted from the header
	// RETURNS
	// true if buf contains a valid OLSR packet header. The header is valid if:
	// a) the message type is Hello AND
	// b) the packet contains a single message (PL == MS+4)
	// If any one of the conditions (a)-(b) fail, the function returns
    //   false, in which case hi's value is unspecified.
		
		if (buflen < 16)
		{
			return false;
		}

		//Check Message type
		// 1 byte
		if (buf[4] != 0x01)
		{
			return false;
		}

		//Grab packet length and message size
		uint16_t packLen = 0, msgSize = 0;
		uint32_t origin;

		std::memcpy(&packLen, buf, sizeof(uint16_t));
		std::memcpy(&msgSize, buf+6, sizeof(uint16_t));
		std::memcpy(&origin, buf+8, sizeof(uint32_t));

		packLen = ntohs(packLen);
		msgSize = ntohs(msgSize);
		IPAddr org = IPAddr(origin);

		if (packLen != msgSize+16)
		{
			return false;
		}

		hi.dataLen = msgSize+4;
		hi.headerLen = 16;
		hi.org = org;

		return true;
	}
};

#endif
