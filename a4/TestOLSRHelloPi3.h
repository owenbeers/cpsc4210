
#ifndef __TESTOLSRHELLOPI3_H
#define __TESTOLSRHELLOPI3_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <bitset>
#include "OLSRHelloFactory.h"
#include "IPAddr.h"

using namespace std;


/// Test fixture for ProgressiveRateStrategy
class TestOLSRHelloPi3 : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(TestOLSRHelloPi3);
	CPPUNIT_TEST(sampleTestOLSRHelloTooLarge);

    CPPUNIT_TEST(testOLSRHelloFrontReserved);
	CPPUNIT_TEST(testOLSRHelloHtime);
	CPPUNIT_TEST(testOLSRHelloWillingness);
	//CPPUNIT_TEST(testOLSRHelloLinkCodeUnspecLink);
    //CPPUNIT_TEST(testOLSRHelloSymLink);
    CPPUNIT_TEST(testOLSRHelloNeighbourType);// NEIGHBOUR TYPE

    CPPUNIT_TEST(testOLSRHelloLinkcode);  // LINK CODE


	CPPUNIT_TEST(testOLSRHelloMidReserved);
    CPPUNIT_TEST(testOLSRHelloLinkMessageSizeWithoutNeighbor);
   // CPPUNIT_TEST(testOLSRHelloLinkMessageWithNeighbor);
    CPPUNIT_TEST(testOLSRHelloInterfaceAddr);


    // Assignment 4 starts from here
    CPPUNIT_TEST(testOLSRHelloDecodingMsgLenTooLarge);
    CPPUNIT_TEST(testOLSRHelloDecodingInvalidOffset);
    CPPUNIT_TEST(testOLSRHelloDecodingLinkCodeInvalid);
    CPPUNIT_TEST(testOLSRHelloDecodingNotNeighbor);
    CPPUNIT_TEST(testOLSRHelloDecodingSymNeighbor);
    CPPUNIT_TEST(testOLSRHelloDecodingLMOffsetEnd);

	CPPUNIT_TEST_SUITE_END();
private:
	// we make the factory static so that we can initialize it in the
	// .cc file to the concrete factory that returns the concrete object
	// with our implementation of setHeader()
	static std::shared_ptr<OLSRHelloFactory> factory;

private:
	// the packet object being tested
	std::shared_ptr<OLSRHello> olsrh;
	// the buffer used for testing
	unsigned char * buffer;
	// the buffer length
	const int buflen = 1000;
    std::vector<IPAddr> mpr;
    std::vector<IPAddr> neighbors;
    int helloMessageSize = 45;



public:

 uint8_t ver_Linkcode=0x6;
	void setUp()  {
		if (factory){
			olsrh = factory->createOLSRHello();

		}
		else {
	    std::cerr << "TestIPUDPSample test fixture not properly initialized!\n";
			exit(EXIT_FAILURE);
		}
		buffer = new unsigned char [buflen];
	};

	void tearDown()  {
		delete[] buffer;
		// we don't need to delete ipudp since it is a smart pointer
		mpr.clear();
		neighbors.clear();
	};

	// *************** test cases ************
	void sampleTestOLSRHelloTooLarge() {
        int helloMessageSize = 52;
		int header_len = olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
		CPPUNIT_ASSERT(header_len == -1);
	}

    void testOLSRHelloFrontReserved() {

        uint16_t sndReserved1 = 0 ; // According to RFC 3626 document: assuming message Size will be 25
		olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        uint16_t rcvdReserved1 = 0;
        memcpy(&rcvdReserved1, buffer, sizeof(uint16_t));
        CPPUNIT_ASSERT(rcvdReserved1 == sndReserved1);
     }



    void testOLSRHelloHtime(){

        uint8_t sndHTime = 0 ; // According to RFC 3626 document: assuming message Size will be 25
		olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        uint8_t rcvdhTime = 0;
        memcpy(&rcvdhTime, buffer+2, sizeof(uint8_t));
        CPPUNIT_ASSERT(rcvdhTime == sndHTime);
     }

    void testOLSRHelloWillingness() {

        uint8_t sndWillingness = 3 ; // According to RFC 3626 document: assuming message Size will be 25
		olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        uint8_t rcvdWillingness = 0;
        memcpy(&rcvdWillingness, buffer+3, sizeof(uint8_t));
        CPPUNIT_ASSERT(rcvdWillingness == sndWillingness);
	}

   // void testOLSRHelloLinkCodeUnspecLink() {
void testOLSRHelloNeighbourType() { // According to RFC 3626 document: assuming message Size will be 25
		olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        //uint8_t rcvdLinkCodeUnspec = 0;
        memcpy(&ver_Linkcode, buffer+4, sizeof(uint8_t));
         ver_Linkcode = (ver_Linkcode & 0xC);
        CPPUNIT_ASSERT(ver_Linkcode == 0x4);
     }


    //void testOLSRHelloSymLink(){
    void testOLSRHelloLinkcode(){
      // unsigned char piNum = '3', nodeNum = '1';
       IPAddr origin;
       origin.to32bWord();
       neighbors.push_back(origin);
      // uint8_t sndLinkSymLink = 0 ;
       olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
       //uint8_t rcvdLinkCodeUnspec = 6;
       memcpy(&ver_Linkcode, buffer+4, sizeof(uint8_t));
           ver_Linkcode = (ver_Linkcode & 0x3);
       CPPUNIT_ASSERT(ver_Linkcode == 0x2);
     }

   void testOLSRHelloMidReserved() {

        uint8_t sndReserved2 = 0 ; // According to RFC 3626 document: assuming message Size will be 25
		olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        uint8_t rcvdReserved2 = 0;
        memcpy(&rcvdReserved2, buffer+5, sizeof(uint16_t));
        CPPUNIT_ASSERT(rcvdReserved2 == sndReserved2);
	}

    void testOLSRHelloLinkMessageSizeWithoutNeighbor() {

        uint16_t sndMsgSizeWithoutNeighbor = 4 ; // According to RFC 3626 document: assuming message Size will be 25
		olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        uint16_t rcvdMsgSizeWithoutNeighbor = 0;
        memcpy(&rcvdMsgSizeWithoutNeighbor, buffer+6, sizeof(uint16_t));
        CPPUNIT_ASSERT(rcvdMsgSizeWithoutNeighbor == sndMsgSizeWithoutNeighbor);
     }

    void testOLSRHelloLinkMessageWithNeighbor(){
        IPAddr origin;
        origin.to32bWord();
        neighbors.push_back(origin);
        uint16_t sndMsgSizeWithNeighbor = 8 ; // According to RFC 3626 document: assuming message Size will be 25
		olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        uint16_t rcvdMsgSizeWithNeighbor = 0;
        memcpy(&rcvdMsgSizeWithNeighbor, buffer+6, sizeof(uint16_t));
        CPPUNIT_ASSERT(rcvdMsgSizeWithNeighbor == sndMsgSizeWithNeighbor);
     }
    void testOLSRHelloInterfaceAddr(){
       //neighbors.push_back(IPAddr(3, 1));
        IPAddr origin;
        origin.to32bWord();
        neighbors.push_back(origin);
        uint32_t sndNeighborList = 4;
       olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
       //IPAddr intrfaceAddr(3, 1);
       uint32_t rcvdNeighborList = 0;
      memcpy(&rcvdNeighborList, buffer+8, sizeof(uint32_t));
      //CPPUNIT_ASSERT(neighborList == intrfaceAddr);
      CPPUNIT_ASSERT(rcvdNeighborList == sndNeighborList);
     }

    //*************************Start : Assignment 4 decoding part************************************//

    void testOLSRHelloDecodingMsgLenTooLarge(){

        IPAddr origin(169,254,98,105), neighbor(169,254,255,255), dest(169,254,255,255);
        neighbors.push_back(neighbor);
		//olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        OLSRHello::HeaderInfo hi;
        CPPUNIT_ASSERT(olsrh->isValid(buffer, 100, 0, hi) == false);
    }

    void testOLSRHelloDecodingInvalidOffset(){

        IPAddr origin(169,254,98,105), neighbor(169,254,255,255), dest(169,254,255,255);
        neighbors.push_back(neighbor);
		//olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        OLSRHello::HeaderInfo hi;
        CPPUNIT_ASSERT(olsrh->isValid(buffer, helloMessageSize, 2, hi) == false);
    }
    void testOLSRHelloDecodingLinkCodeInvalid(){
       IPAddr origin(169,254,98,105), neighbor(169,254,255,255), dest(169,254,255,255);
        neighbors.push_back(neighbor);
		//olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        // here, in buffer[4] we assigned as initially 06, so if we find this value in isValid
        //function then it would be clear that decoding is successful
        buffer[4] = 0x05; // generally link code value will be 0x06, to check abnormality we assign here 0x05
        OLSRHello::HeaderInfo hi;
        CPPUNIT_ASSERT(olsrh->isValid(buffer, helloMessageSize, 0, hi) == false);
    }
    void testOLSRHelloDecodingNotNeighbor(){

        IPAddr origin(169,254,98,105), neighbor(169,254,255,255), dest(169,254,255,255);
        neighbors.push_back(neighbor);
		//olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        OLSRHello::HeaderInfo hi;
        CPPUNIT_ASSERT(olsrh->isValid(buffer, helloMessageSize, 0, hi));
        CPPUNIT_ASSERT(hi.neighbors.empty());
        CPPUNIT_ASSERT(hi.type == OLSRHello::NeighborType::NOTN);
    }
    void testOLSRHelloDecodingSymNeighbor(){
        IPAddr origin(169,254,98,105), neighbor(169,254,255,255), dest(169,254,255,255);
        neighbors.push_back(neighbor);
		//olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        OLSRHello::HeaderInfo hi;
        hi.neighbors.push_back(neighbor);
        CPPUNIT_ASSERT(olsrh->isValid(buffer, helloMessageSize, 0, hi));
        CPPUNIT_ASSERT(hi.type == OLSRHello::NeighborType::SYM);
    }
    void testOLSRHelloDecodingLMOffsetEnd(){

        IPAddr origin(169,254,98,105), neighbor(169,254,255,254), dest(169,254,255,255);
        OLSRHello::HeaderInfo hi;
        hi.neighbors.push_back(neighbor);
        CPPUNIT_ASSERT(olsrh->isValid(buffer, helloMessageSize, 0, hi));
        // As there are only two neighbors(one from above function and one in this function),
        // So, we can expect that LM of Offset would be for just two neighbors that is:
        int expectedOffsetEnd = (hi.neighbors.size()*4) + 4;
        CPPUNIT_ASSERT(hi.LMOffsetEnd == expectedOffsetEnd);
    }

     //*************************End : Assignment 4 decoding part************************************//

};

#endif
