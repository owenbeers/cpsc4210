#include <sys/socket.h>
#include <sys/types.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "sock.h"

int main() {
  struct Ifconfig ifc;
  init(&ifc);

  printf("This Pi is ");
  /* print the MAC addr */
  int i;
  for (i=0; i<MACADDRLEN-1; i++)
    printf("%02X:", ifc.mac[i]);
  printf("%02X\n", ifc.mac[MACADDRLEN-1]);

  /* setting up the socket address structure for the source of the packet */
  struct sockaddr_ll from;
  socklen_t fromlen = sizeof(from);
  /* setting up the buffer for receiving */
  char * buf = malloc(ifc.mtu);
  if (buf == NULL)
    die("Cannot allocate receiving buffer\n");

  while (1) {
    int recvlen = recvfrom(ifc.sockd, buf, ifc.mtu, 0, (struct sockaddr*) &from, &fromlen);

    if (recvlen < 0) {
      printf("Cannot receive data: %s\n", strerror(errno));
    } else if (recvlen < 20) {
      printf("Received %d bytes from ", recvlen);
      for (i=0; i<from.sll_halen-1; i++)
	     printf("%02X:", from.sll_addr[i]);
      printf("%02X >\n", from.sll_addr[from.sll_halen-1]);

      /* print buffer content */
      printf("%s\n", buf);
    }
  }

  /* free resources */
  free(buf);
  destroy(&ifc);
  return 0;
}
