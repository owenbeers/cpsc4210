#include "packet.h"
#include <stddef.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>


//Initialize the packet

void initPacket(struct Packet *pack, struct mac_addr originatorAddress,
				struct mac_addr * symNeighborList, int symlen,
				struct mac_addr * asymNeighborList, int asymlen)
{
	short symLength, asymLength;

	pack->helloSYM = malloc((sizeof(struct Hello)));
	pack->helloASYM = malloc((sizeof(struct Hello)));

	symLength = initHellomsg(pack->helloSYM, originatorAddress, symNeighborList, symlen, 'S');
	asymLength = initHellomsg(pack->helloASYM, originatorAddress, asymNeighborList, asymlen, 'A');

	pack->packetLength = symLength + asymLength + 4;
	pack->packetSequenceNum = 0;
}

char * serializePacket(struct Packet *pack)
{	
	char * buffer;
	buffer = (char*) malloc(pack->packetLength);

	pack->packetLength = htons(pack->packetLength);
	memcpy(&buffer[0], &pack->packetLength, 2);
	pack->packetSequenceNum = htons(pack->packetSequenceNum);
	memcpy(&buffer[2], &pack->packetSequenceNum, 2);

	//Save sizes before serialization, or else they will be misterpreted
	int symSize = pack->helloSYM->messageSize, asymSize = pack->helloASYM->messageSize;
	//Take all of the hello messages (SYM) and put into the serializedPacket
	//modify proper lengths as necessary
	char * symBuff = serializeHelloMessage(pack->helloSYM);
	char * asymBuff = serializeHelloMessage(pack->helloASYM);
	memcpy(&buffer[4], symBuff, symSize);
	memcpy(&buffer[4+symSize], asymBuff, asymSize);

	free(symBuff);
	free(asymBuff);

	return buffer;
}

struct Packet * deserializePacket(char * message)
{	
	short packLength, messageSize1, messageSize2;
	memcpy(&packLength, &message[0], 2);
	packLength = ntohs(packLength);
	printf("Deserialize Packet: PacketLength: %d\n", packLength);
	struct Packet * pack = (struct Packet *)malloc(packLength);

	pack->packetLength = packLength;
	memcpy(&pack->packetSequenceNum, &message[2], 2);
	pack->packetSequenceNum = ntohs(pack->packetSequenceNum);

	// Next 2 bytes are Message Type and VTIME (we can skip these for now)
	memcpy(&messageSize1, &message[6], 2);
	messageSize1 = ntohs(messageSize1);
	memcpy(&messageSize2, &message[6+messageSize1], 2);
	messageSize2 = ntohs(messageSize2);


	char * symMessage = &message[4], * asymMessage = &message[4+messageSize1];
	pack->helloSYM = deserializeHelloMessage((void*)symMessage, messageSize1);
	pack->helloASYM = deserializeHelloMessage((void*)asymMessage, messageSize2);


/* PRINT TESTING? USE FOR DEMO?
	printf("\nHELLO NEIGHBORS DESERIALIZE PACKET\n");
	printf("LINK CODE HELLOSYM: %d \n", pack->helloSYM->linkCode);
	int j;
	int k=1;
	for(j=0; j < pack->helloSYM->linkMsgSize-8; j++)
	{
		if(k%4 == 0)
		{
			printf("%d", pack->helloSYM->neighborList[j]);
		}
		else
			printf("%d:", pack->helloSYM->neighborList[j]);
		if(k%4==0)
		{
			printf("\n");
		}
		k++;
	}


	printf("\nHELLO NEIGHBORS DESERIALIZE PACKET\n");
	printf("LINK CODE HELLOSYM: %d \n", pack->helloSYM.linkCode);
	int j;
	int k=1;
	for(j=0; j < pack->helloSYM.linkMsgSize-8; j++)
	{
		if(k%4 == 0)
		{
			printf("%d", pack->helloSYM.neighborList[j]);
		}
		else
			printf("%d:", pack->helloSYM.neighborList[j]);
		if(k%4==0)
		{
			printf("\n");
		}
		k++;
	}

	printf("\nHELLO NEIGHBORS\n");
	printf("LINK CODE HELLO ASYM: %d \n", pack->helloASYM->linkCode);
	j=0;
	k=1;
	for(j=0; j < pack->helloASYM->linkMsgSize-8; j++)
	{
		if(k%4 == 0)
		{
			printf("%d", pack->helloASYM->neighborList[j]);
		}
		else
			printf("%d:", pack->helloASYM->neighborList[j]);
		if(k%4==0)
		{
			printf("\n");
		}
		k++;
	}
*/
	return pack;
}


void destroyPacket(struct Packet * ptr)
{
	free(ptr->helloASYM->neighborList);
	free(ptr->helloSYM->neighborList);
	free(ptr->helloASYM);
	free(ptr->helloSYM);
	free(ptr);
}
