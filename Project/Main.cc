#include <iostream>
#include <mutex>
#include <thread>
#include <chrono>
#include <sys/socket.h>
#include <sys/types.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <vector>

#include "ConcreteOLSRHello.h"
#include "ConcreteOLSRPacket.h"
#include "ConcreteIPUDPPacket.h"
#include "IPAddr.h"
//extern "C"{
	#include "sock.h"
//}

/*Compile when you can (I can't), there are likely some errors to fix before you start*/

/*Global Neighbor Structure*/
std::vector<IPAddr> neighborList; // Make sure you use mutexes when changing this structure
std::vector<int> neighborTimer;

//senderThread
void sendEvery3Seconds()
{
	struct Ifconfig ifc_send;
	init(&ifc_send);

	/*Set up Sending Socket*/
	struct sockaddr_ll to;
	memset(&to, 0, sizeof(to));
	to.sll_family = AF_PACKET; 
	to.sll_ifindex = ifc_send.ifindex;
	to.sll_halen = MACADDRLEN; 

	while(1)
	{
		IPAddr src = IPAddr(2, 2);
		IPAddr dst;

		unsigned char * buffer;
		const int buflen = 1000;
		buffer = new unsigned char [buflen];

		ConcreteIPUDPPacket IPUDP;
		ConcreteOLSRPacket pack;
		
		int header_len = IPUDP.setHeader(buffer, buflen, src, dst, 1, 11);
		
		header_len += pack.setHeader(buffer+28, buflen, 1, src, 6, 1, 1, 1, 1);
		// for (int i = 0; i < 60; ++i)
		// {
		// 	printf("\n byte %d = %02X", i, buffer[i]);
		// }
		printf("\nSend\n");
		int sendStruct = sendto(ifc_send.sockd, buffer, header_len, 0, (struct sockaddr*) &to, sizeof(to));
		delete[] buffer;
		std::this_thread::sleep_for (std::chrono::seconds(3));
	}
}

//receiverThread
void receiveAllData()
{	
	/*Set up Receive struct*/
	struct Ifconfig ifc_receive;
	init(&ifc_receive);
	
	/*Set up Mutexes*/
	std::mutex neighbourhood_lock;
	std::mutex log_lock;

	/*Set up Timer*/
	time_t timer;
	struct tm * timeinfo;

	uint32_t originalIP;
	uint16_t protocol;
	uint8_t msg;

	bool flag = false;

	while(1)
	{
		//Setup char * to send a string
		const char *neighCStr;
		string neighString;
		bool IPFound = false;

		/*Set up Receive Buffer*/
		unsigned char * buffer;
		const int buflen = 1000;
		buffer = new unsigned char [buflen];

		FILE * file;
		file = fopen("log.txt", "a+"); // SHOULD BE APPENDED
		if(file==NULL)
		printf("error opening file\n");
		struct Ifconfig ifc_send;
		init(&ifc_send);

		/*Set up Sending Socket*/
		struct sockaddr_ll from;
		memset(&from, 0, sizeof(from));
		from.sll_family = AF_PACKET; 
		from.sll_ifindex = ifc_send.ifindex;
		from.sll_halen = MACADDRLEN; 
		socklen_t fromlen = sizeof(from);

		printf("\nReceive\n");
		int recvlen = recvfrom(ifc_receive.sockd, buffer, ifc_receive.mtu, 0, (struct sockaddr*) &from, &fromlen);

		if (recvlen < 0)
		{
			printf("Cannot receive data: %s\n", strerror(errno));
		} 
		else if (recvlen > 20 && recvlen < 200)
		{
			printf("Received %d bytes from ", recvlen);
			for (int i=0; i<from.sll_halen-1; i++)
	     		printf("%02X:", from.sll_addr[i]);


			if(((buffer[0] >> 4) != 4)){
				printf("\nbuffer[0] = %02X", buffer[0]);
				std::cout << "Bad Version" << std::endl;
				continue;
			}
			std::memcpy(&protocol, buffer+20 , sizeof(uint16_t));
			protocol = ntohs(protocol);
			if(protocol != 698) { //0x02BA
				if (protocol != 47618)
				{
					printf("\nprotocol = %d", protocol);
					std::cout << "Bad Protocol" << std::endl;
					continue;
				}
			}
			std::memcpy(&msg, buffer+32 , sizeof(uint8_t));
			if(msg != 1){
				std::cout << "Bad Msg" << std::endl;
				continue;	
			}

			std::memcpy(&originalIP, buffer+36 , sizeof(uint32_t));
			IPAddr currentIP = IPAddr(originalIP);

			//Print sender IP (project asks for debugging)
			if(currentIP == IPAddr(2, 2) && flag)
			{
				printf("\nReceived from self\n");
				continue;
			}
			else
				flag = true;

			std::cout << "Received From IP Address: " << currentIP.toString() << std::endl;

			//Add this originator to our neighbour list if they aren't in it already
			for(unsigned int j=0;j<neighborList.size();j++){
				if(currentIP == neighborList[j]) {
					IPFound = true;
					std::cout << "Duplicate IP Address: " << currentIP.toString() << std::endl;
				}
			}
			if(!IPFound){
				neighborList.push_back(currentIP);
				neighborTimer.push_back(2);
				std::lock_guard<std::mutex> lock(log_lock);
				time (&timer);
				timeinfo = localtime(&timer);
				fprintf(file, "Current Time: %s", asctime(timeinfo));
				fprintf(file, "Neighbor List:");
				for(unsigned int i=0; i < neighborList.size(); i++)
				{
					neighString = neighborList[i].toString();
					neighCStr = neighString.c_str();
					fprintf(file, "%s ,", neighCStr);
				}
				log_lock.unlock();
			}
		}
		fclose(file);
		delete[] buffer;
	}
}

//updaterThread
void updateNeighbors()
{

	const char *neighCStr;
	string neighString;

	/*Set up Mutexes*/
	std::mutex neighbourhood_lock;
	std::mutex log_lock;

	/*Set up Timer*/
	time_t timer;
	struct tm * timeinfo;

	while(1)
	{
		FILE * file;
		file = fopen("log.txt", "a+"); // SHOULD BE APPENDED
		if(file==NULL)
		printf("error opening file\n");
		std::lock_guard<std::mutex> lock(neighbourhood_lock); // Destroyed when we leave function
		bool listChanged = false;
		//Update neighbor structure
		for(unsigned int i=0; i < neighborTimer.size(); i++)
		{
			neighborTimer[i]--;
			if(neighborTimer[i] == 0)
			{
				neighborTimer.erase(neighborTimer.begin()+i);
				neighborList.erase(neighborList.begin()+i);
				listChanged = true;
			}
		}
		if(listChanged)
		{
			//If anything changes, logfile must be updated (NEEDS IF STATEMENT)
			std::lock_guard<std::mutex> lock2(log_lock);
			time (&timer);
			timeinfo = localtime(&timer);
			fprintf(file, "Current Time: %s", asctime(timeinfo));
			fprintf(file, "Neighbor List:");
			for(unsigned int i=0; i < neighborList.size(); i++)
			{
					neighString = neighborList[i].toString();
					neighCStr = neighString.c_str();
					fprintf(file, "\n%s\n ,", neighCStr);
			}
			log_lock.unlock();
			neighbourhood_lock.unlock();
			fclose(file);
		}
		std::this_thread::sleep_for (std::chrono::seconds(3));
	}
}

int main()
{
	std::mutex log_lock;

	const char * IPCStr;
	string IPString;

	/*Set up Log File*/
	FILE * file;
	file = fopen("log.txt", "a+"); // SHOULD BE APPENDED
	if(file==NULL)
		printf("error opening file\n");

	/*Set up Timer*/
	time_t timer;
	struct tm * timeinfo;

	/*Print IP Address to screen*/
	IPAddr src = IPAddr(2, 2);
	std::cout << "This Pi is " << src.toString() << std::endl;

	/*Print IP and Time to Log*/
	std::lock_guard<std::mutex> lock(log_lock);
	time (&timer);
	timeinfo = localtime(&timer);
	fprintf(file, "Current Time: %s", asctime(timeinfo));
	IPString = src.toString();
	IPCStr = IPString.c_str();
	fprintf(file, "This IP: %s \n", IPCStr);
	log_lock.unlock();
	fclose(file);

	struct Ifconfig ifc_send;
	init(&ifc_send);

	/*Set up Sending Socket*/
	struct sockaddr_ll to;
	memset(&to, 0, sizeof(to));
	to.sll_family = AF_PACKET; 
	to.sll_ifindex = ifc_send.ifindex;
	to.sll_halen = MACADDRLEN; 

	/* setup broadcast address of FF:FF:FF:FF:FF:FF */
	for (int i=0; i<MACADDRLEN; i++)
		to.sll_addr[i] = 0xff;

	/*Set up Receive Socket*/
	struct sockaddr_ll from;
	socklen_t fromlen = sizeof(from);

	/*Start Threads*/
	std::thread senderThread(sendEvery3Seconds); //Functions called are loops
	std::thread receiverThread(receiveAllData); 
	std::thread updaterThread(updateNeighbors); 

	/*Synchronize Threads*/
	senderThread.join();
	receiverThread.join();
	updaterThread.join();

	fclose(file);
	return 0;
}
/*OLD Stuff - Probably dont need*/
/*
	struct mac_addr asymList[10];
	struct mac_addr symList[10];
	struct mac_addr twoHopList[10];

	struct mac_addr * asymPtr = malloc(sizeof(struct mac_addr));
	asymPtr = &asymList[0];

	struct mac_addr * symPtr = malloc(sizeof(struct mac_addr));
	symPtr = &symList[0];

	struct mac_addr * twoHopPtr = malloc(sizeof(struct mac_addr));
	twoHopPtr = &twoHopList[0];

	int sizeAsym = 0;
	int sizeSym = 0;
	int sizeTwoHop = 0;
	int *sizeAsymPtr = &sizeAsym;
	int *sizeSymPtr = &sizeSym;
	int *sizeTwoHopPtr = &sizeTwoHop;

	int timer=0;
	
	while(1)
	{
		//Init hellomsg
		struct Packet * pack = malloc(sizeof(struct Packet));//, *packPtr;
		initPacket(pack, ourMAC, symList, sizeSym, asymList, sizeAsym);
		int packetLength = pack->packetLength;

		char *sendBuffer = serializePacket(pack);

		// Send the hello message after it has been initialized
		int sendStruct = sendto(ifc_send.sockd, sendBuffer, packetLength, 0, (struct sockaddr*) &to, sizeof(to));
		printf("%d bytes sent\n", sendStruct);

  		sleep(1);
  		timer+=1;

		int recvlen = recvfrom(ifc_receive.sockd, buf_receive, ifc_receive.mtu, 0, (struct sockaddr*) &from, &fromlen);
		if (recvlen < 0)
		{
			printf("Cannot receive data: %s\n", strerror(errno));
		} 
		else if (recvlen > 20 && recvlen < 200)
		{
			printf("Received %d bytes from ", recvlen);

			for (i=0; i<from.sll_halen-1; i++)
	     		printf("%02X:", from.sll_addr[i]);
      		printf("%02X >\n", from.sll_addr[from.sll_halen-1]);

      		if(buf_receive[4] != 0x01)
      			continue;
      		 // message type == 0x01
				
			struct Packet *recHi;
			recHi = deserializePacket(buf_receive);
			struct mac_addr senderMAC;
			senderMAC = recHi->helloSYM->originatorAddress;

			if(ourMAC.bytes[0] == senderMAC.bytes[0])
			{
				printf("Received from ourselves\n");
				continue;
			}


			recvAddressList(timer, recHi->helloSYM, asymPtr, symPtr, twoHopPtr, senderMAC, ourMAC, sizeAsymPtr, sizeSymPtr, sizeTwoHopPtr, file);
			recvAddressList(timer, recHi->helloASYM, asymPtr, symPtr, twoHopPtr, senderMAC, ourMAC, sizeAsymPtr, sizeSymPtr, sizeTwoHopPtr, file);
			/* print buffer content 
			printf("%s\n", buf_receive);
			destroyPacket(recHi);
  		}

  		checkExpiry(asymPtr, symPtr, twoHopPtr, sizeAsymPtr, sizeSymPtr, sizeTwoHopPtr, timer, file);

  		free(buf_receive);
  		buf_receive = malloc(ifc_receive.mtu);
  		destroyPacket(pack);
		
	}

	fclose(file);
	free(asymPtr);
	free(symPtr);
	free(twoHopPtr);
	free(sizeSymPtr);
	free(sizeAsymPtr);
	free(sizeTwoHopPtr);
	destroy(&ifc_receive);
	destroy(&ifc_send);
	return 0;
}
*/
