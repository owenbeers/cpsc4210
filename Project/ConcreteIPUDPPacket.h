// *** CPSC 4210/5210 Assignment 3 code
// ***** IPUDPPacket interface: creates the combined header of IP/UDP packed

#ifndef __CONCRETEIPUDPPACKET_H
#define __CONCRETEIPUDPPACKET_H

#include <cstring>
#include "IPUDPPacket.h"
#include <iostream>

class ConcreteIPUDPPacket : public IPUDPPacket {
public:
	// Can provide any constructors needed; the sample relies on the
	// default constructor supplied by the compiler

	// concrete implementation for the header routine
	int setHeader(unsigned char * buf, int buflen, IPAddr src,
								IPAddr dest, int TTL, int datalen) {
		// we don't do much, just fill the data length part of the header,
		// as demo.

		// first check if the buffer is big enough
		if (buflen < datalen+28)
			return -1;

		//Setup Version and IHL
		unsigned char Version, IHL, VIHL;
		Version = 4 << 4;
		IHL = 5;
		VIHL = Version | IHL;

		//Find Checksum
		int checkSumVal;
		checkSumVal = (int)(Version >> 4);
		checkSumVal += (int)IHL + 0 + (28+datalen) + 1 + 0 + TTL;
		
		std::memset(buf, 0, buflen);  // we clear the buffer first!
		*(buf) 								  		= VIHL;  // Version IHL
		*(buf+1)									= 1; //ToS
		*(reinterpret_cast<uint16_t*>(buf+2)) 	 	= 28+datalen;  // packet
		*(reinterpret_cast<uint16_t*>(buf+4))  	 	= 1; // Hello message Identification
		*(buf+6)  									= 0; // Flags and Fragment Offset
		*(reinterpret_cast<uint8_t*>(buf+8))		= TTL; // TTL
		*(buf+9)									= 138 | 0x00; // Protocol
		//Checksum GARBAGE
		*(buf+10) 								 	= checkSumVal;
		*(reinterpret_cast<uint32_t*>(buf+12))  	= src.to32bWord(); // source IP
		*(reinterpret_cast<uint32_t*>(buf+16))  	= dest.to32bWord(); // dest IP
		*(reinterpret_cast<uint32_t*>(buf+20))  	= htons(698); // Src Port
		*(reinterpret_cast<uint32_t*>(buf+22))  	= htons(698); // Dst Port
		//We need to implement this function later.
		*(reinterpret_cast<uint32_t*>(buf+24))  	= 1000; // Length
		*(reinterpret_cast<uint32_t*>(buf+26))  	= 0; // Checksum

																										// length field
		return 28;  // the size of the header in bytes
	}
	bool isValid(unsigned char * buf, int buflen, HeaderInfo & hi) {

		if(hi.dataLen + hi.headerLen != buflen)
			return false;
		uint16_t VIHL;
		std::memcpy(&VIHL, buf , sizeof(uint16_t));
		if((VIHL >> 4) != 4)
			return false;
		if((VIHL & 0x05) != 5)
			return false;
		uint16_t ToS;
		std::memcpy(&ToS, buf+1 , sizeof(uint16_t));
		if(ToS != 0)
			return false;
		uint16_t length;
		std::memcpy(&length, buf+2 , sizeof(uint16_t));
		uint16_t actualLength = (length >> 8);
		actualLength += (length & 0x0F);
		if(actualLength != 120)
			return false;
		uint32_t identity;
		std::memcpy(&identity, buf+4 , sizeof(uint32_t));
		if(identity != 0)
			return false;
		uint8_t TTL;
		std::memcpy(&TTL, buf+8 , sizeof(uint8_t));
		if(TTL != 1)
			return false;
		uint16_t protocol;
		std::memcpy(&protocol, buf+9 , sizeof(uint16_t));
		if(protocol != 138)
			return false;
		uint16_t srcPort;
		std::memcpy(&srcPort, buf+20 , sizeof(uint16_t));
		//Bytes swapped
		if(srcPort != 0xba02)
			return false;
		uint16_t destPort;
		std::memcpy(&destPort, buf+22 , sizeof(uint16_t));
		//Bytes swapped
		if(destPort != 0xba02)
			return false;
		uint16_t udpLength;
		std::memcpy(&udpLength, buf+24 , sizeof(uint16_t));
		if((udpLength >> 8) != 100)
			return false;
		uint16_t udpChecksum;
		std::memcpy(&udpChecksum, buf+26 , sizeof(uint32_t));
		if(udpChecksum != 0)
			return false;
		//IP Checksum Calculation
		uint16_t expectedChecksum = 0xF687;
		//Add everything except the Checksum
		uint16_t ipChecksum = 4+5+0+0+hi.headerLen+hi.dataLen+0+0+0+0+1+138+169+254+1+1+169+254+255+255+512+186+0+100+0+0;
		ipChecksum = (ipChecksum & 0xFFFF);
		ipChecksum = (~ipChecksum);
		if(ipChecksum != expectedChecksum)
			return false;

		//2424(dec)	,		0000100101111000(bin)
		//1's Compliment
		//Add last 4: 	0000	+	0000100101111000
		//Flip:						1111011010000111
		//In dec					63111(dec)
		//In Hex					F687(hex)

		return true;
	}
};

#endif
