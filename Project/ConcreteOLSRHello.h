// *** CPSC 4210/5210 Assignment 3 code
// ***** IPUDPPacket interface: creates the combined header of IP/UDP packed

#ifndef __CONCRETEOLSRHELLO_H
#define __CONCRETEOLSRHELLO_H

#include <cstring>
#include <iostream>
#include "OLSRHello.h"

class ConcreteOLSRHello : public OLSRHello {
public:
	// Can provide any constructors needed; the sample relies on the
	// default constructor supplied by the compiler

	// concrete implementation for the header routine
	int setHeader(unsigned char * buf, int buflen, std::vector<IPAddr> mpr,
								std::vector<IPAddr> nbours) {
		int headerLen = 8;

		uint16_t reserved = 0;
		uint8_t reserved2 = 0;
		uint8_t Htime = 0;
		uint8_t Willingness = 3;


		// first check if the buffer is big enough
		if (buflen < headerLen + (4*(int)nbours.size()) + (4*(int)mpr.size()))
			return -1;
		
		std::memset(buf, 0, buflen);  // we clear the buffer first!
		*(reinterpret_cast<uint16_t*>(buf)) = reserved;
		*(reinterpret_cast<uint8_t*>(buf+2)) = Htime;
		*(reinterpret_cast<uint8_t*>(buf+3)) = Willingness;

		uint8_t LinkCode;

		if(((int)(nbours.size()) + (int)(mpr.size())) == 0)
			LinkCode = 0;
		else
			LinkCode = 6;

		*(reinterpret_cast<uint8_t*>(buf+4)) = LinkCode;
		*(reinterpret_cast<uint8_t*>(buf+5)) = reserved2;

		uint16_t LinkMessageSize = 4+(4*(int)mpr.size()) + (4* (int)nbours.size());  // set the packet length field

		*(reinterpret_cast<uint16_t*>(buf+6)) = LinkMessageSize;

		if(mpr.size() < 1 && nbours.size() < 1)
			return 8 + (4*(int)mpr.size()) + (4*(int)nbours.size());  // the size of the header in bytes
		else if(mpr.size() < 1 && nbours.size() > 0)
		{
			for(unsigned int i = 0; i < nbours.size(); i++)
				*(reinterpret_cast<uint32_t*>(buf+8+(i*4))) = nbours[i].to32bWord();
		}
		else if(mpr.size() > 0 && nbours.size() < 1)
		{
			for(unsigned int i = 0; i < mpr.size(); i++)
				*(reinterpret_cast<uint32_t*>(buf+8+(i*4))) = mpr[i].to32bWord();
		}
		else if(mpr.size() > 0 && nbours.size() > 0)
		{
			for(unsigned int i= 0; i < mpr.size(); i++)
				*(reinterpret_cast<uint32_t*>(buf+8+(i*4))) = mpr[i].to32bWord();
			for(unsigned int j= 0; j < nbours.size(); j++)
				*(reinterpret_cast<uint32_t*>(buf+8+(mpr.size()*4)+(j*4))) = nbours[j].to32bWord();
		}

		return 8 + (4*(int)mpr.size()) + (4*(int)nbours.size());  // the size of the header in bytes
	}

	bool isValid(unsigned char * buf, int msglen, int offset, HeaderInfo & hi) {
		if(offset > msglen-4)
			return false;
		if(offset < 0)
			return false;

		//Test all invalid values of linkcode
		uint8_t linkcode;
		std::memcpy(&linkcode, buf+offset, sizeof(uint8_t));
		if(linkcode != 0 && linkcode !=6)
			return false;

		if(linkcode == 0)
		{
			if(offset+4 != msglen) // If there are neighbours in this list, linkcode 0 is invalid
				return false;
			hi.type = NeighborType::NOTN;
			offset += 4;
			hi.LMOffsetEnd = offset;
			/*Loop is not necessary for an empty list
			while(offset != msglen)
			{
				uint32_t neighbour;
				std::memcpy(&neighbour, buf+offset, sizeof(uint32_t));
				hi.neighbors.push_back(IPAddr(neighbour));
				offset+=4;
			}
			*/
		}
		if(linkcode == 6)
		{
			
			hi.type = NeighborType::SYM;
			offset+=4; //set offset to the start of the neighbour list
			if(offset == msglen) //If there are no neighbours, link code 6 is invalid
				return false;
			while(offset != msglen)
			{
				IPAddr neighbor;
				std::memcpy(&neighbor, buf+offset, sizeof(uint32_t));
				hi.neighbors.push_back(IPAddr(neighbor));
				offset+=4;
			}
			hi.LMOffsetEnd = offset;
			
		}
		return true;
	}
};

#endif
