// *** CPSC 4210/5210 Assignment 3 code
// ***** IPUDPPacket interface: creates the combined header of IP/UDP packed

#ifndef __IPUDPPACKET_H
#define __IPUDPPACKET_H

#include "IPAddr.h"

// interface class; provide a concrete class implementing the member function.
// Note this is an instance of the Adapter design pattern: you will
// adapt your existing code that creates packet headers to the
// provided interface. 
class IPUDPPacket {
public:
	// assigns values according to the IP/UDP packet header to the
	// buffer buf.
	// buflen is the size of the buffer in bytes
	// src is the IP address of the source
	// dest is the IP address of the destination
	// TTL is the value of the time to live field
	// datalen is the size of the data portion following the IP+UDP
	// header
	// RETURNS
	// the length of the header in bytes, or -1 if the buffer is not big
	// enough to store the data and the headers
	virtual int setHeader(unsigned char * buf, int buflen, IPAddr src,
								 IPAddr dest, int TTL, int datalen)=0;

	virtual ~IPUDPPacket() {}
};

#endif
