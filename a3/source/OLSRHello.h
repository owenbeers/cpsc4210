// *** CPSC 4210/5210 Assignment 3 code
// ***** OLSRHello interface: creates the header of a Hello Message

#ifndef __OLSRHELLO_H
#define __OLSRHELLO_H

#include <vector>
#include "IPAddr.h"

// interface class; provide a concrete class implementing the member function.
// Note this is an instance of the Adapter design pattern: you will
// adapt your existing code that creates packet headers to the
// provided interface. 
class OLSRHello {
public:
	// assigns values according to the OLSR packet header to the
	// buffer buf.
	// buflen is the size of the buffer in bytes
	// mpr is a vector of IP addresses selected to be MPR's
	// nbours is a vector of IP addresses that are the neighbour nodes
	// RETURNS
	// the length of the entire message in bytes, or -1 if the buffer is not big
	// enough to store the message and the headers
	virtual int setHeader(unsigned char * buf, int buflen, std::vector<IPAddr> mpr,
												std::vector<IPAddr> nbours)=0;

	virtual ~OLSRHello() {}
};

#endif
