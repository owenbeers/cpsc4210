#ifndef __PACKET_H_
#define __PACKET_H_

#include "helloMsg.h"
#include "mac.h"
//Include
/*
Char * == 1 byte
Short == 2 bytes
long == 4 bytes
*/

struct Packet
{
	short packetLength;
	short packetSequenceNum;
	struct Hello * helloASYM;
	struct Hello * helloSYM;
};

//Initialize the packet
void initPacket(struct Packet *pack, struct mac_addr originatorAddress,
				struct mac_addr * symNeighborList, int symlen,
				struct mac_addr * asymNeighborList, int asymLen);
char * serializePacket(struct Packet *pack);
struct Packet * deserializePacket(char * message);
void destroyPacket(struct Packet * pack);


#endif