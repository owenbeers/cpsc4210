// Cody Barnson
// Group Pi4

#ifndef __TESTOLSRPACKETPI4_H
#define __TESTOLSRPACKETPI4_H

#include <cppunit/TextTestRunner.h>
#include <cppunit/TextOutputter.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestListener.h>
#include <cppunit/extensions/TestSuiteBuilderContext.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/ui/text/TextTestRunner.h>

#include <cstdlib>
#include <iostream>
#include <memory>

#include <arpa/inet.h>

#include "IPAddr.h"
#include "OLSRPacket.h"
#include "OLSRPacketFactory.h"

/// Test fixture for ProgressiveRateStrategy
class TestOLSRPacketPi4 : public CppUnit::TestFixture {
   CPPUNIT_TEST_SUITE(TestOLSRPacketPi4);
   CPPUNIT_TEST(testOLSRPacketDataTooLarge);

    CPPUNIT_TEST(testOLSRPacketFieldPacketLength); 
    CPPUNIT_TEST(testOLSRPacketFieldPacketSequenceNumber); 
    CPPUNIT_TEST(testOLSRPacketFieldMessageTypeIsHello); 
    CPPUNIT_TEST(testOLSRPacketFieldVtimeDefault); 
    CPPUNIT_TEST(testOLSRPacketFieldMessageSize); 
    CPPUNIT_TEST(testOLSRPacketFieldOriginatorAddress); 
    CPPUNIT_TEST(testOLSRPacketFieldTTL); 
    CPPUNIT_TEST(testOLSRPacketFieldHopCount); 
    CPPUNIT_TEST(testOLSRPacketFieldMessageSequenceNumber); 
   
   CPPUNIT_TEST_SUITE_END();
  private:
   // we make the factory static so that we can initialize it in the
   // .cc file to the concrete factory that returns the concrete object
   // with our implementation of setHeader()
   static std::shared_ptr<OLSRPacketFactory> OLSRfactory;

  private:
   // the packet object being tested
   std::shared_ptr<OLSRPacket> olsr;
   // the buffer used for testing
   unsigned char * buffer;
   // the buffer length
   const int buflen = 1000;
	

  public:
   void setUp()  {
      if (OLSRfactory)
	 olsr = OLSRfactory->createOLSRPacket();
      else {
	 std::cerr << "TestOLSRPacketPi4 test fixture not properly initialized!\n";
	 exit(EXIT_FAILURE);
      }
      buffer = new unsigned char [buflen];
   };
	
   void tearDown()  {
      delete[] buffer;
      // we don't need to delete ipudp since it is a smart pointer
   };

   // *************** test cases ************
   void testOLSRPacketDataTooLarge() {
       IPAddr origin;
       int header_len = olsr->setHeader(buffer, 16+10, 1, origin, 1, 0, 33, 66, 11); 
      CPPUNIT_ASSERT(header_len == -1);
   }

   void testOLSRPacketFieldPacketLength() {
      IPAddr origin;
       int header_len = olsr->setHeader(buffer, 16+10, 1, origin, 1, 0, 33, 66, 10);
       uint16_t packet_len = 0;
       memcpy(&packet_len, buffer, sizeof(uint16_t));
       packet_len = ntohs(packet_len);
       CPPUNIT_ASSERT(packet_len == 26);
   }


   void testOLSRPacketFieldPacketSequenceNumber() {
      IPAddr origin;
       int header_len = olsr->setHeader(buffer, 16+10, 1, origin, 1, 0, 33, 66, 10);
       uint16_t packet_seq_num = 0;
       memcpy(&packet_seq_num, buffer+2, sizeof(uint16_t));
       packet_seq_num = ntohs(packet_seq_num);
       CPPUNIT_ASSERT(packet_seq_num == 33);
   }

   void testOLSRPacketFieldMessageTypeIsHello() {
      IPAddr origin;
       int header_len = olsr->setHeader(buffer, 16+10, 1, origin, 1, 0, 33, 66, 10);
       uint8_t msg_type = 0;
       memcpy(&msg_type, buffer+4, sizeof(uint8_t));
       CPPUNIT_ASSERT(msg_type == 1);
   }

   // note default vtime configured to be 6.0 seconds 
   void testOLSRPacketFieldVtimeDefault() {
      IPAddr origin;
       int header_len = olsr->setHeader(buffer, 16+10, 1, origin, 1, 0, 33, 66, 10);
       uint8_t t = 0;
       memcpy(&t, buffer+5, sizeof(uint8_t));
       int b = (int)(std::bitset<4>(t).to_ulong());
      int a = (int)(t >> 4);
      // to be safe, pow unpredictable...
      int bb = 1;
      for (int i = 0; i < b; i++)
         bb *= 2;
      const double SCALING_FACTOR = 0.0625; //seconds
      double val = (SCALING_FACTOR * (1.0 + ((double)a / 16)) * (double)bb);
      CPPUNIT_ASSERT(val == 6.0);
   }

   void testOLSRPacketFieldMessageSize() {
      IPAddr origin;
       int header_len = olsr->setHeader(buffer, 16+10, 1, origin, 1, 0, 33, 66, 10);
       uint16_t msg_size = 0;
       memcpy(&msg_size, buffer+6, sizeof(uint16_t));
       //msg_size = ntohs(msg_size);
       CPPUNIT_ASSERT(msg_size == 12+10);
   }

   void testOLSRPacketFieldOriginatorAddress() {
      IPAddr origin(4, 2); // pi 4 node 2
       int header_len = olsr->setHeader(buffer, 16+10, 1, origin, 1, 0, 33, 66, 10);
       uint32_t addr = 0;
       memcpy(&addr, buffer+8, sizeof(uint32_t));
       addr = ntohl(addr);
       CPPUNIT_ASSERT(addr == origin.to32bWord());
   }

   void testOLSRPacketFieldTTL() {
      IPAddr origin;
      int ttl_param = 2;
       int header_len = olsr->setHeader(buffer, 16+10, 1, origin, ttl_param, 0, 33, 66, 10);
       uint8_t ttl = 0;
       memcpy(&ttl, buffer+12, sizeof(uint8_t));
       CPPUNIT_ASSERT(ttl == ttl_param);
   }

   void testOLSRPacketFieldHopCount() {
      IPAddr origin;
      int hop_param = 2;
       int header_len = olsr->setHeader(buffer, 16+10, 1, origin, 1, hop_param, 33, 66, 10);
       uint8_t hop = 0;
       memcpy(&hop, buffer+13, sizeof(uint8_t));
       CPPUNIT_ASSERT(hop == hop_param);
   }

   void testOLSRPacketFieldMessageSequenceNumber() {
      IPAddr origin;
      int msn = 401;
       int header_len = olsr->setHeader(buffer, 16+10, 1, origin, 1, 0, 33, msn, 10);
       uint16_t mseq = 0;
       memcpy(&mseq, buffer+14, sizeof(uint16_t));
       mseq = ntohs(mseq);
       CPPUNIT_ASSERT(mseq == msn);
   }

   
};

#endif
