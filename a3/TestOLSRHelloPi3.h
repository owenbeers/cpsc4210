
#ifndef __TESTOLSRHELLOPI3_H
#define __TESTOLSRHELLOPI3_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <bitset>
#include "OLSRHelloFactory.h"
#include "IPAddr.h"
//#include "OLSRHelloTestCases.h"

using namespace std;


/// Test fixture for ProgressiveRateStrategy
class TestOLSRHelloPi3 : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(TestOLSRHelloPi3);
	CPPUNIT_TEST(sampleTestOLSRHelloTooLarge);

    CPPUNIT_TEST(testOLSRHelloFrontReserved);
	CPPUNIT_TEST(testOLSRHelloHtime);
	CPPUNIT_TEST(testOLSRHelloWillingness);
	CPPUNIT_TEST(testOLSRHelloLinkCodeUnspecLink);
    //CPPUNIT_TEST(testOLSRHelloSymLink);
	CPPUNIT_TEST(testOLSRHelloMidReserved);
    CPPUNIT_TEST(testOLSRHelloLinkMessageSizeWithoutNeighbor);
   // CPPUNIT_TEST(testOLSRHelloLinkMessageWithNeighbor);
    CPPUNIT_TEST(testOLSRHelloInterfaceAddr);

	CPPUNIT_TEST_SUITE_END();
private:
	// we make the factory static so that we can initialize it in the
	// .cc file to the concrete factory that returns the concrete object
	// with our implementation of setHeader()
	static std::shared_ptr<OLSRHelloFactory> factory;

private:
	// the packet object being tested
	std::shared_ptr<OLSRHello> olsrh;
	// the buffer used for testing
	unsigned char * buffer;
	// the buffer length
	const int buflen = 1000;
    std::vector<IPAddr> mpr;
    std::vector<IPAddr> neighbors;
    int helloMessageSize = 45;



public:

	void setUp()  {
		if (factory){
			olsrh = factory->createOLSRHello();

		}
		else {
	    std::cerr << "TestIPUDPSample test fixture not properly initialized!\n";
			exit(EXIT_FAILURE);
		}
		buffer = new unsigned char [buflen];
	};

	void tearDown()  {
		delete[] buffer;
		// we don't need to delete ipudp since it is a smart pointer
		mpr.clear();
		neighbors.clear();
	};

	// *************** test cases ************
	void sampleTestOLSRHelloTooLarge() {
        int helloMessageSize = 52;
		int header_len = olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
		CPPUNIT_ASSERT(header_len == -1);
	}

    void testOLSRHelloFrontReserved() {

        uint16_t sndReserved1 = 0 ; // According to RFC 3626 document: assuming message Size will be 25
		int header_len = olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        uint16_t rcvdReserved1 = 0;
        memcpy(&rcvdReserved1, buffer, sizeof(uint16_t));
        CPPUNIT_ASSERT(rcvdReserved1 == sndReserved1);
     }

    void testOLSRHelloHtime(){

        uint8_t sndHTime = 0 ; // According to RFC 3626 document: assuming message Size will be 25
		int header_len = olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        uint8_t rcvdhTime = 0;
        memcpy(&rcvdhTime, buffer+2, sizeof(uint8_t));
        CPPUNIT_ASSERT(rcvdhTime == sndHTime);
     }

    void testOLSRHelloWillingness() {

        uint8_t sndWillingness = 3 ; // According to RFC 3626 document: assuming message Size will be 25
		int header_len = olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        uint8_t rcvdWillingness = 0;
        memcpy(&rcvdWillingness, buffer+3, sizeof(uint8_t));
        CPPUNIT_ASSERT(rcvdWillingness == sndWillingness);
	}

    void testOLSRHelloLinkCodeUnspecLink() {

        uint8_t sndLinkCodeUnspec = 0 ; // According to RFC 3626 document: assuming message Size will be 25
		int header_len = olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        uint8_t rcvdLinkCodeUnspec = 0;
        memcpy(&rcvdLinkCodeUnspec, buffer+4, sizeof(uint8_t));
        CPPUNIT_ASSERT(rcvdLinkCodeUnspec == sndLinkCodeUnspec);
     }

    void testOLSRHelloSymLink(){
       unsigned char piNum = '3', nodeNum = '1';
       IPAddr origin;
       origin.to32bWord();
       neighbors.push_back(origin);
       uint8_t sndLinkSymLink = 0 ;
       int header_len = olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
       uint8_t rcvdLinkCodeUnspec = 6;
       memcpy(&rcvdLinkCodeUnspec, buffer+4, sizeof(uint8_t));
       CPPUNIT_ASSERT(sndLinkSymLink == rcvdLinkCodeUnspec);
     }

   void testOLSRHelloMidReserved() {

        uint8_t sndReserved2 = 0 ; // According to RFC 3626 document: assuming message Size will be 25
		int header_len = olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        uint8_t rcvdReserved2 = 0;
        memcpy(&rcvdReserved2, buffer+5, sizeof(uint16_t));
        CPPUNIT_ASSERT(rcvdReserved2 == sndReserved2);
	}

    void testOLSRHelloLinkMessageSizeWithoutNeighbor() {

        uint16_t sndMsgSizeWithoutNeighbor = 4 ; // According to RFC 3626 document: assuming message Size will be 25
		int header_len = olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        uint16_t rcvdMsgSizeWithoutNeighbor = 0;
        memcpy(&rcvdMsgSizeWithoutNeighbor, buffer+6, sizeof(uint16_t));
        CPPUNIT_ASSERT(rcvdMsgSizeWithoutNeighbor == sndMsgSizeWithoutNeighbor);
     }

    void testOLSRHelloLinkMessageWithNeighbor(){
        IPAddr origin;
        origin.to32bWord();
        neighbors.push_back(origin);
        uint16_t sndMsgSizeWithNeighbor = 8 ; // According to RFC 3626 document: assuming message Size will be 25
		int header_len = olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
        uint16_t rcvdMsgSizeWithNeighbor = 0;
        memcpy(&rcvdMsgSizeWithNeighbor, buffer+6, sizeof(uint16_t));
        CPPUNIT_ASSERT(rcvdMsgSizeWithNeighbor == sndMsgSizeWithNeighbor);
     }
    void testOLSRHelloInterfaceAddr(){
       neighbors.push_back(IPAddr(3, 1));
       int header_len = olsrh->setHeader(buffer, helloMessageSize, mpr, neighbors);
       IPAddr intrfaceAddr(3, 1);
       uint32_t neighborList = 0;
       memcpy(&neighborList, buffer+8, sizeof(uint32_t));

      CPPUNIT_ASSERT(ntohl(neighborList) == intrfaceAddr.to32bWord());
     }
};

#endif
