#include <stddef.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "helloMsg.h"


short initHellomsg(struct Hello *Hi, struct mac_addr originatorAddress,
				   struct mac_addr * neighborList, int len, char type)
{
	//Initialize static attributes in Header
	Hi->messageType = 0x01; //1 == HELLO
	Hi->VTIME = 0x00;

	int i, j;
	Hi->originatorAddress = originatorAddress;
	Hi->timeToLive = 0x0A; //10
	Hi->hopCount = 0;
	Hi->messageSequenceNum = 0;
	
	//Initialize static attributes in Content
	Hi->RESERVED = 0; //This should be 0, not 1?
	Hi->HTIME = '0'; // DO not use or specify field HTIME?? just set it to a dummy variable
	Hi->willingness = '3'; // 3 == WILL_DEFAULT
	if (type=='S')
		Hi->linkCode = 0x09;
	else
		Hi->linkCode = 0x04;	
	
	Hi->RESERVED2 = '0';
	

	//Dynamic Attributes of Header
	Hi->messageSize = sizeof(char)*8 + sizeof(int)*1 + sizeof(short)*4 + 4*len; // + len(neighborList) * 4
	//Dynamic Attributs of Content
	Hi->linkMsgSize = 8+4*len;
	Hi->neighborList = malloc(4*len);
	for (i = 0; i < len; ++i)
	{
		for (j = 0; j < 4; ++j)
		{
			memcpy(&Hi->neighborList[4*i+j], &neighborList[i].bytes[j], 1);
		}
		
	}

	//memcpy(&Hi->neighborList, neighborList, 4*len);
	return Hi->messageSize;
}

char * serializeHelloMessage(struct Hello * Hi)
{
	char * buffer;
	buffer = malloc(Hi->messageSize);
	int saveSize = Hi->messageSize;
	//HEADER
	buffer[0] = Hi->messageType;
	buffer[1] = Hi->VTIME;
	Hi->messageSize = htons(Hi->messageSize);
	memcpy(&buffer[2], &Hi->messageSize, 2);
	int i;
	for(i = 0; i < 4; i++)
	{
		memcpy(&buffer[4+i], &Hi->originatorAddress.bytes[i], 1);
	}
	buffer[8] = Hi->timeToLive;
	buffer[9] = Hi->hopCount;
	Hi->messageSequenceNum = htons(Hi->messageSequenceNum);
	memcpy(&buffer[10], &Hi->messageSequenceNum, 2);

	//CONTENT
	Hi->RESERVED = htons(Hi->RESERVED);
	memcpy(&buffer[12], &Hi->RESERVED, 2);
	buffer[14] = Hi->HTIME;
	buffer[15] = Hi->willingness;
	buffer[16] = Hi->linkCode;
	buffer[17] = Hi->RESERVED2;
	Hi->linkMsgSize = htons(Hi->linkMsgSize);
	memcpy(&buffer[18], &Hi->linkMsgSize, 2);

	//This is where we add the neighbors to the buffer

	memcpy(&buffer[20], &Hi->neighborList, saveSize-20); //len(neighborList *4)
	// int i;
	// for(i=0; i<saveSize; i++)
	// {
	// 	printf("buffer[%d] : %02X \n", i, (unsigned char)buffer[i]);
	// }
	for (i = 0; i < saveSize - 20; ++i)
	{
		memcpy(&buffer[i+20], &Hi->neighborList[i], 1);
	}
	return buffer;

}
struct Hello * deserializeHelloMessage(char *message, int length)
{
	struct Hello * Hi = malloc(sizeof(struct Hello));

	//Header
	Hi->messageType = message[0];
	Hi->VTIME = message[1];
	Hi->messageSize = length;
	int i;
	for(i = 0; i < 4; i++)
	{
		memcpy(&Hi->originatorAddress.bytes[i], &message[4+i], 1);
	}
	//Hi->originatorAddress = ntohl(Hi->originatorAddress);
	Hi->timeToLive = message[8];
	Hi->hopCount = message[9];
	memcpy(&Hi->messageSequenceNum, &message[10], 2);
	Hi->messageSequenceNum = ntohs(Hi->messageSequenceNum);

	//Content
	memcpy(&Hi->messageSequenceNum, &message[12], 2);
	Hi->RESERVED = ntohs(Hi->RESERVED);
	Hi->HTIME = message[14];
	Hi->willingness = message[15];
	Hi->linkCode = message[16];
	Hi->RESERVED2 = message[17];
	memcpy(&Hi->linkMsgSize, &message[18], 2);
	Hi->linkMsgSize = ntohs(Hi->linkMsgSize);
	Hi->neighborList = malloc(Hi->linkMsgSize-8);
	memcpy(Hi->neighborList, &message[20], Hi->linkMsgSize-8);

	//printf("Leaving deserialize Hello message \n");
	return Hi;
}


void recvAddressList(int timer, struct Hello *recHi, struct mac_addr *asymList,
					 struct mac_addr *symList, struct mac_addr *twoHopList, struct mac_addr senderMAC,
					 struct mac_addr ourMAC, int *sizeAsym, int *sizeSym, int *sizeTwoHop, FILE * log)
{
	struct mac_addr currentMac;
	bool changesMade = false;
	int k = 0;
	if (recHi->linkCode==4) //ASYM LIST 
	{
		printf("Entering Asym List\n");
		bool selfFound = false;
		bool foundSenderInSym;
		foundSenderInSym = false;
		int i;
		for(i=0; i<((recHi->linkMsgSize - 8)/4); i++) 
		//Loop through all of the neighbours
		{
			int j;
			for(j=0; j<4; j++)
			//Each neighbour occupies 4 slots in the char array, one for each group of 2 hex characters
			{
				//We add 'k' so we are working with the 'next' neighbour
				currentMac.bytes[j] = recHi->neighborList[j+k];			
			}
			//compare this neighbour to our address
			if(currentMac.bytes[0] == ourMAC.bytes[0] && currentMac.bytes[1] == ourMAC.bytes[1]
				&& currentMac.bytes[2] == ourMAC.bytes[2] && currentMac.bytes[3] == ourMAC.bytes[3])
			{
				selfFound = true; //We set this to true so we don't add this neighbour to our Asym list
			}
			k+=4;
		}
		//Finished looking for ourself in the asym list.
		if (selfFound == true)
		{
			//We found ourself in this host's asym list. This means they received a hello from us, 
			//but didn't see themselves. We need to make sure they are in our sym list.
			int m;
			for(m=0; m<*sizeSym; m++)
			{
				if(senderMAC.bytes[0] == symList[0].bytes[0] && senderMAC.bytes[1] == symList[1].bytes[1]
					&& senderMAC.bytes[2] == symList[2].bytes[2] && senderMAC.bytes[3] == symList[3].bytes[3])
				{
					foundSenderInSym = true;
				}
			}
			//Do not combine these loops. We need to verify them separately.
			int j;
			for(m=0; m<*sizeAsym; m++)
			{
				if(senderMAC.bytes[0] == asymList[0].bytes[0] && senderMAC.bytes[1] == asymList[1].bytes[1]
					&& senderMAC.bytes[2] == asymList[2].bytes[2] && senderMAC.bytes[3] == asymList[3].bytes[3])
				{
					if(selfFound == true)
					{
						for(j=m; j < *sizeAsym; j++)
						{
							asymList[j] = asymList[j+1];
						}
						(*sizeAsym)--;
						changesMade = true;
						printf("Removed Sender: %02X:%02x:%02x:%02x from Asym Neighbors.\n",
							senderMAC.bytes[0], senderMAC.bytes[1], senderMAC.bytes[2], senderMAC.bytes[3]);
					}
				}
			}
			if(foundSenderInSym == false)
			{
				//Add this sender to our sym list
				senderMAC.expiry = timer+5;
				symList[(*sizeSym)] = senderMAC;
				(*sizeSym)++;
				changesMade = true;
				printf("Added Sender: %02X:%02x:%02x:%02x to Sym Neighbors.\n",
							senderMAC.bytes[0], senderMAC.bytes[1], senderMAC.bytes[2], senderMAC.bytes[3]);
			}
		}
		else 
		{
			int m;
			bool foundSenderInAsym = false;
			for(m=0; m<*sizeAsym; m++)
			{
				if(senderMAC.bytes[0] == asymList[m].bytes[0] && senderMAC.bytes[1] == asymList[m].bytes[1]
					&& senderMAC.bytes[2] == asymList[m].bytes[2] && senderMAC.bytes[3] == asymList[m].bytes[3])
				{
					foundSenderInAsym = true;
				}
			}
			if(foundSenderInAsym == false)
			{
				//Add this sender to our asym list
				senderMAC.expiry = timer+5;
				asymList[(*sizeAsym)] = senderMAC;
				(*sizeAsym)++;
				changesMade = true;
				printf("Added Sender: %02X:%02x:%02x:%02x to Asym Neighbors.\n",
							senderMAC.bytes[0], senderMAC.bytes[1], senderMAC.bytes[2], senderMAC.bytes[3]);
			}
		}
	}
	
	else if (recHi->linkCode==9) //SYM LIST (All neighbours are either sender sym 1-hop or our 2-hops)
	{
		k=0;
		printf("Entering Sym List\n");
		bool foundInAsym = false;
		bool foundInSym = false;
		bool foundSenderInSym = false;
		int i;

		//Should this be -20?
		for(i=0; i<((recHi->linkMsgSize - 8)/4); i++) 
		//Loop through all of the neighbours
		{
			int j;
			for(j=0; j<4; j++)
			//Each neighbour occupies 4 slots in the char array, one for each group of 2 hex characters
			{
				//We add 'k' so we are working with the 'next' neighbour
				currentMac.bytes[j] = recHi->neighborList[j+k];	
			}

			//compare this neighbour to our address
			if(currentMac.bytes[0] == ourMAC.bytes[0] && currentMac.bytes[1] == ourMAC.bytes[1]
				&& currentMac.bytes[2] == ourMAC.bytes[2] && currentMac.bytes[3] == ourMAC.bytes[3])
			{
				//This neighbour in the list is us. We will discard this packet
				//after updating this neighbours status.
				int l;
				for(l=0; l<*sizeSym; l++)
				{
					if(symList[l].bytes[0] == senderMAC.bytes[0] && symList[l].bytes[1] == senderMAC.bytes[1]
						&& symList[l].bytes[2] == senderMAC.bytes[2] && symList[l].bytes[3] == senderMAC.bytes[3])
					{
						foundSenderInSym = true;
					}
				}
				for(l=0; l<*sizeAsym; l++)
				{
					//If the sender is in our Asym list, remove them
					if(asymList[l].bytes[0] == senderMAC.bytes[0] && asymList[l].bytes[1] == senderMAC.bytes[1]
						&& asymList[l].bytes[2] == senderMAC.bytes[2] && asymList[l].bytes[3] == senderMAC.bytes[3])
					{
						for(j=0; j< *sizeAsym; j++)
						{
							asymList[l] = asymList[l+1];
						}
						(*sizeAsym)--;
						changesMade = true;
						printf("Removed Sender: %02X:%02x:%02x:%02x from Asym Neighbors.\n",
							senderMAC.bytes[0], senderMAC.bytes[1], senderMAC.bytes[2], senderMAC.bytes[3]);
					}
				}
				//If the sender is not in our sym list, add them.
				if(foundSenderInSym == false)
				{
					senderMAC.expiry = timer+5;
					symList[(*sizeSym)] = senderMAC;
					(*sizeSym)++;
					foundSenderInSym = true;
					changesMade = true;
					printf("Added Sender: %02X:%02x:%02x:%02x to Sym Neighbors.\n",
							senderMAC.bytes[0], senderMAC.bytes[1], senderMAC.bytes[2], senderMAC.bytes[3]);

				}
			}

			//Check if we have heard from this neighbour before
			int n;
			for(n=0; n<*sizeSym; n++) //If these len() checks dont work, we can use (*sizeAsym) - 1
			{
				if(currentMac.bytes[0] == symList[0].bytes[0] && currentMac.bytes[1] == symList[1].bytes[1]
					&& currentMac.bytes[2] == symList[2].bytes[2] && currentMac.bytes[3] == symList[3].bytes[3])
				{
					foundInSym = true;
				}
			}
			int o;
			for(o=0; o<*sizeAsym; o++)
			{
				if(currentMac.bytes[0] == asymList[0].bytes[0] && currentMac.bytes[1] == asymList[1].bytes[1]
					&& currentMac.bytes[2] == asymList[2].bytes[2] && currentMac.bytes[3] == asymList[3].bytes[3])
				{
					foundInAsym = true;
				}
			}
			if(foundInSym == false && foundInAsym == false)
			{
				//Add neighbour to twohop
				currentMac.expiry = timer+5;
				twoHopList[(*sizeTwoHop)] = currentMac;
				(*sizeTwoHop)++;
				changesMade = true;
				printf("Added Neighbour of Sender: %02X:%02x:%02x:%02x to TwoHop Neighbors.\n",
							currentMac.bytes[0], currentMac.bytes[1], currentMac.bytes[2], currentMac.bytes[3]);
			}
			foundInAsym = false;
			foundInSym = false;
			//else do nothing because the neighbour is within our range (we heard a hello from them before)
			k+=4;
			//Make sure we are grabbing the next neighbours mac
		}
	}
	else
	{
		printf("Invalind linkCode of %d received", recHi->linkCode);
	}
	if(changesMade)
	{
		log = fopen("log.txt", "a");

		fprintf(log, "TIME: %d \n", timer);
		fprintf(log,"ASYM Neighbors:\n");
		int j;
		int k;
		for(j=0; j < *sizeAsym; j++)
		{
			for(k=1; k<5; k++)
			{
				if(k%4 == 0)
					fprintf(log,"%02X", asymList[j].bytes[k-1]);
				else
					fprintf(log,"%02X:", asymList[j].bytes[k-1]);

				if(k%4 == 0)
					fprintf(log,"\n");
			}
		}
		fprintf(log,"\n SYM Neighbors:\n");
		for(j=0; j < *sizeSym; j++)
		{
			for(k=1; k<5; k++)
			{
				if(k%4 == 0)
					fprintf(log,"%02X", symList[j].bytes[k-1]);
				else
					fprintf(log,"%02X:", symList[j].bytes[k-1]);

				if(k%4 == 0)
					fprintf(log,"\n");
			}
		}
		fprintf(log,"\n Two Hop Neighbors:\n");
		for(j=0; j < *sizeTwoHop; j++)
		{
			for(k=1; k<5; k++)
			{
				if(k%4 == 0)
					fprintf(log,"%02X", twoHopList[j].bytes[k-1]);
				else
					fprintf(log,"%02X:", twoHopList[j].bytes[k-1]);

				if(k%4 == 0)
					fprintf(log,"\n");
			}
		}
		fclose(log);
	}
}

void checkExpiry(struct mac_addr *asymList, struct mac_addr *symList, struct mac_addr *twoHopList, 
	int *sizeAsym, int *sizeSym, int *sizeTwoHop, int timer, FILE * log)
{
	int j;
	int k;

	bool changesMade = false;

	for(j=0; j < *sizeAsym; j++)
	{
		if(asymList[j].expiry == timer || asymList[j].expiry < timer)
		{
			for(k=j; k < *sizeAsym-1; k++)
			{
				asymList[k] = asymList[k+1];
			}
			(*sizeAsym)--;
			printf("Removed Asym Neighbour: %02X:%02x:%02x:%02x.\n",
					asymList[j].bytes[0], asymList[j].bytes[1], asymList[j].bytes[2], asymList[j].bytes[3]);
			changesMade = true;
		}
	}
	for(j=0; j < *sizeSym; j++)
	{
		if(symList[j].expiry == timer || symList[j].expiry < timer)
		{
			for(k=j; k < *sizeSym-1; k++)
			{
				symList[k] = symList[k+1];
			}
			(*sizeSym)--;
			printf("Removed Sym Neighbour: %02X:%02x:%02x:%02x.\n",
							symList[j].bytes[0], symList[j].bytes[1], symList[j].bytes[2], symList[j].bytes[3]);
			changesMade = true;

		}
	}
	for(j=0; j < *sizeTwoHop; j++)
	{
		if(twoHopList[j].expiry == timer || twoHopList[j].expiry < timer)
		{
			for(k=j; k < *sizeTwoHop-1; k++)
			{
				twoHopList[k] = twoHopList[k+1];
			}
			(*sizeTwoHop)--;
			printf("Removed TwoHop Neighbour: %02X:%02x:%02x:%02x.\n",
							twoHopList[j].bytes[0], twoHopList[j].bytes[1], twoHopList[j].bytes[2], twoHopList[j].bytes[3]);
			changesMade = true;

		}
	}
	if(changesMade)
	{
		log = fopen("log.txt", "a");

		fprintf(log, "TIME: %d \n", timer);
		fprintf(log,"ASYM Neighbors:\n");
		int j;
		int k;
		for(j=0; j < *sizeAsym; j++)
		{
			for(k=1; k<5; k++)
			{
				if(k%4 == 0)
					fprintf(log,"%02X", asymList[j].bytes[k-1]);
				else
					fprintf(log,"%02X:", asymList[j].bytes[k-1]);

				if(k%4 == 0)
					fprintf(log,"\n");
			}
		}
		fprintf(log,"\n SYM Neighbors:\n");
		for(j=0; j < *sizeSym; j++)
		{
			for(k=1; k<5; k++)
			{
				if(k%4 == 0)
					fprintf(log,"%02X", symList[j].bytes[k-1]);
				else
					fprintf(log,"%02X:", symList[j].bytes[k-1]);

				if(k%4 == 0)
					fprintf(log,"\n");
			}
		}
		fprintf(log,"\n Two Hop Neighbors:\n");
		for(j=0; j < *sizeTwoHop; j++)
		{
			for(k=1; k<5; k++)
			{
				if(k%4 == 0)
					fprintf(log,"%02X", twoHopList[j].bytes[k-1]);
				else
					fprintf(log,"%02X:", twoHopList[j].bytes[k-1]);

				if(k%4 == 0)
					fprintf(log,"\n");
			}
		}
		fclose(log);
	}
}