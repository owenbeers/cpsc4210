#ifndef __TESTIPUDPPACKETPI1_H
#define __TESTIPUDPPACKETPI1_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <iostream>



#include "IPUDPPacketFactory.h"
#include "IPAddr.h"


/// Test fixture for ProgressiveRateStrategy
class TestIPUDPPacketPi1 : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(TestIPUDPPacketPi1);
	CPPUNIT_TEST(sampleTestIPUDPDataTooLarge);

	CPPUNIT_TEST(checkIPUdpVersion);
	CPPUNIT_TEST(checkIPUdpIhl);
	CPPUNIT_TEST(checkIPUdpTypeOfService);
	CPPUNIT_TEST(checkIPUdpTotalLength);
	CPPUNIT_TEST(checkIPUdpIdentification);
	CPPUNIT_TEST(checkIPUdpFlagsAndFragmentOffset);
	CPPUNIT_TEST(checkIPUdpTTL);
	CPPUNIT_TEST(checkIPUdpProtocol);
	CPPUNIT_TEST(checkIPUdpHeaderChecksumIP);
	CPPUNIT_TEST(checkIPUdpSourceAddress);
	CPPUNIT_TEST(checkIPUdpDestAddress);
	CPPUNIT_TEST(checkIPUdpSourcePort);
	CPPUNIT_TEST(checkIPUdpDestPort);
	CPPUNIT_TEST(checkIPUdpLengthUDP);
	CPPUNIT_TEST(checkIPUdpChecksumUDP);

	CPPUNIT_TEST_SUITE_END();
private:
	// we make the factory static so that we can initialize it in the
	// .cc file to the concrete factory that returns the concrete object
	// with our implementation of setHeader()
	static std::shared_ptr<IPUDPPacketFactory> factory;

private:
	// the packet object being tested
	std::shared_ptr<IPUDPPacket> ipudp;
	// the buffer used for testing
	unsigned char * buffer;
	// the buffer length
	const int buflen = 1000;


public:

    uint8_t ver_ihl = 45;
    uint8_t typeOfSer = 0;
    uint16_t totalLen = 38;
    uint16_t identification = 0;
    uint16_t flag_offset = 0;
    uint8_t ttl = 1;
    uint8_t protocol = 138;
    uint16_t checksumIp = 0;
    uint32_t srcAddr;
    uint32_t dstAddr;
    uint16_t sourcePort = 698;
    uint16_t destPort = 698;
    uint16_t length = 38;
    uint16_t checksumUdp = 0;


	void setUp()  {
		if (factory)
			ipudp = factory->createIPUDPPacket();
		else {
	    std::cerr << "TestIPUDPSample test fixture not properly initialized!\n";
			exit(EXIT_FAILURE);
		}
		buffer = new unsigned char [buflen];
	};

	void tearDown()  {
		delete[] buffer;
		// we don't need to delete ipudp since it is a smart pointer
	};

	// *************** test cases ************
	void sampleTestIPUDPDataTooLarge() {
		IPAddr src,dst;
		int header_len = ipudp->setHeader(buffer, 28+10, src, dst, 1, 11);
		CPPUNIT_ASSERT(header_len == -1);
	}

	void checkIPUdpVersion() {
	    IPAddr src, dst;
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&ver_ihl,buffer,sizeof(ver_ihl));
		CPPUNIT_ASSERT((ver_ihl/10) == 4);

	}

	void checkIPUdpIhl() {
	    IPAddr src, dst;
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
        memcpy(&ver_ihl,buffer,sizeof(ver_ihl));
		CPPUNIT_ASSERT((ver_ihl%10) == 5);
	}

	void checkIPUdpTypeOfService() {

		IPAddr src, dst;
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&typeOfSer, buffer+1,sizeof(typeOfSer));
		CPPUNIT_ASSERT(typeOfSer == 0);
	}

	void checkIPUdpTotalLength() {

		IPAddr src, dst;
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&totalLen, buffer+2,sizeof(totalLen));
		CPPUNIT_ASSERT(totalLen == 38);
	}

	void checkIPUdpIdentification() {

		IPAddr src, dst;
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&identification, buffer+4,sizeof(identification));
		CPPUNIT_ASSERT(identification == 0);
	}

	void checkIPUdpFlagsAndFragmentOffset() {

		IPAddr src, dst;
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&flag_offset, buffer+6,sizeof(flag_offset));
		CPPUNIT_ASSERT(flag_offset == 0);
	}

	void checkIPUdpTTL() {

		IPAddr src, dst;
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&ttl, buffer+8,sizeof(ttl));
		CPPUNIT_ASSERT(ttl == 1);
	}

	void checkIPUdpProtocol() {

		IPAddr src, dst;
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&protocol, buffer+9,sizeof(protocol));
		CPPUNIT_ASSERT(protocol == 138);
	}

	void checkIPUdpHeaderChecksumIP() {

		IPAddr src, dst;
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&checksumIp, buffer+10,sizeof(checksumIp));
		CPPUNIT_ASSERT(checksumIp == 0);
	}

	void checkIPUdpSourceAddress() {

		IPAddr src(1,1), dst;
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&srcAddr, buffer+12,sizeof(srcAddr));
		CPPUNIT_ASSERT(srcAddr == src.to32bWord());
	}

	void checkIPUdpDestAddress() {

		IPAddr src, dst(4,2);
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&dstAddr, buffer+16,sizeof(dstAddr));
		CPPUNIT_ASSERT(dstAddr == dst.to32bWord());
	}

	void checkIPUdpSourcePort() {

		IPAddr src, dst;
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&sourcePort, buffer+20,sizeof(sourcePort));
		CPPUNIT_ASSERT(sourcePort == 698);
	}

	void checkIPUdpDestPort() {

		IPAddr src, dst;
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&destPort, buffer+22,sizeof(destPort));
		CPPUNIT_ASSERT(destPort == 698);
	}

	void checkIPUdpLengthUDP() {

		IPAddr src, dst;
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&length, buffer+24,sizeof(length));
		CPPUNIT_ASSERT(length == 38);
	}

	void checkIPUdpChecksumUDP() {

		IPAddr src, dst;
		ipudp->setHeader(buffer, 28+10, src, dst, 1, 10);
		memcpy(&checksumUdp, buffer+26,sizeof(checksumUdp));
		CPPUNIT_ASSERT(checksumUdp == 0);
	}



};

#endif
