
#ifndef __TESTIPUDPPACKETPI3_H
#define __TESTIPUDPPACKETPI3_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <bitset>
#include "IPUDPPacketFactory.h"
#include "IPAddr.h"
//#include "IPUDPPacketSample.h"
#include "IPUDPPacket.h"

using namespace std;


/// Test fixture for ProgressiveRateStrategy
class TestIPUDPPacketPi3 : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(TestIPUDPPacketPi3);
	CPPUNIT_TEST(sampleTestIPUDPPacketTooLarge);


    CPPUNIT_TEST(testIPUDPPktVersion);
	CPPUNIT_TEST(testIPUDPPktIhl);
	CPPUNIT_TEST(testIPUDPPktTypeOfService);
	CPPUNIT_TEST(testIPUDPPktTotalLength);
    CPPUNIT_TEST(testIPUDPPktIdentification);

	CPPUNIT_TEST(testIPUDPPktFlagsAndFragmentOffset);
	//CPPUNIT_TEST(testIPUDPPktFragmentOffset);
	CPPUNIT_TEST(testIPUDPPktTimeToLive);
    CPPUNIT_TEST(testIPUDPPktProtocol);

	CPPUNIT_TEST(testIPUDPPktHeaderChecksum);
	CPPUNIT_TEST(testIPUDPPktSourceAddr);
	CPPUNIT_TEST(testIPUDPPktDestinationAddr);
    CPPUNIT_TEST(testIPUDPPktSourcePort);

	CPPUNIT_TEST(testIPUDPPktDestinationPort);
	CPPUNIT_TEST(testIPUDPPktDataLength);
    CPPUNIT_TEST(testIPUDPPktUDPChecksum);

	CPPUNIT_TEST_SUITE_END();
private:
	// we make the factory static so that we can initialize it in the
	// .cc file to the concrete factory that returns the concrete object
	// with our implementation of setHeader()
	static std::shared_ptr<IPUDPPacketFactory> factory;

private:
	// the packet object being tested
	std::shared_ptr<IPUDPPacket> ipudp;
	// the buffer used for testing
	unsigned char * buffer;
	// the buffer length
	const int buflen = 1000;
    uint16_t ipUdpPacketLength = 45;
    uint8_t timeToLive = 1;
    uint16_t dataLen = 20;

public:

	void setUp()  {
		if (factory){
			ipudp = factory->createIPUDPPacket();

		}
		else {
	    std::cerr << "TestIPUDPSample test fixture not properly initialized!\n";
			exit(EXIT_FAILURE);
		}
		buffer = new unsigned char [buflen];
	};

	void tearDown()  {
		delete[] buffer;
		// we don't need to delete ipudp since it is a smart pointer
	};

	// *************** test cases ************
	void sampleTestIPUDPPacketTooLarge() {
        IPAddr origin, destination;
        int ipUdpPacketLength = 100;
		int header_len = ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
		CPPUNIT_ASSERT(header_len == -1);
	}

   void testIPUDPPktVersion() {

        IPAddr origin, destination;
        uint8_t sndVersion = 4;
		int header_len = ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint8_t rcvdVersion = 0;
        memcpy(&rcvdVersion, buffer, sizeof(uint8_t));
        rcvdVersion = rcvdVersion/10;
		CPPUNIT_ASSERT(rcvdVersion == sndVersion);
     }

    void testIPUDPPktIhl(){

        IPAddr origin, destination;
        uint8_t sndIhl = 5;
		int header_len = ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint8_t rcvdIhl = 0;
        memcpy(&rcvdIhl, buffer, sizeof(uint8_t));
        rcvdIhl = rcvdIhl%10;
		CPPUNIT_ASSERT(rcvdIhl == sndIhl);
     }

    void testIPUDPPktTypeOfService() {

        IPAddr origin, destination;
        uint8_t sndTypeOfService = 0;
		int header_len = ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint8_t rcvdTypeOfService = 0;
        memcpy(&rcvdTypeOfService, buffer+1, sizeof(uint8_t));
		CPPUNIT_ASSERT(rcvdTypeOfService == sndTypeOfService);
	}

	  void testIPUDPPktTotalLength() {

        IPAddr origin, destination;
        uint16_t sndTotalLength = 50;
		int header_len = ipudp->setHeader(buffer, sndTotalLength, origin, destination, timeToLive, dataLen);
        uint16_t rcvdTotalLength = 0;
        memcpy(&rcvdTotalLength, buffer+2, sizeof(uint16_t));
		CPPUNIT_ASSERT(rcvdTotalLength == sndTotalLength);
	}

    void testIPUDPPktIdentification() {

        IPAddr origin, destination;
        uint16_t sndIdentification = 0;
		int header_len = ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint16_t rcvdIdentification = 0;
        memcpy(&rcvdIdentification, buffer+4, sizeof(uint16_t));
		CPPUNIT_ASSERT(rcvdIdentification == sndIdentification);
     }

    void testIPUDPPktFlagsAndFragmentOffset(){
        IPAddr origin, destination;
        uint16_t sndFlags = 0;
		int header_len = ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint16_t rcvdFlags = 0;
        memcpy(&rcvdFlags, buffer+6, sizeof(uint16_t));
		CPPUNIT_ASSERT(rcvdFlags == sndFlags);
     }


    void testIPUDPPktTimeToLive() {

        IPAddr origin, destination;
        uint8_t sndTimeToLive = 1;
		int header_len = ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint8_t rcvdTimeToLive = 0;
        memcpy(&rcvdTimeToLive, buffer+8, sizeof(uint8_t));
		CPPUNIT_ASSERT(rcvdTimeToLive == sndTimeToLive);
     }

    void testIPUDPPktProtocol(){

        IPAddr origin, destination;
        uint8_t sndProtocol = 1;
		int header_len = ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint8_t rcvdProtocol = 0;
        memcpy(&rcvdProtocol, buffer+9, sizeof(uint8_t));
		CPPUNIT_ASSERT(rcvdProtocol == sndProtocol);
     }
       void testIPUDPPktHeaderChecksum() {

        IPAddr origin, destination;
        uint16_t sndHeaderChecksum = 0;
		int header_len = ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint16_t rcvdHeaderChecksum = 0;
        memcpy(&rcvdHeaderChecksum, buffer+10, sizeof(uint16_t));
		CPPUNIT_ASSERT(rcvdHeaderChecksum == sndHeaderChecksum);
     }

    void testIPUDPPktSourceAddr(){

      IPAddr origin(3, 1), destination;
      int header_len = ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
      uint32_t rcvdSrcAddr = 0;
      memcpy(&rcvdSrcAddr, buffer+12, sizeof(uint32_t));
      rcvdSrcAddr = ntohl(rcvdSrcAddr);
      CPPUNIT_ASSERT(rcvdSrcAddr == origin.to32bWord());
     }

    void testIPUDPPktDestinationAddr() {

      IPAddr origin, destination(3,1);
      int header_len = ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
      uint32_t rcvdDstAddr = 0;
      memcpy(&rcvdDstAddr, buffer+16, sizeof(uint32_t));
      rcvdDstAddr = ntohl(rcvdDstAddr);
      CPPUNIT_ASSERT(rcvdDstAddr == destination.to32bWord());
	}

	  void testIPUDPPktSourcePort() {

        IPAddr origin, destination;
        uint16_t sndSourcePort = 698;
		int header_len = ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint16_t rcvdSourcePort = 0;
        memcpy(&rcvdSourcePort, buffer+20, sizeof(uint16_t));
		CPPUNIT_ASSERT(rcvdSourcePort == sndSourcePort);
	}

    void testIPUDPPktDestinationPort() {

        IPAddr origin, destination;
        uint16_t sndDstPort = 698;
		int header_len = ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint16_t rcvdDstPort = 0;
        memcpy(&rcvdDstPort, buffer+22, sizeof(uint16_t));
		CPPUNIT_ASSERT(rcvdDstPort == sndDstPort);
     }

    void testIPUDPPktDataLength(){
        IPAddr origin, destination;
        uint16_t sndDataLength = 15;
		int header_len = ipudp->setHeader(buffer, ipUdpPacketLength, origin, destination, timeToLive, dataLen);
        uint16_t rcvdDataLength = 0;
        memcpy(&rcvdDataLength, buffer+24, sizeof(uint16_t));
		CPPUNIT_ASSERT(rcvdDataLength == sndDataLength);
     }


    void testIPUDPPktUDPChecksum() {

        IPAddr origin;
        uint16_t sndUdpCheckSum = 0;
		int message_type = olsrp->setHeader(buffer, packHeaderLength, messageType, origin, timeToLive, hopCount, packetSeqNum, messageSeqNum, messageSize);
		uint16_t rcvdUdpCheckSum = 0;
        memcpy(&rcvdUdpCheckSum, buffer+26, sizeof(uint16_t));
        CPPUNIT_ASSERT(rcvdUdpCheckSum == sndUdpCheckSum);
     }

};

#endif

