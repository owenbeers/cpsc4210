// *** CPSC 4210/5210 Assignment 3 code
// ***** IPUDPPacket interface: creates the combined header of IP/UDP packed

#ifndef __OLSRPACKET_H
#define __OLSRPACKET_H

#include "IPAddr.h"

// interface class; provide a concrete class implementing the member function.
// Note this is an instance of the Adapter design pattern: you will
// adapt your existing code that creates packet headers to the
// provided interface. 
class OLSRPacket {
public:
	// assigns values according to the OLSR packet header to the
	// buffer buf.
	// buflen is the size of the buffer in bytes
	// msg_type is the type of the message (1 = Hello)
	// origin is the IP address of the originator node
	// TTL is the time to live value
	// hop_count is th ehop count value
	// psn is the packet sequence number
	// msglen is the size of the message portion following the OLSR packet
	// header, excluding the header
	// RETURNS
	// the length of the header in bytes, or -1 if the buffer is not big
	// enough to store the data and the headers
	virtual int setHeader(unsigned char * buf, int buflen, int msg_type, IPAddr origin,
						  int TTL, int hop_count,
						  uint16_t psn, uint16_t msn, int msglen)=0;

	virtual ~OLSRPacket() {}
};

#endif
