// *** CPSC 4210/5210 Assignment 3 code
// ***** IPUDPPacket interface: creates the combined header of IP/UDP packed

#ifndef __IPUDPPACKETSAMPLE_H
#define __IPUDPPACKETSAMPLE_H

#include <cstring>
#include "IPUDPPacket.h"
#include <iostream>

class IPUDPPacketSample : public IPUDPPacket {
public:
	// Can provide any constructors needed; the sample relies on the
	// default constructor supplied by the compiler

	// concrete implementation for the header routine
	int setHeader(unsigned char * buf, int buflen, IPAddr src,
								IPAddr dest, int TTL, int datalen) {
		// we don't do much, just fill the data length part of the header,
		// as demo.

		// first check if the buffer is big enough
		if (buflen < datalen+28)
			return -1;

		//Setup Version and IHL
		unsigned char Version, IHL, VIHL;
		Version = 4 << 4;
		IHL = 5;
		VIHL = Version | IHL;

		//Find Checksum
		int checkSumVal;
		checkSumVal = (int)(Version >> 4);
		checkSumVal += (int)IHL + 0 + (28+datalen) + 1 + 0 + TTL;
		
		std::memset(buf, 0, buflen);  // we clear the buffer first!
		*(buf) 								  		= VIHL;  // Version IHL
		*(buf+1)									= 1; //ToS
		*(reinterpret_cast<uint16_t*>(buf+2)) 	 	= 28+datalen;  // packet
		*(reinterpret_cast<uint16_t*>(buf+4))  	 	= 1; // Hello message Identification
		*(buf+6)  									= 0; // Flags and Fragment Offset
		*(reinterpret_cast<uint8_t*>(buf+8))		= TTL; // TTL
		*(buf+9)									= 138 | 0x00; // Protocol
		//Checksum GARBAGE
		*(buf+10) 								 	= checkSumVal;
		*(reinterpret_cast<uint32_t*>(buf+12))  	= src.to32bWord(); // source IP
		*(reinterpret_cast<uint32_t*>(buf+16))  	= dest.to32bWord(); // dest IP
		*(reinterpret_cast<uint32_t*>(buf+20))  	= 698; // Src Port
		*(reinterpret_cast<uint32_t*>(buf+22))  	= 698; // Dst Port
		//We need to implement this function later.
		*(reinterpret_cast<uint32_t*>(buf+24))  	= 1000; // Length
		*(reinterpret_cast<uint32_t*>(buf+26))  	= 0; // Checksum

																										// length field
		return 28;  // the size of the header in bytes
	}
};

#endif
