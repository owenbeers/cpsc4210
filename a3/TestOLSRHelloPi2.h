// *** CPSC 4210/5210 Assignment 3 code
// ***** CPPUnit fixture class sample for creating headers for OLSRHello messages ***

#ifndef __TESTOLSRHELLOPI2_H
#define __TESTOLSRHELLOPI2_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <iostream>
#include <vector>

#include "OLSRHelloFactory.h"
#include "IPAddr.h"


/// Test fixture for ProgressiveRateStrategy
class TestOLSRHelloPi2 : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(TestOLSRHelloPi2);
	CPPUNIT_TEST(testOLSRHelloBufferTooSmall);
	CPPUNIT_TEST(testOLSRHelloDataAcceptable);
	CPPUNIT_TEST(testOLSRHelloNeighbourAddsSize);
	CPPUNIT_TEST(testOLSRHelloNeighbourCorrectSize);
	CPPUNIT_TEST(testOLSRHelloReserved1);
	CPPUNIT_TEST(testOLSRHelloHtime);
	CPPUNIT_TEST(testOLSRHelloWillingness);
	CPPUNIT_TEST(testOLSRHelloLinkCodeUNSPECNOT);
	CPPUNIT_TEST(testOLSRHelloLinkCodeSYMSYM);
	CPPUNIT_TEST(testOLSRHelloReserved2);
	CPPUNIT_TEST(testOLSRHelloLinkMessageSizeNeighbourless);
	CPPUNIT_TEST(testOLSRHelloLinkMessageSizeNeighbour);
	CPPUNIT_TEST_SUITE_END();
private:
	// we make the factory static so that we can initialize it in the
	// .cc file to the concrete factory that returns the concrete object
	// with our implementation of setHeader()
	static std::shared_ptr<OLSRHelloFactory> factory;

private:
	// the packet object being tested
	std::shared_ptr<OLSRHello> hello;
	// the buffer used for testing
	unsigned char * buffer;
	// the buffer length
	const int buflen = 1000;

	std::vector<IPAddr> mpr, nbours;
	int offset = 44;
	//Size of all 3 headers is 52 bytes, so this is the minimum size to insert a hello header
	//offset by 44 bytes (the combination of the other two headers)


public:
	void setUp()  {
		if (factory)
			hello = factory->createOLSRHello();
		else {
	    std::cerr << "TestOLSRHelloPi2 test fixture not properly initialized!\n";
			exit(EXIT_FAILURE);
		}
		buffer = new unsigned char [buflen];


	};
	
	void tearDown()  {
		delete[] buffer;
	};

	// *************** test cases ************
	void testOLSRHelloBufferTooSmall() {
		int header_len = hello->setHeader(buffer, 7, mpr, nbours);

		CPPUNIT_ASSERT(header_len == -1);
	}
	
	void testOLSRHelloDataAcceptable() {
		int header_len = hello->setHeader(buffer, 8, mpr, nbours);

		CPPUNIT_ASSERT(header_len == 8);
	}

	void testOLSRHelloNeighbourAddsSize() {
		IPAddr nbour1;
		nbour1.to32bWord();
		nbours.push_back(nbour1);
		int header_len = hello->setHeader(buffer, 8, mpr, nbours);

		CPPUNIT_ASSERT(header_len == -1);
	}
	void testOLSRHelloNeighbourCorrectSize() {
		IPAddr nbour1;
		nbour1.to32bWord();
		nbours.push_back(nbour1);
		int header_len = hello->setHeader(buffer, 12, mpr, nbours);

		CPPUNIT_ASSERT(header_len == 12);
	}
	void testOLSRHelloReserved1() {
		int header_len = hello->setHeader(buffer, buflen, mpr, nbours);
		uint16_t reserved;
		uint16_t expected = 0; 
		std::memcpy(&reserved, buffer, sizeof(uint16_t));

		CPPUNIT_ASSERT(reserved == expected);
	}
	void testOLSRHelloHtime() {
		int header_len = hello->setHeader(buffer, buflen, mpr, nbours);
		uint8_t htime;
		uint8_t expected = 0;
		std::memcpy(&htime, buffer+2, sizeof(uint8_t));

		CPPUNIT_ASSERT(htime == expected);
	}
	void testOLSRHelloWillingness() {
		int header_len = hello->setHeader(buffer, buflen, mpr, nbours);
		uint8_t willingness;
		uint8_t expected = 3;
		std::memcpy(&willingness, buffer+3, sizeof(uint8_t));

		CPPUNIT_ASSERT(willingness == expected);
	}
	void testOLSRHelloLinkCodeUNSPECNOT() {
		int header_len = hello->setHeader(buffer, buflen, mpr, nbours);
		uint8_t link;
		uint8_t expected = 0;
		std::memcpy(&link, buffer+4, sizeof(uint8_t));

		CPPUNIT_ASSERT(link == expected);
	}
	void testOLSRHelloLinkCodeSYMSYM() {
		IPAddr nbour1;
		nbour1.to32bWord();
		nbours.push_back(nbour1);
		int header_len = hello->setHeader(buffer, buflen, mpr, nbours);
		uint8_t link;
		uint8_t expected = 6;

		std::memcpy(&link, buffer+4, sizeof(uint8_t));
		CPPUNIT_ASSERT(link == expected);
	}
	void testOLSRHelloReserved2() {
		uint8_t reserved;
		uint8_t expected = 0;
		int header_len = hello->setHeader(buffer, buflen, mpr, nbours);
		std::memcpy(&reserved, buffer+5, sizeof(uint8_t));

		CPPUNIT_ASSERT(reserved == expected);
	}
	void testOLSRHelloLinkMessageSizeNeighbourless() {
		int header_len = hello->setHeader(buffer, buflen, mpr, nbours);
		uint16_t msgsize;
		uint16_t expected = 4;
		std::memcpy(&msgsize, buffer+6, sizeof(uint16_t));
		
		CPPUNIT_ASSERT(msgsize == expected);
	}
	void testOLSRHelloLinkMessageSizeNeighbour() {
		IPAddr nbour1;
		nbour1.to32bWord();
		nbours.push_back(nbour1);
		int header_len = hello->setHeader(buffer, buflen, mpr, nbours);
		uint16_t msgsize;
		uint16_t expected = 8;
		std::memcpy(&msgsize, buffer+6, sizeof(uint16_t));

		CPPUNIT_ASSERT(msgsize == expected);
	}
	void testOLSRHelloNeighbourListToString() {
		IPAddr nbour1;
		nbour1.to32bWord();
		nbours.push_back(nbour1.to32bWord());
		int header_len = hello->setHeader(buffer, buflen, mpr, nbours);

		uint32_t neigh1;
		memcpy(&neigh1, buffer+8, sizeof(uint32_t));

		CPPUNIT_ASSERT(IPAddr(neigh1).toString() == "169.254.255.255");
	}
};

#endif
