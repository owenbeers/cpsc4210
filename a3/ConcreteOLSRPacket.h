//ConcreteOLSRPacket.h

// *** CPSC 4210/5210 Assignment 3 code
// ***** OLSRPacket interface: creates the combined header of IP/UDP packed

#ifndef __CONCRETEOLSRPACKET_H
#define __CONCRETEOLSRPACKET_H

#include <cstring>
#include "OLSRPacket.h"
#include <netinet/in.h>
#include <iostream>
#include <iomanip>

#include <arpa/inet.h>

using namespace std;
class ConcreteOLSRPacket : public OLSRPacket {
public:

	int setHeader(unsigned char * buf, int buflen, int msg_type, IPAddr origin,
				  int TTL, int hop_count, uint16_t psn, uint16_t msn, int msglen) {

		int offset = 0; // length of the IPUDP Header ** Correction, we dont need to worry about this
						// Just setting it to 0 for now
		int headerLen = 16; // supposed length of the OLSR packet header
		
		// first check if the buffer is big enough
		if (buflen < msglen+headerLen+offset)
			return -1;
		
		// For all of the integers that are being put into one byte fields
		// Assert that their value is not negative and less than 256

		// getting a valid msg_type
		if (msg_type > 255 || msg_type < 0)
			return -1;

		//getting a valid time to live
		if (TTL > 255 || TTL < 0)
			return -1;

		//getting a valid value for hop count
		if (hop_count > 255 || hop_count < 0)
			return -1;

		std::memset(buf+offset, 0, buflen-offset);  // buffer should be cleared already? not necessary?
		// set the packet length field. Should be equal to 16+msglen
		*(reinterpret_cast<uint16_t*>(buf + offset)) = ntohs(headerLen+msglen);
		// set the packet sequence number to the passed in parameter psn (2 bytes) 
		*(reinterpret_cast<uint16_t*>(buf+offset+2)) = ntohs(psn);
		// set packet msg_type to passed in parameter msg_type (integer is 4 bytes, just take last byte?)
		// std::printf("%d\n", msg_type);
		// int netOrdInt = msg_type; //htonl(msg_type);

		//Grab last byte of integer?? Asserted that anything above 255 is garbage
		*(buf+offset+4) = (msg_type & 0xFF); // = 0x01
		*(buf+offset+5) = 0x06; // VTIME
		*(reinterpret_cast<uint16_t*>(buf+offset+6)) = ntohs(msglen);
		*(reinterpret_cast<uint32_t*>(buf+offset+8)) = origin.to32bWord();
		*(buf+offset+12) = TTL & 0xFF; //grab last byte of integer
		*(buf+offset+13) = hop_count & 0xFF; //grab last byte of integer
		*(reinterpret_cast<uint16_t*>(buf+offset+14)) = ntohs(msn);
																										
		return 16;  // the size of the header in bytes
	}
};

#endif
