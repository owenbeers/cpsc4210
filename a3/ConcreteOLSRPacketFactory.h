//ConcreteOLSRPacketFactory.h
// *** CPSC 4210/5210 Assignment 3 code
// *** Provides a concrete OLSRPacketFactory sample class that creates OLSRPacket objects

#ifndef __CONCRETEOLSRPacketFACTORY_H
#define __CONCRETEOLSRPacketFACTORY_H

#include <memory>
#include "OLSRPacketFactory.h"
#include "ConcreteOLSRPacket.h"

class ConcreteOLSRPacketFactory : public OLSRPacketFactory {
public:
	std::shared_ptr<OLSRPacket> createOLSRPacket() {
		return std::make_shared<ConcreteOLSRPacket>();
	}
};

#endif