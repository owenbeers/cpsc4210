// Cody Barnson
// Group Pi4

#ifndef __TESTOLSRHELLOPI4_H
#define __TESTOLSRHELLOPI4_H

#include <cppunit/TextTestRunner.h>
#include <cppunit/TextOutputter.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestListener.h>
#include <cppunit/extensions/TestSuiteBuilderContext.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/ui/text/TextTestRunner.h>

#include <cstdlib>
#include <iostream>
#include <vector>
#include <memory>

#include <arpa/inet.h>

/* #include <IPUDPPacket.h> */
/* #include <IPUDPPacketFactory.h> */
#include "IPAddr.h"
#include "OLSRPacket.h"
#include "OLSRPacketFactory.h"
#include "OLSRHello.h"
#include "OLSRHelloFactory.h"

/// Test fixture for ProgressiveRateStrategy
class TestOLSRHelloPi4 : public CppUnit::TestFixture {
   CPPUNIT_TEST_SUITE(TestOLSRHelloPi4);

   CPPUNIT_TEST(testOLSRHelloDataTooLarge);
   CPPUNIT_TEST(testOLSRHelloFieldFirstReservedIsZero);
   CPPUNIT_TEST(testOLSRHelloFieldHtimeIsZero);
   CPPUNIT_TEST(testOLSRHelloFieldWillingnessWillDefault);
   CPPUNIT_TEST(testOLSRHelloFieldLinkCodeNonEmptyNeighborList);
   CPPUNIT_TEST(testOLSRHelloFieldLinkCodeEmptyNeighborList);
   CPPUNIT_TEST(testOLSRHelloFieldSecondReservedIsZero);
   CPPUNIT_TEST(testOLSRHelloFieldLinkMessageSizeEmptyLists);
   CPPUNIT_TEST(testOLSRHelloFieldLinkMessageSizeNonEmptyLists);
   CPPUNIT_TEST(testOLSRHelloNeighInterfaceAddressIsCorrect);

   CPPUNIT_TEST_SUITE_END();
  private:
   // we make the factory static so that we can initialize it in the
   // .cc file to the concrete factory that returns the concrete object
   // with our implementation of setHeader()
   static std::shared_ptr<OLSRHelloFactory> helloFactory;

  private:
   // the packet object being tested
   std::shared_ptr<OLSRHello> hello;
   // the buffer used for testing
   unsigned char * buffer;
   // the buffer length
   const int buflen = 1000;
   std::vector<IPAddr> v_mpr;
  std::vector<IPAddr> v_nbours;
	

  public:
   void setUp()  {
      if (helloFactory)
	 hello = helloFactory->createOLSRHello();
      else {
	 std::cerr << "TestOLSRPacketPi4 test fixture not properly initialized!\n";
	 exit(EXIT_FAILURE);
      }
      buffer = new unsigned char [buflen];
      
   };
	
   void tearDown()  {
      delete[] buffer;
      // we don't need to delete ipudp since it is a smart pointer
      v_mpr.clear();
      v_nbours.clear();
   };

   // *************** test cases ************
   void testOLSRHelloDataTooLarge() {
      std::vector<IPAddr> v_mpr;
      v_mpr.push_back(IPAddr(1, 3));
      int header_len = hello->setHeader(buffer, 8, v_mpr, v_nbours);
      CPPUNIT_ASSERT(header_len == -1);
   }

   void testOLSRHelloFieldFirstReservedIsZero() {
      int header_len = hello->setHeader(buffer, 8, v_mpr, v_nbours);
      uint16_t reserved = ~(0);
      memcpy(&reserved, buffer, sizeof(uint16_t));
      CPPUNIT_ASSERT(reserved == 0);
   }

   void testOLSRHelloFieldHtimeIsZero() {
      int header_len = hello->setHeader(buffer, 8, v_mpr, v_nbours);
      uint8_t htime = ~(0);
      memcpy(&htime, buffer+2, sizeof(uint8_t));
      CPPUNIT_ASSERT(htime == 0);
   }

   void testOLSRHelloFieldWillingnessWillDefault() {
      uint8_t will_default = 3;
      int header_len = hello->setHeader(buffer, 8, v_mpr, v_nbours);
      uint8_t wd = 0;
      memcpy(&wd, buffer+3, sizeof(uint8_t));
      CPPUNIT_ASSERT(wd == will_default);
   }

   // for all links, we consider them as symmetric
   void testOLSRHelloFieldLinkCodeNonEmptyNeighborList() {
      uint8_t link_code_sym_link = 0x06; // SYM_NEIGH and SYM_LINK
      v_nbours.push_back(IPAddr(1, 3));
      int header_len = hello->setHeader(buffer, buflen, v_mpr, v_nbours);
      uint8_t lc = 0;
      memcpy(&lc, buffer+4, sizeof(uint8_t));
      CPPUNIT_ASSERT(lc == link_code_sym_link);
   }

   // for empty neighbor list, link code should be UNSPEC_LINK
   void testOLSRHelloFieldLinkCodeEmptyNeighborList() {
      uint8_t link_code_unspec_link = 0x00;
      int header_len = hello->setHeader(buffer, 8, v_mpr, v_nbours);
      uint8_t lc = ~(0);
      memcpy(&lc, buffer+4, sizeof(uint8_t));
      CPPUNIT_ASSERT(lc == link_code_unspec_link);
   }

   void testOLSRHelloFieldSecondReservedIsZero() {
      int header_len = hello->setHeader(buffer, 8, v_mpr, v_nbours);
      uint8_t reserved = ~(0);
      memcpy(&reserved, buffer+5, sizeof(uint8_t));
      CPPUNIT_ASSERT(reserved == 0);
   }

   void testOLSRHelloFieldLinkMessageSizeEmptyLists() {
      int header_len = hello->setHeader(buffer, 8, v_mpr, v_nbours);
      uint16_t link_msg_size = 0;
      memcpy(&link_msg_size, buffer+6, sizeof(uint16_t));
      link_msg_size = ntohs(link_msg_size);
      CPPUNIT_ASSERT(link_msg_size == 4);
   }

   void testOLSRHelloFieldLinkMessageSizeNonEmptyLists() {
      v_nbours.push_back(IPAddr(1, 3));
      int header_len = hello->setHeader(buffer, buflen, v_mpr, v_nbours);
      uint16_t link_msg_size = 0;
      memcpy(&link_msg_size, buffer+6, sizeof(uint16_t));
      link_msg_size = ntohs(link_msg_size);
      CPPUNIT_ASSERT(link_msg_size == 4 + (4 * v_nbours.size()));
   }

   void testOLSRHelloNeighInterfaceAddressIsCorrect() {
      v_nbours.push_back(IPAddr(1, 3));
      int header_len = hello->setHeader(buffer, buflen, v_mpr, v_nbours);
      IPAddr ip_addr(1, 3);
      uint32_t addr = 0;
      memcpy(&addr, buffer+8, sizeof(uint32_t));
      addr = ntohl(addr);
      CPPUNIT_ASSERT(addr == ip_addr.to32bWord());
   }
   
};

#endif
