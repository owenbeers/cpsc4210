// *** CPSC 4210/5210 Assignment 3 code
// ***** IPUDPPacket interface: creates the combined header of IP/UDP packed

#ifndef __CONCRETEOLSRHELLO_H
#define __CONCRETEOLSRHELLO_H

#include <cstring>
#include <iostream>
#include "OLSRHello.h"

class ConcreteOLSRHello : public OLSRHello {
public:
	// Can provide any constructors needed; the sample relies on the
	// default constructor supplied by the compiler

	// concrete implementation for the header routine
	int setHeader(unsigned char * buf, int buflen, std::vector<IPAddr> mpr,
								std::vector<IPAddr> nbours) {
		int headerLen = 8;

		uint16_t reserved = 0;
		uint8_t reserved2 = 0;
		uint8_t Htime = 0;
		uint8_t Willingness = 3;


		// first check if the buffer is big enough
		if (buflen < headerLen + (4*(int)nbours.size()) + (4*(int)mpr.size()))
			return -1;
		
		std::memset(buf, 0, buflen);  // we clear the buffer first!
		*(reinterpret_cast<uint16_t*>(buf)) = reserved;
		*(reinterpret_cast<uint8_t*>(buf+2)) = Htime;
		*(reinterpret_cast<uint8_t*>(buf+3)) = Willingness;

		uint8_t LinkCode;

		if(((int)(nbours.size()) + (int)(mpr.size())) == 0)
			LinkCode = 0;
		else
			LinkCode = 6;

		*(reinterpret_cast<uint8_t*>(buf+4)) = LinkCode;
		*(reinterpret_cast<uint8_t*>(buf+5)) = reserved2;

		uint16_t LinkMessageSize = 4+(4*(int)mpr.size()) + (4* (int)nbours.size());  // set the packet length field

		*(reinterpret_cast<uint16_t*>(buf+6)) = LinkMessageSize;

		return 8 + (4*(int)mpr.size()) + (4*(int)nbours.size());  // the size of the header in bytes
	}
};

#endif
