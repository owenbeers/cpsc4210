//TestOLSRPacketPi2.h

// *** CPSC 4210/5210 Assignment 3 code
// ***** CPPUnit fixture class sample for creating headers for OLSRPacket messages ***

#ifndef __TESTOLSRPacketPI2_H
#define __TESTOLSRPacketPI2_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <iostream>

#include "OLSRPacketFactory.h"
#include "IPAddr.h"

using namespace std;
/// Test fixture for ProgressiveRateStrategy
class TestOLSRPacketPi2 : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(TestOLSRPacketPi2);
	CPPUNIT_TEST(testOLSRPacketDataTooLarge);
	CPPUNIT_TEST(testOLSRPacketPacketLength);
	CPPUNIT_TEST(testOLSRPacketPSN);
	CPPUNIT_TEST(testOLSRPacketMsgType);
	CPPUNIT_TEST(testOLSRValidMessageType);
	CPPUNIT_TEST(testOLSRPacketVTIME);
	CPPUNIT_TEST(testOLSRPacketOriginatorAddress);
	CPPUNIT_TEST(testOLSRPacketTTL);
	CPPUNIT_TEST(testOLSRPacketHopCount);
	CPPUNIT_TEST(testOLSRPacketMsgLen);
	CPPUNIT_TEST_SUITE_END();
private:
	// we make the factory static so that we can initialize it in the
	// .cc file to the concrete factory that returns the concrete object
	// with our implementation of setHeader()
	static std::shared_ptr<OLSRPacketFactory> factory;

private:
	// the packet object being tested
	std::shared_ptr<OLSRPacket> packet;
	// the buffer used for testing
	unsigned char * buffer;
	// the buffer length
	const int buflen = 1000;
	
	//Constants for tests
	int offset = 0;
	int packHeaderLen = 16;
	IPAddr src,dst;
	int msg_type = 1;
	int TTL = 6;
	int hop_count = 1;
	uint16_t psn = 1;
	uint16_t msn = 1;
	int msglen = 1;
	

public:
	void setUp()  {
		if (factory)
			packet = factory->createOLSRPacket();
		else {
	    std::cerr << "TestOLSRPacketPi2 test fixture not properly initialized!\n";
			exit(EXIT_FAILURE);
		}

		buffer = new unsigned char [buflen];

		
	};
	
	void tearDown()  {
		delete[] buffer;
		offset = 0;
		packHeaderLen = 16;
		msg_type = 1;
		TTL = 6;
		hop_count = 1;
		psn = 1;
		msn = 1;
		msglen = 1;
	};

	// *************** test cases ************
	void testOLSRPacketDataTooLarge() {

		int header_len = packet->setHeader(buffer, packHeaderLen, msg_type, src, TTL, hop_count,
										   psn, msn, msglen);
		CPPUNIT_ASSERT(header_len == -1);
	}

	void testOLSRPacketPacketLength()
	{	
		msglen = 30;
		packet->setHeader(buffer, buflen, msg_type, src, TTL, hop_count,
										   psn, msn, msglen);

		//get the packet length integer (should be the 4 bytes of)
		uint16_t newPackLen, expectedVal = packHeaderLen + msglen;
		std::memcpy(&newPackLen, buffer+offset, sizeof(uint16_t));
		newPackLen = ntohs(newPackLen);

		//std::printf("%d, %d", newPackLen, expectedVal);
		CPPUNIT_ASSERT(newPackLen == expectedVal);

	}

	void testOLSRPacketPSN()
	{
		psn = 2;
		uint16_t newPsn, expectedVal = 2;
		packet->setHeader(buffer, buflen, msg_type, src, TTL, hop_count,
										   psn, msn, msglen);

		std::memcpy(&newPsn, buffer+offset+2, sizeof(uint16_t));
		// std::printf("%d, %d", newPsn, expectedVal);
		newPsn = ntohs(newPsn);
		CPPUNIT_ASSERT(newPsn == expectedVal);
		psn = 1;
		packet->setHeader(buffer, buflen, msg_type, src, TTL, hop_count,
										   psn, msn, msglen);
		std::memcpy(&newPsn, buffer+offset+2, sizeof(uint16_t));
		newPsn = ntohs(newPsn);
		CPPUNIT_ASSERT(newPsn != expectedVal);
	}


	void testOLSRPacketMsgType()
	{	
		msg_type = 1;
		int newMsgType, expectedVal=1;
		packet->setHeader(buffer, buflen, msg_type, src, TTL, hop_count,
										   psn, msn, msglen);
		std::memcpy(&newMsgType, buffer+offset+4, 1);
		CPPUNIT_ASSERT(newMsgType == expectedVal);


		msg_type = 112;
		packet->setHeader(buffer, buflen, msg_type, src, TTL, hop_count,
									   psn, msn, msglen);
		std::memset(&newMsgType, 0, sizeof(int));
		std::memcpy(&newMsgType, buffer+offset+4, 1);
		CPPUNIT_ASSERT(newMsgType != expectedVal);
	}

	void testOLSRValidMessageType()
	{
		msg_type = 256;
		int header_len = packet->setHeader(buffer, buflen, msg_type, src, TTL, hop_count,
										   psn, msn, msglen);
		CPPUNIT_ASSERT(header_len == -1);
	}

	void testOLSRPacketVTIME()
	{
		packet->setHeader(buffer, buflen, msg_type, src, TTL, hop_count,
						  psn, msn, msglen);

		unsigned int newVTIME;

		std::memcpy(&newVTIME, buffer+offset+5, 1);
		CPPUNIT_ASSERT(6 == newVTIME);
	}


	void testOLSRPacketOriginatorAddress()
	{
		packet->setHeader(buffer, buflen, msg_type, src, TTL, hop_count,
						  psn, msn, msglen);

		uint32_t newSrc;
		memcpy(&newSrc, buffer+offset+8, sizeof(uint32_t));

		// unsigned char parseAddress[4];

		// parseAddress[3] = (newSrc >> 24) & 0xFF;
		// parseAddress[2] = (newSrc >> 16) & 0xFF;
		// parseAddress[1] = (newSrc >> 8) & 0xFF;
		// parseAddress[0] = newSrc & 0xFF;

		// for(int i=0; i < 4; i++)
		// 	cout << (int) parseAddress[i] << endl;

		// cout << src.to32bWord() << endl;
		CPPUNIT_ASSERT(newSrc == src.to32bWord());

		unsigned char pi = '2', node = '1';
		src = IPAddr(pi, node);
		packet->setHeader(buffer, buflen, msg_type, src, TTL, hop_count,
						  psn, msn, msglen);
		uint32_t secondSrc;
		memcpy(&secondSrc, buffer+offset+8, sizeof(uint32_t));
		CPPUNIT_ASSERT(secondSrc == src.to32bWord());

	}

	void testOLSRPacketTTL()
	{
		TTL = -1;
		int header_len = packet->setHeader(buffer, buflen, msg_type, src, TTL, hop_count,
						  				   psn, msn, msglen);
		CPPUNIT_ASSERT(header_len == -1);

		TTL = 6;

		packet->setHeader(buffer, buflen, msg_type, src, TTL, hop_count,
						  psn, msn, msglen);

		int newTTL;
		std::memcpy(&newTTL, buffer+offset+12, 1);
		CPPUNIT_ASSERT(newTTL == TTL);
	}

	void testOLSRPacketHopCount()
	{
		hop_count = 256;
		int header_len = packet->setHeader(buffer, buflen, msg_type, src, TTL, hop_count,
						  				   psn, msn, msglen);
		CPPUNIT_ASSERT(header_len == -1);

		hop_count = 2;
		packet->setHeader(buffer, buflen, msg_type, src, TTL, hop_count,
						  psn, msn, msglen);
		int newHopCount;
		std::memcpy(&newHopCount, buffer+offset, 1);
		CPPUNIT_ASSERT(newHopCount = hop_count);
	}

	void testOLSRPacketMsgLen()
	{
		msglen = 30;
		packet->setHeader(buffer, buflen, msg_type, src, TTL, hop_count,
						  psn, msn, msglen);
		uint16_t newMsgLen;
		std::memcpy(&newMsgLen, buffer+offset+6, sizeof(uint16_t));
		newMsgLen = ntohs(newMsgLen);
		CPPUNIT_ASSERT(newMsgLen == msglen);
	}
};

#endif
