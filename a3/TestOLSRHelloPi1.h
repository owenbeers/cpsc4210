#ifndef __TESTOLSRHELLOPI1_H
#define __TESTOLSRHELLOPI1_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <iostream>

#include "OLSRPacketFactory.h"
#include "OLSRHelloFactory.h"
#include "IPAddr.h"

#include <vector>

/// Test fixture for ProgressiveRateStrategy
class TestOLSRHelloPi1 : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(TestOLSRHelloPi1);
	CPPUNIT_TEST(helloBufferTooSmall);
	CPPUNIT_TEST(testMPR);
	CPPUNIT_TEST(testNeighbour);
	CPPUNIT_TEST(testForCompletePacket);
	CPPUNIT_TEST(checkFrontBufferAllocation);
	CPPUNIT_TEST(checkMidBufferAllocation);
	CPPUNIT_TEST(checkForSixLinkCode);
	CPPUNIT_TEST(checkLMS);

	CPPUNIT_TEST_SUITE_END();
private:
	// we make the factory static so that we can initialize it in the
	// .cc file to the concrete factory that returns the concrete object
	// with our implementation of setHeader()
	static std::shared_ptr<OLSRHelloFactory> factory;

private:
	// the packet object being tested
	std::shared_ptr<OLSRHello> helloPacket;

	// the buffer used for testing
	unsigned char * buffer;
	// the buffer length
	const int buflen = 1000;

	int offset = 0;
	std::vector<IPAddr> testMpr, testNbours;



public:
	void setUp()  {
		if (factory)
			helloPacket = factory->createOLSRHello();
		else {
	    std::cerr << "OLSR Hello packet tests not properly initialized!\n (TestOLSRHelloPi1.h - Line 39)\n";
			exit(EXIT_FAILURE);
		}
		buffer = new unsigned char [buflen];
	};

	void tearDown()  {
		delete[] buffer;
		// we don't need to delete ipudp since it is a smart pointer
	};

	// *************** test cases ************
	void helloBufferTooSmall() {
		int header_len = helloPacket->setHeader(buffer, 7, testMpr, testNbours);
		CPPUNIT_ASSERT(header_len == -1);
	}

	void testMPR(){
		IPAddr testIP;
		testIP.to32bWord();

		testMpr.push_back(testIP);
		int header_len = helloPacket->setHeader(buffer, 8, testMpr, testNbours);

		CPPUNIT_ASSERT(header_len == -1);

	}

	void testNeighbour(){
		IPAddr testIP;
		testIP.to32bWord();

		testNbours.push_back(testIP);
		int header_len = helloPacket->setHeader(buffer, 8, testMpr, testNbours);

		CPPUNIT_ASSERT(header_len == -1);

	}

	void testForCompletePacket(){
		int header_len = helloPacket->setHeader(buffer, 12, testMpr, testNbours);
		CPPUNIT_ASSERT(header_len == 96);
	}

	void checkFrontBufferAllocation(){
		helloPacket->setHeader(buffer, buflen, testMpr, testNbours);

		uint16_t testVal = 0;
		uint16_t frontReserve;


		std::memcpy(&frontReserve, buffer+offset, sizeof(uint16_t));
		CPPUNIT_ASSERT(testVal == frontReserve);
	}

	void checkMidBufferAllocation(){
		helloPacket->setHeader(buffer, buflen, testMpr, testNbours);

		uint16_t testVal = 0;
		uint16_t midReserve;


		std::memcpy(&midReserve, buffer+offset+5, sizeof(uint16_t));
		CPPUNIT_ASSERT(testVal == midReserve);
	}

	void checkForSixLinkCode(){

		uint8_t testVal = 6;
		uint8_t linkCode;


		std::memcpy(&linkCode, buffer+offset+4, sizeof(uint8_t));
		CPPUNIT_ASSERT(testVal == linkCode);
	}

	void checkLMS(){
		helloPacket->setHeader(buffer, buflen, testMpr, testNbours);

		int nboursSize = (4*(int)testNbours.size());
		int mprSize = (4*(int)testMpr.size());


		int headerlen = 8;
		int headerSize = headerlen + nboursSize + mprSize + offset;

		uint8_t testVal = headerSize;
		uint8_t lMS;


		std::memcpy(&lMS, buffer+offset+7, sizeof(uint8_t));
		CPPUNIT_ASSERT(testVal == lMS);
	}
};

#endif
