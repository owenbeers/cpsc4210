from socket import AF_NETLINK, SOCK_DGRAM, socket
from uuid import getnode as get_mac

sock = socket(AF_NETLINK, SOCK_DGRAM)

# Port number and socket
port = None
sock = None
mac = get_mac()
h = hex(mac)[2:].zfill(12)
print ":".join(i + j for i, j in zip(h[::2], h[1::2]))
