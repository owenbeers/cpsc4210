	#PACKETS
	#RESERVED: 16 BITS (0-15)	Htime: 8 bits (16-23)	Willingness: 8 bits (24-31)
		#0000000000000000 		00000000 				00000011
	#LinkCode: 8 bits (0-7) 	RESERVED: 8 bits (8-15) 	Link Message Size 16 bits (16-31)
		#ASYM_LINK NOT_NEIGH								BYTES TILL NEXT LINK MESSAGE (# of neighbors?) 
		#0001 0000 				00000000 					0000000000001111    - Denotes 7 neighbors in this list
		#SYM_LINK SYM_NEIGH 								BYTES TILL NEXT LINK MESSAGE (# of neighbors?)
		#0010 0001 				00000000 					0000000000010000 	- Denotes 8 neighbors in this list
	#START NEIGHBOR LIST	
	#NEIGHBOR MAC ADDRESS: 32 bits (0-31)
		#MAC ADDRESS IN STRING
		#MAC ADDRESS IN STRING
		#MAC ADDRESS IN STRING
		#MAC ADDRESS IN STRING
		#etc...
	#LinkCode: 8 bits (0-7) 	RESERVED: 8 bits (8-15)		Link Message Size 16 bits (16-31)
		#ASYM_LINK SYM_NEIGH								BYTES TILL NEXT LINK MESSAGE (# of neighbors?) 
		#0001 0001 				00000000 					0000000000001111    - Denotes 7 neighbors in this list
		#SYM_LINK NOT_NEIGH 								BYTES TILL NEXT LINK MESSAGE (# of neighbors?)
		#0010 0000 				00000000 					0000000000010000 	- Denotes 8 neighbors in this list
	#START NEIGHBOR LIST
		#MAC ADDRESS IN STRING
		#MAC ADDRESS IN STRING
		#etc...



		#HTIME BASED ON 2 SECONDS - A = 0 (0000), B = 5 (1001) ( NOT USED ) SET TO 0 ALWAYS

		#SYM_LINK indicates the links are symmetric (received self in hello message)

		#ASYM_LINK indicates neighbor has been heard but not confirmed

		#SYM_NEIGH indicates the neighbor list are symmetric neighbors
			#USED FOR INDICATING THE LIST OF NEIGHBORS ARE 1-HOP SYMMETRICS

		#NOT_NEIGH indicates the neighbor list are no longer neighbors or are not symmetric yet
			#USED FOR INCLUDING ASYMMETRIC NEIGHBORS IN HELLO (TO DETERMINE SYMMETRIC NEIGHBORS)

		#NOTE: WE DO NOT SEND THE LIST OF 2-HOP NEIGHBORS EVER, ONLY PRINT TO FILE(LOG).

		#NOTE2: IF A MESSAGE ARRIVES INDICATING SYM_LINK AND NOT_NEIGH, THE PACKET IS WRONG AND
				#SHOULD BE DISCARDED.