import time
import uuid

#Change all splits to use the correct delimiter, right now it works with ' '

def main():
	#Thanks stack overflow
	def get_mac():
		mac_num = hex(uuid.getnode()).replace('0x', '').upper()
 		mac = '-'.join(mac_num[i : i + 2] for i in range(0, 11, 2))
  		return mac

	class Chat_Receiver(threading.Thread):
		def __init__(self):
			threading.Thread.__init__(self)
			self.port = None
			self.sock = None
			self.running = 1
			def run(self):
				while self.running:
					try:
						ourMAC = get_mac()
						message, address = self.sock.recvfrom(10000)
						senderIP, senderPORT = address
						#FIX--We want the senders MAC, not their IP
						#senderMAC = SOMETHING

						foundSender = false
						for v in range(len(foundClose))
							if senderMAC == foundClose[v]:
								foundSender = true
						if not foundSender:
							foundClose.append(senderMAC)

						if message:
							reserved, htime, willingness, linktype, neightype, reserved2, numAddresses, addressList = message.split(8)
							#ignore reserved, htime, reserved2
							if willingness != "00000011"
								break

							#Symmetric Link, Asymmetric Neighbors
							if linktype == "0010" and neightype == "0000"
								break #Invalid Packet

							#Asymmetric Link, Asymmetric Neighbors <--- DISCARD?
							if linktype == "0001" and neightype == "0000"

							#Asymmetric Link, Symmetric Neighbors <---- DISCARD?
							if linktype == "0001" and neightype == "0001"

							#Symmetric Link, Symmetric Neighbors <--- WE WANT THIS
							if linktype == "0010" and neightype == "0001"
								addressList = addressList.split(numAddresses)

								for w in range(len(addressList)):
									currentMAC = addressList.pop()

									foundSymmetric = false
									foundClose = false
									foundFar = false
									#Check if this address is us
									if currentMAC == ourMAC:
										#Add this neighbor to our symmetric link list for sending
										# or reset their timer
										for x in range(len(symmetricNeighbors)):
											if senderMAC == symmetricNeighbors[x]:
												foundSymmetric = true
												symmetricNeighbors[x].timer = resetTimerVal
										if not foundSymmetric:
											symmetricNeighbors.append(senderMAC)

									#If this address is not us, check our neighbor lists
									else:
										#Loop through our 1-hop neighbors
										for y in range(len(closeneighbors)):
											if currentMAC == closeneighbors[y]:
												foundClose = true

										#Loop through our 2-hop neighbors
										if not foundClose:
											for z in range(len(farneighbors)):
												if currentMAC == farneighbors[z]:
													foundFar = true
											if not foundFar:
												farneighbors.append(currentMAC)