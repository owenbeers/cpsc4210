#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "packet.h"
#include "helloMsg.h"

int main(int argc, char** argv)
{

	/*Neighbour Lists*/
	struct mac_addr asymList[10];
	struct mac_addr symList[10];
	struct mac_addr twoHopList[10];

	struct mac_addr ourMAC;
	ourMAC.bytes[0] = '\x02';
	ourMAC.bytes[1] = '\x02';
	ourMAC.bytes[2] = '\x02';
	ourMAC.bytes[3] = '\x02';

	int i;

	struct mac_addr senderMAC;
	for(i=0; i<4; i++)
		senderMAC.bytes[i] = 0x03;

	struct mac_addr * asymPtr = malloc(sizeof(struct mac_addr));
	asymPtr = &asymList[0];

	struct mac_addr * symPtr = malloc(sizeof(struct mac_addr));
	symPtr = &symList[0];

	struct mac_addr * twoHopPtr = malloc(sizeof(struct mac_addr));
	twoHopPtr = &twoHopList[0];

	int sizeAsym = 0;
	int sizeSym = 0;
	int sizeTwoHop = 0;
	int *sizeAsymPtr = &sizeAsym;
	int *sizeSymPtr = &sizeSym;
	int *sizeTwoHopPtr = &sizeTwoHop;

	FILE * file;
	file = fopen("log.txt", "w");
	if(file==NULL)
		printf("error opening file\n");


	printf("%s", "step 1");
	
	struct mac_addr originatorAddress;
	originatorAddress.bytes[0] = '\x02';
	originatorAddress.bytes[1] = '\x03';
	originatorAddress.bytes[2] = '\x04';
	originatorAddress.bytes[3] = '\x05';
	struct Packet pack, *recHi;
	initPacket(&pack, ourMAC, symList, sizeSym, asymList, sizeAsym);
	printf("%s", "step 2");
	char * buffer = serializePacket(&pack);
	recHi = deserializePacket(buffer);

	int timer;
	for(timer=0; timer<20; timer++)
	{
		recvAddressList(timer, recHi->helloSYM, asymPtr, symPtr, twoHopPtr, senderMAC, ourMAC, sizeAsymPtr, sizeSymPtr, sizeTwoHopPtr, file);
		recvAddressList(timer, recHi->helloASYM, asymPtr, symPtr, twoHopPtr, senderMAC, ourMAC, sizeAsymPtr, sizeSymPtr, sizeTwoHopPtr, file);
		checkExpiry(asymPtr, symPtr, twoHopPtr, sizeAsymPtr, sizeSymPtr, sizeTwoHopPtr, timer);
	}


	printf("We made it!");
	free(buffer);
	free(pack.helloSYM->neighborList);
	free(pack.helloASYM->neighborList);
	free(pack.helloASYM);
	free(pack.helloSYM);
	destroyPacket(recHi);

	return 0;
}

// for (i = 0; i < 4; ++i)
// 	{	
// 		symNeighborList[0].bytes[i] = 0x01;
// 	}
// 	for (i = 0; i < 4; ++i)
// 	{	
// 		symNeighborList[1].bytes[i] = 0x05;
// 	}

// 	for (i = 0; i < 4; ++i)
// 	{	
// 		asymNeighborList[0].bytes[i] = 0x05;
// 	}

// struct mac_addr symNeighborList[5];
// struct mac_addr asymNeighborList[5];