CFLAGS=-Wall
CC=gcc
LN=gcc

# prefix rules
%.o : %.c
	$(CC) $(CFLAGS) -c -o $@ $<
	$(CC) $(CFLAGS) -MM -MP -MT $@ $< > $(basename $@).d

SENDOBJ = sock sendmain
RECVOBJ = sock recvmain
NEWOBJ = sock helloMsg packet newMain
TESTOBJ = helloMsg packet tests

all: mysend myrecv main tests

mysend: $(addsuffix .o, $(SENDOBJ))
	$(LN) -o $@ $^

myrecv: $(addsuffix .o, $(RECVOBJ))
	$(LN) -o $@ $^

tests: $(addsuffix .o, $(TESTOBJ))
	$(LN) -o $@ $^


main: $(addsuffix .o, $(NEWOBJ))
	$(LN) -o $@ $^

.PHONY : clean
clean :
	rm -f *.o *.d *~ mysend

# add dependency info to Makefile
-include $(addsuffix .d,$(basename $(SENDOBJ) $(RECVOBJ)))
