#ifndef __MAC_H_
#define __MAC_H_

struct mac_addr
{
	unsigned char bytes[4];
	int expiry;
};

#endif