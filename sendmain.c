#include <sys/socket.h>
#include <sys/types.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <stdio.h>
#include <string.h>

#include "sock.h"


/* broadcast a hello world packet */
int main() {
  struct Ifconfig ifc;
  init(&ifc);

  printf("Sending from ");
  /* print the MAC addr */
  int i;
  for (i=0; i<MACADDRLEN-1; i++)
    printf("%02X:", ifc.mac[i]);
  printf("%02X\n", ifc.mac[MACADDRLEN-1]);

  /* set-up destination structure in a sockaddr_ll struct */
  struct sockaddr_ll to;
  memset(&to, 0, sizeof(to)); /* clearing the structure, just in case */
  to.sll_family = AF_PACKET; /* always AF_PACKET */
  to.sll_ifindex = ifc.ifindex;
  to.sll_halen = MACADDRLEN;

  /* setup broadcast address of FF:FF:FF:FF:FF:FF */
  for (i=0; i<MACADDRLEN; i++)
    to.sll_addr[i] = 0xff;

  char * msg = "Hello world!";
  int sentlen = sendto(ifc.sockd, msg, strlen(msg)+1, 0, (struct sockaddr*) &to, sizeof(to));
  printf("%d bytes sent\n", sentlen);
  
  destroy(&ifc);
  return 0;
}
